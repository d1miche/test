<?php

namespace App\Controller;

use App\Service\CompanyService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends BaseController
{
    /**
     * @param string $id
     * @param CompanyService $companyService
     * @Route("/company/category/{id}", name="get_company_by_category_id")
     * @return Response
     */
    public function getByCategory($id, CompanyService $companyService)
    {
        $models = $companyService->getByCategory($id);
        return $this->buildView($models);
    }

    /**
     * @param Request $request
     * @param CompanyService $companyService
     * @Route("/company/find", name="find_company_by_name")
     * @return Response
     */
    public function findByName(Request $request, CompanyService $companyService)
    {
        $query = $request->get('query', '');
        $models = $companyService->findByName($query);
        return $this->buildView($models);
    }

    /**
     * @param string $id
     * @param CompanyService $companyService
     * @Route("/company/{id}", name="get_company_by_id")
     * @return Response
     */
    public function getById($id, CompanyService $companyService)
    {
        $models = $companyService->getById($id);
        return $this->buildView($models);
    }
}
