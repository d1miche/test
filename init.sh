#sudo rm -rf docker/postgres/*
docker-compose up -d
docker-compose exec php php composer.phar install
docker-compose exec php php bin/console doctrine:migrations:migrate --no-interaction
docker-compose exec php php bin/console app:db:fill