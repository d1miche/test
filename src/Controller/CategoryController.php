<?php


namespace App\Controller;


use App\Service\CategoryService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends BaseController
{
    /**
     * @param string $id
     * @param CategoryService $categoryService
     * @Route("/category/parent/{id}", name="get_category_by_parent_id")
     * @return Response
     */
    public function getByParentId($id, CategoryService $categoryService)
    {
        $models = $categoryService->getByParentId($id);
        return $this->buildView($models);
    }
}