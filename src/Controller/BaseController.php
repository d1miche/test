<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends AbstractController
{
    /**
     * @param $models
     * @return Response
     */
    protected function buildView($models)
    {
        $data = $this->container
            ->get('serializer')
            ->serialize($models, 'json', ['groups' => 'public']);

        return new Response($data);
    }
}