<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200522070007 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(/** @lang PostgreSQL */ 'CREATE TABLE "company" (
            id serial PRIMARY KEY NOT NULL,
            title VARCHAR(255) NOT NULL
        )');

        $this->addSql(/** @lang PostgreSQL */
            'CREATE INDEX company_title ON company(title)'
        );

        $this->addSql(/** @lang PostgreSQL */ 'CREATE TABLE "category" (
            id serial PRIMARY KEY NOT NULL,
            title VARCHAR(255) NOT NULL,
            parent_id integer REFERENCES category(id)
        )');

        $this->addSql(/** @lang PostgreSQL */ 'CREATE TABLE "company_category" (
            id serial PRIMARY KEY NOT NULL,
            company_id INTEGER NOT NULL REFERENCES company(id),
            category_id INTEGER NOT NULL REFERENCES category(id)
        )');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE company_category');
    }
}
