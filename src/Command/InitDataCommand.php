<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitDataCommand extends Command
{
    protected static $defaultName = 'app:db:fill';

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Fill database.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $faker = Factory::create();

        $categories = [];
        for ($i = 0; $i<=300; $i++) {
            $category = new Category();
            $category->setTitle($faker->title);

            if (intval($i/100) > 0) {
                $category->setParent($categories[$i%100]);
            }

            $categories[] = $category;
            $this->em->persist($category);
        }

        for ($i = 0; $i<=1000; $i++) {
            $company = new Company();
            $company->setTitle($faker->company);

            for ($c = 0; $c <= 3; $c++) {
                $company->addCategory($faker->randomElement($categories));
            }

            $this->em->persist($company);
        }


        $this->em->flush();
    }
}
