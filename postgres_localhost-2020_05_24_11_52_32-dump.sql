--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    parent_id integer
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- Name: company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company (
    id integer NOT NULL,
    title character varying(255) NOT NULL
);


ALTER TABLE public.company OWNER TO postgres;

--
-- Name: company_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company_category (
    id integer NOT NULL,
    company_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.company_category OWNER TO postgres;

--
-- Name: company_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.company_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_category_id_seq OWNER TO postgres;

--
-- Name: company_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.company_category_id_seq OWNED BY public.company_category.id;


--
-- Name: company_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.company_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_id_seq OWNER TO postgres;

--
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.company_id_seq OWNED BY public.company.id;


--
-- Name: migration_versions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migration_versions (
    version character varying(14) NOT NULL,
    executed_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.migration_versions OWNER TO postgres;

--
-- Name: COLUMN migration_versions.executed_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.migration_versions.executed_at IS '(DC2Type:datetime_immutable)';


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- Name: company id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company ALTER COLUMN id SET DEFAULT nextval('public.company_id_seq'::regclass);


--
-- Name: company_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_category ALTER COLUMN id SET DEFAULT nextval('public.company_category_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (id, title, parent_id) FROM stdin;
1	Pfeffer, Haag and Kuphal	\N
2	Powlowski Ltd	\N
3	Bergnaum, Daniel and Rogahn	\N
4	Gibson LLC	\N
5	Feeney-Hodkiewicz	\N
6	Grady-Gleason	\N
7	Steuber-Lind	\N
8	McGlynn-Nienow	\N
9	Dietrich, Pacocha and Kautzer	\N
10	Leannon-Hirthe	\N
11	Hoppe-Larkin	\N
12	Wolff LLC	\N
13	Stanton LLC	\N
14	Predovic Ltd	\N
15	Haag LLC	\N
16	Grimes, Braun and Pacocha	\N
17	Jakubowski, Hoeger and Mitchell	\N
18	Hayes-Nicolas	\N
19	Kovacek-Auer	\N
20	Prohaska Inc	\N
21	Thiel-Weissnat	\N
22	Rosenbaum, Zemlak and Brakus	\N
23	Stanton-Donnelly	\N
24	Goldner-Armstrong	\N
25	Bradtke Group	\N
26	Zboncak and Sons	\N
27	Kuhic Inc	\N
28	Langosh, Zboncak and Bashirian	\N
29	Eichmann Inc	\N
30	Raynor and Sons	\N
31	White and Sons	\N
32	Kuhlman-Schultz	\N
33	Mills Inc	\N
34	Huels-Stoltenberg	\N
35	Hammes Ltd	\N
36	Ledner-Legros	\N
37	Schoen-Sanford	\N
38	Kreiger-Roberts	\N
39	Towne, Kunde and Klocko	\N
40	Schaden, Lynch and Emmerich	\N
41	Herman Ltd	\N
42	Goyette-Frami	\N
43	Abbott Inc	\N
44	Bernhard-Koss	\N
45	Lueilwitz Ltd	\N
46	Hodkiewicz-Roob	\N
47	Will, Klocko and Brown	\N
48	Volkman Ltd	\N
49	Barrows-Turcotte	\N
50	Schuster PLC	\N
51	West Group	\N
52	Deckow, Grimes and Hirthe	\N
53	Friesen-O'Hara	\N
54	Frami-McDermott	\N
55	Walker, Macejkovic and Schumm	\N
56	Lakin-Thompson	\N
57	Labadie LLC	\N
58	Rutherford, Mills and Upton	\N
59	White, Hayes and Collier	\N
60	Runolfsson-Barton	\N
61	Reichel Ltd	\N
62	Nader-Moen	\N
63	Herman-Heaney	\N
64	Lindgren Inc	\N
65	Stiedemann, Hessel and Quitzon	\N
66	Gorczany-Cole	\N
67	Toy, Ferry and D'Amore	\N
68	Collins-Boehm	\N
69	Miller Group	\N
70	Kreiger, Kuvalis and Champlin	\N
71	Hamill and Sons	\N
72	Mosciski and Sons	\N
73	Corwin and Sons	\N
74	Murazik Ltd	\N
75	Vandervort-Hudson	\N
76	Bauch-Keeling	\N
77	Anderson Group	\N
78	Schumm LLC	\N
79	Berge Group	\N
80	Gerhold-Treutel	\N
81	Schuster Inc	\N
82	Spinka and Sons	\N
83	Rosenbaum, Langosh and Dickens	\N
84	Legros, Heller and Powlowski	\N
85	Welch-Harvey	\N
86	Cronin Ltd	\N
87	Ward, Armstrong and Gibson	\N
88	Dach, Lesch and Durgan	\N
89	Kuhn-Bradtke	\N
90	Ernser-Buckridge	\N
91	Pollich-Trantow	\N
92	Stanton, Beahan and Miller	\N
93	Glover-Purdy	\N
94	Collier Ltd	\N
95	Bechtelar-Huels	\N
96	Satterfield Group	\N
97	Lockman PLC	\N
98	Ruecker LLC	\N
99	Kessler, Russel and Schumm	\N
100	Boyer Group	\N
302	Stroman-Hettinger	\N
303	Skiles, Schuster and Reichel	\N
304	Orn, Lindgren and Schultz	\N
305	Fritsch-Lueilwitz	\N
306	Welch-Kertzmann	\N
307	Huels-Frami	\N
308	Barrows, Lemke and Wuckert	\N
309	Walter-Wisoky	\N
310	Zemlak PLC	\N
311	Blick-Tromp	\N
312	Williamson, Ankunding and Bauch	\N
313	Streich, Crona and Roberts	\N
314	Yost-Purdy	\N
315	Jacobson, Marquardt and Hagenes	\N
316	Schneider-Rempel	\N
317	Bergstrom, Weimann and Macejkovic	\N
318	Erdman, Ryan and O'Conner	\N
319	Turcotte and Sons	\N
320	Glover and Sons	\N
321	Little-Zboncak	\N
322	Quigley Group	\N
323	Rolfson, Skiles and Botsford	\N
324	Streich Group	\N
325	Runolfsson, Zieme and Skiles	\N
326	Pacocha, Osinski and Borer	\N
327	Gerhold Ltd	\N
328	Ankunding-Bayer	\N
329	Kling-Hilpert	\N
330	Sipes, Koch and Block	\N
331	Osinski Ltd	\N
332	Ryan LLC	\N
333	Renner, Raynor and Lebsack	\N
334	Stoltenberg, Hudson and Treutel	\N
335	McDermott, Dietrich and Balistreri	\N
336	DuBuque Ltd	\N
337	Beier and Sons	\N
338	Hoppe-Kub	\N
339	Botsford, Reynolds and Rutherford	\N
340	Boyle and Sons	\N
341	Steuber PLC	\N
342	Quitzon and Sons	\N
343	Gleichner PLC	\N
344	Monahan Inc	\N
345	Eichmann PLC	\N
346	Welch-Schaefer	\N
347	Kutch-Larson	\N
348	Wuckert-Haag	\N
349	Rempel Inc	\N
350	Gleason, Wunsch and Kovacek	\N
351	Raynor-Grimes	\N
352	Kuhlman-Schaefer	\N
353	Collins-Stoltenberg	\N
354	Nader PLC	\N
355	Stoltenberg, Deckow and Glover	\N
356	Heathcote-Considine	\N
357	Raynor Group	\N
358	Mohr-Roob	\N
359	Larson Group	\N
360	Schumm Ltd	\N
361	Dickens-Donnelly	\N
362	Bayer, Herzog and Christiansen	\N
363	Armstrong, Morar and Feil	\N
364	Pacocha and Sons	\N
365	Schuppe and Sons	\N
366	Quitzon-Gottlieb	\N
367	Bogan-Mills	\N
368	Hyatt-Schulist	\N
369	Nader Ltd	\N
370	Bayer, Corwin and Hessel	\N
371	Kshlerin-Ortiz	\N
372	Jones, Torphy and Nienow	\N
373	Conn-Moen	\N
374	Ferry-Block	\N
375	Ebert-Ankunding	\N
376	Hansen-Crona	\N
377	Torphy Inc	\N
378	Wolf Inc	\N
379	Lesch Ltd	\N
380	Sipes-Predovic	\N
381	Yost, Mante and Wehner	\N
382	Leffler-Weber	\N
383	Cole PLC	\N
384	Osinski, Flatley and Cartwright	\N
385	Simonis, Sipes and Ondricka	\N
386	Rolfson PLC	\N
387	Howell Ltd	\N
388	Morar-Turcotte	\N
389	Senger, Kemmer and Kris	\N
390	Pouros PLC	\N
391	Boehm-Bahringer	\N
392	Lebsack, Pouros and Parisian	\N
393	Goodwin Group	\N
394	Legros, Pouros and Bogisich	\N
395	Jerde-Hand	\N
396	Tremblay LLC	\N
397	Kihn Ltd	\N
398	McDermott-Pouros	\N
399	Lueilwitz, Schuppe and Padberg	\N
400	Olson-Schoen	\N
401	Cassin, Wehner and Dooley	\N
101	Becker-Rogahn	1
102	Hansen, Bashirian and Lebsack	2
103	Toy-Jacobs	3
104	Yost-Hintz	4
105	Torp, Waelchi and Sipes	5
402	Huel-Schmeler	302
106	Walter, Reinger and Grimes	6
107	Flatley, Williamson and Kassulke	7
108	Ernser Group	8
109	Walsh Group	9
110	Dooley LLC	10
111	Ebert and Sons	11
112	Dicki-Sanford	12
113	Muller, Barton and Walsh	13
114	Turcotte Inc	14
115	Pfeffer, Lang and Stamm	15
116	Murphy-Hartmann	16
117	Vandervort and Sons	17
118	DuBuque Inc	18
119	Nitzsche-Anderson	19
120	Walter, Lang and Kutch	20
121	Dickens, Nitzsche and Little	21
122	Hagenes-Dickens	22
123	Dach, Rath and Weissnat	23
124	Bernhard, Morissette and Ratke	24
125	Hane, Thompson and Smitham	25
126	Cassin-Howell	26
127	Bode-Abernathy	27
128	Aufderhar, Fahey and Pacocha	28
129	Price-Miller	29
130	Hermann PLC	30
131	Cormier PLC	31
132	Bogisich Inc	32
133	Eichmann LLC	33
134	Robel Group	34
135	Mraz-Green	35
136	Reinger, Okuneva and Stracke	36
137	Schaden and Sons	37
138	Lang-Price	38
139	Reynolds Group	39
140	Heidenreich Group	40
141	Considine-Cronin	41
142	Goyette-Wolff	42
143	Frami-Grady	43
144	Dare and Sons	44
145	Cassin-Koelpin	45
146	Abernathy, Herzog and Rutherford	46
147	Bechtelar, Hayes and DuBuque	47
148	Jast LLC	48
149	Williamson, Sipes and Klein	49
150	Corwin Ltd	50
151	Bergstrom-Stiedemann	51
152	Morar Ltd	52
153	Schuster Group	53
154	Pouros-Hauck	54
155	Bogan Inc	55
156	Hirthe and Sons	56
157	Dickinson, Cremin and Greenholt	57
158	Walter, Bode and Deckow	58
159	Renner-Stiedemann	59
160	Konopelski-Upton	60
161	Friesen-Wintheiser	61
162	Hessel, Mueller and Daniel	62
163	Waelchi-Johns	63
164	Parker-Hilpert	64
165	Steuber-Batz	65
166	Goodwin-Paucek	66
167	Shanahan-Wunsch	67
168	Bergnaum LLC	68
169	Olson-Zulauf	69
170	Williamson-Hahn	70
171	Bruen-Rodriguez	71
172	McClure-Langosh	72
173	Hackett, Dach and Prosacco	73
174	Gottlieb, Kuvalis and Klein	74
175	Mertz, Blanda and Hyatt	75
176	Lindgren Group	76
177	Welch-Jacobson	77
178	Marks Ltd	78
179	Wuckert-Klocko	79
180	Friesen-Turcotte	80
181	King PLC	81
182	Casper, Littel and Ratke	82
183	Hudson, Jacobi and Gutmann	83
184	Gorczany LLC	84
185	Streich-Ebert	85
186	Shanahan-O'Keefe	86
187	Klein Inc	87
188	Lehner, Glover and Hyatt	88
189	Shanahan-Toy	89
190	Baumbach Ltd	90
191	Schiller Inc	91
192	Marks-Mills	92
193	Medhurst LLC	93
194	Hintz, Stark and Wiza	94
195	Lehner PLC	95
196	Romaguera, O'Conner and Larkin	96
197	Herman, Wisozk and Kessler	97
198	Volkman, Wiegand and Prohaska	98
199	Littel, Keebler and Keebler	99
200	Cummings PLC	100
201	Bernhard Inc	1
202	Ullrich-Rodriguez	2
203	Reichel-Bins	3
204	Hirthe LLC	4
205	Effertz Inc	5
206	Gusikowski-Aufderhar	6
207	Johns PLC	7
208	Volkman, Auer and Dickens	8
209	Kuhn-Hilpert	9
210	Ortiz, Funk and Littel	10
211	Mertz LLC	11
212	Fay-Carter	12
213	Botsford Inc	13
214	Haley-Bergnaum	14
215	Lockman-Mayer	15
216	Stracke and Sons	16
217	Predovic and Sons	17
218	Mohr, Wisoky and Barton	18
219	Lowe-Lueilwitz	19
220	Hirthe Inc	20
221	Bahringer, Abshire and Abernathy	21
222	Goldner LLC	22
223	Franecki, Runte and Reichel	23
224	Lesch, Howell and Stokes	24
225	Leffler, Littel and Rice	25
226	O'Kon PLC	26
227	Pouros PLC	27
228	Stanton Inc	28
229	Smith-Cremin	29
230	Lockman-Nicolas	30
231	Altenwerth-Gerlach	31
232	Baumbach-Hammes	32
233	Kris-Lueilwitz	33
234	Jerde-Bashirian	34
235	Wisozk, Stamm and Legros	35
236	Lakin-Jacobson	36
237	Welch, Schumm and Emard	37
238	Romaguera, Davis and King	38
239	Olson and Sons	39
240	Bauch PLC	40
241	Harris Ltd	41
242	Larkin-Cartwright	42
243	Mohr, Cassin and Keebler	43
244	Fahey-Lang	44
245	Rodriguez, Hintz and Ernser	45
246	Rippin, Padberg and VonRueden	46
247	Borer and Sons	47
248	Zieme Inc	48
249	Wiza Ltd	49
250	McKenzie-Rutherford	50
251	Beier LLC	51
252	Schulist PLC	52
253	Daniel-Rowe	53
254	Ortiz Ltd	54
255	Daniel, O'Connell and Shanahan	55
256	Padberg, Beatty and Schaden	56
257	Spinka LLC	57
258	Kihn-DuBuque	58
259	Ebert-Schroeder	59
260	Bruen LLC	60
261	Williamson-Renner	61
262	Jaskolski-Wuckert	62
263	Jaskolski LLC	63
264	Volkman PLC	64
265	Bosco, Bartoletti and Schumm	65
266	Torp-Cartwright	66
267	Anderson-Graham	67
268	Hagenes, Schultz and Steuber	68
269	Nienow Inc	69
270	Nikolaus-Nolan	70
271	Greenholt Ltd	71
272	Lesch, Gerhold and Hane	72
273	Tillman Inc	73
274	Barton, Skiles and Jacobson	74
275	Stokes, Hackett and Hand	75
276	Wiegand Inc	76
277	Ritchie Group	77
278	Trantow, White and Brakus	78
279	Dickens, Harvey and Lowe	79
280	Erdman LLC	80
281	Prosacco LLC	81
282	Grady-Frami	82
283	Pacocha-Klocko	83
284	Hammes, McDermott and Turcotte	84
285	O'Kon-Raynor	85
286	Davis and Sons	86
287	Rohan-Tromp	87
288	Cruickshank-Ward	88
289	Crist, Spencer and Bashirian	89
290	Block Ltd	90
291	Hickle, Predovic and Langosh	91
292	Johnston LLC	92
293	Dickinson-Sauer	93
294	Berge-Stiedemann	94
295	Spinka-Rau	95
296	Brown-Veum	96
297	Kirlin, Emard and Kuhic	97
298	Wiza, Ratke and Rosenbaum	98
299	Dach-Cummerata	99
300	Reichert, Bartell and Carroll	100
301	Littel LLC	1
403	Champlin-Schuppe	303
404	Olson, Frami and Kuhic	304
405	Klocko, Boehm and Braun	305
406	Kirlin-Dibbert	306
407	Walker, Nicolas and Ledner	307
408	Weber-Roob	308
409	D'Amore-Daniel	309
410	Davis-Aufderhar	310
411	Prohaska, Schoen and Padberg	311
412	Jakubowski-Luettgen	312
413	Powlowski, Bernhard and O'Hara	313
414	Kiehn, Little and Dicki	314
415	Ratke, Hilpert and Hudson	315
416	McKenzie Group	316
417	Smitham Inc	317
418	Berge-Steuber	318
419	Waelchi Inc	319
420	Stracke, Sipes and Ritchie	320
421	Berge Group	321
422	Hickle Inc	322
423	Mraz PLC	323
424	Kuhlman PLC	324
425	Hessel-Labadie	325
426	Connelly-Bruen	326
427	Upton and Sons	327
428	Deckow Ltd	328
429	Paucek PLC	329
430	Wintheiser Ltd	330
431	Shields-Buckridge	331
432	Schmitt, Graham and Runte	332
433	Murazik LLC	333
434	Spencer-Koch	334
435	Block Inc	335
436	O'Connell, Donnelly and Little	336
437	Sporer-Daugherty	337
438	Hand, Kihn and Pagac	338
439	Franecki Inc	339
440	Olson and Sons	340
441	Schowalter LLC	341
442	Goodwin, Schmeler and Gerlach	342
443	Bergnaum, Barton and Bayer	343
444	Cole Inc	344
445	Considine, Jenkins and Quigley	345
446	Hamill-Adams	346
447	Tremblay-Heathcote	347
448	Goyette PLC	348
449	Kuphal-Littel	349
450	Ferry-Oberbrunner	350
451	Veum-Langworth	351
452	Klein-Metz	352
453	Vandervort PLC	353
454	Murazik PLC	354
455	Rolfson-Mayert	355
456	Wisoky Group	356
457	Yost PLC	357
458	Leannon LLC	358
459	Cartwright, Casper and Wiegand	359
460	Ryan Inc	360
461	Gusikowski-Lueilwitz	361
462	Feest Inc	362
463	Schroeder-Schowalter	363
464	Rippin PLC	364
465	Gusikowski-Swift	365
466	Jones-Strosin	366
467	Jacobs-Collins	367
468	Rogahn, Rosenbaum and Terry	368
469	Klocko Inc	369
470	D'Amore-Kessler	370
471	Osinski Inc	371
472	Terry, Robel and Adams	372
473	Johnston, Ullrich and Crist	373
474	Franecki-Cremin	374
475	Ziemann-Dibbert	375
476	Kihn, Rosenbaum and Barrows	376
477	Beier and Sons	377
478	Parisian-Mraz	378
479	Hirthe Ltd	379
480	Littel, Witting and Borer	380
481	Medhurst, Baumbach and McGlynn	381
482	Cartwright, Roob and Runte	382
483	Konopelski-Reichert	383
484	Senger-Goldner	384
485	Gulgowski, Weimann and West	385
486	Jacobi, Huels and Bergstrom	386
487	Kirlin-McGlynn	387
488	McKenzie PLC	388
489	Klein-Effertz	389
490	Rowe Inc	390
491	Frami and Sons	391
492	Schmeler-Wyman	392
493	Wilkinson-Powlowski	393
494	Toy, Raynor and Veum	394
495	Moore Group	395
496	Bartell, Skiles and Howe	396
497	Pfeffer and Sons	397
498	O'Kon, Satterfield and Labadie	398
499	Aufderhar Inc	399
500	Dickens-Carter	400
501	Kreiger, Dietrich and Schoen	401
502	Bins-Rosenbaum	302
503	O'Kon, Witting and Cummerata	303
504	Lynch, Gorczany and Murray	304
505	Dietrich-Mraz	305
506	Torp-Crist	306
507	Auer, Hackett and Pfeffer	307
508	Haag-Corwin	308
509	Runolfsdottir-Fadel	309
510	Kreiger, Johnson and Reinger	310
511	Abshire, Gleason and O'Reilly	311
512	Block-Rodriguez	312
513	Treutel Ltd	313
514	Langosh-Bins	314
515	Wiza, Mayer and Blanda	315
516	Murphy-Prosacco	316
517	Ortiz, Volkman and Jacobs	317
518	Abernathy-Bradtke	318
519	Cummings PLC	319
520	Carroll and Sons	320
521	Ortiz, Stanton and Johns	321
522	Quitzon, Wisozk and Watsica	322
523	Cremin-Hyatt	323
524	Leannon, Kilback and Friesen	324
525	Kemmer-Brekke	325
526	Gottlieb-Wehner	326
527	Miller, McClure and Fay	327
528	Baumbach-Hahn	328
529	Keebler Ltd	329
530	Osinski-Lynch	330
531	Langworth Ltd	331
532	Emard, McCullough and Cronin	332
533	Wuckert-Haag	333
534	Torphy Ltd	334
535	Spencer and Sons	335
536	Hand, Johnston and Blanda	336
537	Emmerich PLC	337
538	Leuschke Group	338
599	Von-Kuphal	399
539	Keeling and Sons	339
540	Schiller-Schumm	340
541	Jacobs, Swift and Gerhold	341
542	Rolfson, Runolfsson and Hodkiewicz	342
543	Daniel-Cummerata	343
544	Pouros PLC	344
545	Volkman-Pollich	345
546	Maggio PLC	346
547	Moore Ltd	347
548	Ebert, Koch and Bogan	348
549	Mosciski-Reinger	349
550	Kirlin, Considine and Larkin	350
551	Hamill PLC	351
552	Gulgowski LLC	352
553	Marks-Spinka	353
554	Parker Ltd	354
555	Leffler-Feeney	355
556	Sipes, Dach and Funk	356
557	Huels and Sons	357
558	Strosin Group	358
559	Konopelski-Graham	359
560	Goldner-McCullough	360
561	Zulauf PLC	361
562	Block, Stracke and Bailey	362
563	Gusikowski-Hahn	363
564	Collier Group	364
565	Dickinson, Collier and Lemke	365
566	Jacobi, Waters and Reichel	366
567	Kiehn, Kessler and Hyatt	367
568	Abbott, Kuhlman and Runolfsdottir	368
569	Veum, Tremblay and Kassulke	369
570	Lynch, Mitchell and Satterfield	370
571	Gislason Inc	371
572	Morissette, Schoen and Hammes	372
573	Goldner, Smitham and Klein	373
574	Upton, Bogan and Heaney	374
575	Grant and Sons	375
576	Johns-Rohan	376
577	Huels Ltd	377
578	Boehm, Mitchell and Kunde	378
579	Gaylord, Rau and Beer	379
580	Maggio-Boyle	380
581	Keebler PLC	381
582	Carter-Kessler	382
583	Breitenberg Group	383
584	Treutel-Monahan	384
585	Gleason Ltd	385
586	Runolfsson-Connelly	386
587	O'Hara, Gutkowski and Nicolas	387
588	Kerluke LLC	388
589	Funk Ltd	389
590	Borer Ltd	390
591	Heller Inc	391
592	Bradtke, Boyle and Padberg	392
593	Schaefer Group	393
594	Schoen-Schneider	394
595	Treutel PLC	395
596	Schiller-Schmitt	396
597	Abshire, Feil and Hackett	397
598	Grimes, Feeney and Grant	398
600	Kirlin Group	400
601	Koepp Inc	401
602	Ziemann, Wisoky and Koss	302
\.


--
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.company (id, title) FROM stdin;
1	Jerde Group
2	Zieme Inc
3	Hirthe Ltd
4	Littel, Senger and Gutmann
5	Schulist, Koepp and Bins
6	Veum, Dibbert and Towne
7	Brown PLC
8	Runte, Gutkowski and Bednar
9	Hickle Group
10	Conroy-Volkman
11	Gaylord, West and Hintz
12	Quitzon-Eichmann
13	Davis, Muller and Koelpin
14	Bartoletti LLC
15	Smith and Sons
16	Schmitt and Sons
17	Shields Ltd
18	Fadel LLC
19	Mann, Hegmann and Johns
20	Dach Ltd
21	Rice, Okuneva and Gottlieb
22	Pfannerstill-Aufderhar
23	Heidenreich-Bashirian
24	Dach-Kohler
25	Bruen-VonRueden
26	Hagenes and Sons
27	Jaskolski LLC
28	Ferry Inc
29	Dickinson, McClure and Hauck
30	Stiedemann Ltd
31	Fay Inc
32	Bosco, Terry and Veum
33	Fahey-Willms
34	Johns-Pacocha
35	Durgan Group
36	Heller-Sawayn
37	Greenholt-Orn
38	Boehm, Erdman and Langworth
39	Donnelly-Robel
40	O'Connell PLC
41	Leffler, Champlin and Torphy
42	Marquardt Ltd
43	Doyle LLC
44	Luettgen Inc
45	Hand-Dach
46	Becker, McKenzie and Jast
47	Rutherford, Cummings and Herman
48	Schultz Inc
49	Rau-Veum
50	Gleason-Pouros
51	Brown Group
52	Dooley Ltd
53	Reynolds Ltd
54	Gislason and Sons
55	Prosacco, Schmeler and Okuneva
56	Russel LLC
57	Gutmann-Parisian
58	Leuschke Ltd
59	Swaniawski Ltd
60	Kihn Inc
61	Moore Ltd
62	Yost Ltd
63	Weissnat-Ryan
64	Schuster, Marks and Walsh
65	Vandervort-Osinski
66	Konopelski, Hansen and Dare
67	Gottlieb PLC
68	Cronin, Cruickshank and Ledner
69	Hettinger LLC
70	Deckow Group
71	Casper and Sons
72	Purdy-Wiegand
73	Hoppe Group
74	Pouros-Sauer
75	Koepp-Abbott
76	Lesch-Stehr
77	Williamson LLC
78	Wilderman-Hackett
79	Buckridge-Dicki
80	Kuhlman-Powlowski
81	Casper-Franecki
82	Graham Group
83	Greenholt Inc
84	Langosh, Little and Fahey
85	McLaughlin, Reichel and Ebert
86	Swift Ltd
87	Littel Ltd
88	Ortiz-Reichert
89	Krajcik Ltd
90	Stanton, Boyer and West
91	Hirthe Group
92	Stiedemann, Beer and Rodriguez
93	Rohan, Considine and Walker
94	Fadel-Erdman
95	Tromp, Lang and Schneider
96	Runolfsdottir and Sons
97	Beahan PLC
98	Reinger-Lang
99	Gutkowski Ltd
100	Ryan, Olson and Nader
101	Hyatt, Bode and Rath
102	Ruecker-Ernser
103	Volkman, Schmitt and Muller
104	Hegmann, Brekke and Rippin
105	Boyer, Balistreri and Gulgowski
106	Davis, Kulas and Towne
107	Fay and Sons
108	Stracke-Bergstrom
109	Hodkiewicz-Haag
110	Stamm, Keeling and Pollich
111	Frami-McCullough
112	Hintz and Sons
113	Turner and Sons
114	Doyle PLC
115	Armstrong PLC
116	Nitzsche-Lemke
117	Marks-Kulas
118	Fisher-Kunze
119	Daniel, Medhurst and Miller
120	Koepp-Wunsch
121	Wilderman, Weber and Wolff
122	Fay Group
123	McCullough, Haag and Prosacco
124	Boyle-Upton
125	Carroll-Collier
126	O'Kon, Collier and Pacocha
127	Dooley Group
128	Cole, Herman and Cole
129	Legros, Mills and Weber
130	Bergstrom Ltd
131	Wiza, Ernser and Denesik
132	West PLC
133	Gutkowski-Pfannerstill
134	Greenholt-Champlin
135	Altenwerth, Powlowski and Spencer
136	Grant Ltd
137	Adams-Wolff
138	Reynolds Group
139	Hagenes, Bernier and Kub
140	Ferry Inc
141	Veum-Pagac
142	Wiegand-Collins
143	Jacobi-Mertz
144	Terry Inc
145	Hills, Bartoletti and Hirthe
146	Grimes-Miller
147	Berge Group
148	Schaden-Davis
149	Zboncak-Russel
150	Kuhn, Lubowitz and Carroll
151	Hodkiewicz-Kuhn
152	Haag, Smitham and Crooks
153	Batz-Kerluke
154	Howe and Sons
155	Barrows, Rowe and Littel
156	Balistreri-Gleason
157	Pouros, Boehm and Kessler
158	Carter Ltd
159	Rosenbaum-Rutherford
160	Parisian, Smitham and Hegmann
161	Gottlieb Ltd
162	Graham and Sons
163	Harber LLC
164	Von and Sons
165	Kiehn, Collins and Lynch
166	Nitzsche-McCullough
167	McLaughlin, Kohler and Bauch
168	Tillman, Schowalter and Harber
169	Cartwright-Ullrich
170	Johnston, Roberts and Considine
171	Kuhlman, Brekke and Klein
172	Satterfield-Lowe
173	Gutmann LLC
174	Cole, Haag and Nitzsche
175	Ondricka, Huels and Kautzer
176	Kertzmann Ltd
177	Waelchi-Franecki
178	O'Conner-McDermott
179	Smith-VonRueden
180	Schroeder Inc
181	Schinner-Carter
182	Pagac-Spinka
183	DuBuque-Hayes
184	Botsford-Schmitt
185	Hyatt LLC
186	Lowe-Adams
187	Ratke-Pollich
188	Rogahn-Okuneva
189	Marks-Swift
190	Keeling, Schimmel and Koss
191	Barrows Ltd
192	Ondricka, Fadel and Hilpert
193	O'Conner, Bogan and Kuhlman
194	Cummerata PLC
195	Daugherty, Weber and Crona
196	Gulgowski-Baumbach
197	Hickle Inc
198	Rodriguez and Sons
199	Bins-Paucek
200	Terry, Koelpin and Crooks
201	Wuckert PLC
202	Kutch, Sauer and Kunde
203	Fay-Eichmann
204	Heathcote, Flatley and Zieme
205	Blanda Group
206	Reinger-Bode
207	Larkin PLC
208	McLaughlin-Lowe
209	Schroeder PLC
210	Strosin-Block
211	Rogahn Ltd
212	Langworth-Abernathy
213	Ryan, Lowe and Johnston
214	Watsica, Krajcik and Connelly
215	Hansen, Collins and Braun
216	Berge and Sons
217	Denesik, Wiegand and Kunde
218	Bernhard Group
219	Rolfson Ltd
220	Kirlin-Ernser
221	Schultz, Feest and Cormier
222	Quitzon-Runte
223	Tillman Inc
224	Wiegand and Sons
225	Padberg Ltd
226	Kohler, Sanford and Hermann
227	Medhurst PLC
228	Jakubowski-Abshire
229	Murray, Jones and Rodriguez
230	Dietrich Ltd
231	O'Reilly Group
232	Paucek-Treutel
233	Sipes-Crona
234	Runolfsdottir-Casper
235	Dickens LLC
236	Swaniawski, Schmeler and Mueller
237	Pacocha Group
238	Hessel Inc
239	Block Ltd
240	Leannon PLC
241	Price Ltd
242	McDermott, Ruecker and Eichmann
243	Cartwright-Miller
244	Schuster-Morissette
245	McGlynn, Roob and Smitham
246	Wyman, Kohler and Grady
247	Batz Inc
248	Kub LLC
249	Feest Inc
250	Streich, Shields and Hudson
251	Hackett Ltd
252	Goldner, Bins and McClure
253	Erdman PLC
254	O'Connell, Stanton and Kulas
255	Donnelly Inc
256	Metz, Russel and Turcotte
257	Hauck, Herzog and Nolan
258	Zieme, Renner and Runolfsdottir
259	Hammes-Lang
260	Johnston-Marquardt
261	Abbott, Luettgen and Donnelly
262	Schaden-Bernier
263	Boyer-Will
264	Goldner-Stanton
265	Feil-Morar
266	Brown Inc
267	Bradtke-Smith
268	Stracke, Romaguera and Kling
269	Grady LLC
270	Mills and Sons
271	Raynor, Kautzer and Bins
272	Green-Dooley
273	Leannon, Bins and Kling
274	Kautzer LLC
275	Morissette-Wunsch
276	Stokes PLC
277	Roberts, Von and Grant
278	Larkin, Jerde and Fisher
279	Nienow PLC
280	Towne LLC
281	Jacobs LLC
282	Pollich LLC
283	McLaughlin-Dicki
284	Erdman, Lynch and Kub
285	Bernier-Christiansen
286	Kemmer Ltd
287	Howe Inc
288	Rice Inc
289	Rosenbaum, Smith and Stanton
290	Dare-Dooley
291	Harris and Sons
292	Bayer Ltd
293	Kris, Mosciski and Krajcik
294	Friesen PLC
295	Heathcote and Sons
296	Herzog, DuBuque and Cartwright
297	McGlynn, Towne and Windler
298	Kozey Group
299	Haley-Hagenes
300	Koelpin LLC
301	Dietrich-Ebert
302	Jast-O'Connell
303	Schiller-Abbott
304	Wiza LLC
305	Bartoletti-Wuckert
306	Koch-Nikolaus
307	Parisian, Predovic and Brekke
308	Schimmel, Bradtke and Spinka
309	Bergnaum Inc
310	Wehner, Towne and Gaylord
311	Bradtke LLC
312	Huels, Hane and Hayes
313	Ferry Inc
314	Howell Group
315	Lueilwitz-Sporer
316	Tillman-Crona
317	Volkman, Monahan and O'Conner
318	Schmidt-Hauck
319	Hudson, Davis and Spencer
320	Bergstrom, Bogisich and Wilderman
321	Lind Inc
322	Ullrich, Zieme and Fay
323	Corwin, Simonis and Kunze
324	Franecki-Cummerata
325	Botsford, Morar and Cummerata
326	Yundt, Nader and Schmidt
327	O'Reilly, O'Reilly and Swift
328	Lesch, McLaughlin and Walter
329	Little, Bogisich and Jaskolski
330	Bailey-Johnston
331	Breitenberg Ltd
332	Hammes, Williamson and Kuhn
333	Kreiger Group
334	Reichel, Waters and Renner
335	Lesch, Dare and Friesen
336	Schamberger, Luettgen and Gerlach
337	Satterfield Group
338	Hammes LLC
339	Conroy, Stanton and Harvey
340	Dach, Gleichner and Satterfield
341	Feil, Skiles and Braun
342	Maggio-Jaskolski
343	Murphy, Robel and Sanford
344	Schimmel-Ernser
345	Murray-Mante
346	Krajcik-Willms
347	Bashirian-Bruen
348	Pfeffer and Sons
349	Zieme-Kessler
350	Orn-Kreiger
351	Schneider, Welch and Smith
352	Larson PLC
353	Satterfield, Tremblay and Fay
354	Mraz Ltd
355	Hyatt LLC
356	Botsford and Sons
357	Fritsch LLC
358	Parker, VonRueden and Keeling
359	Heathcote PLC
360	Witting-Schoen
361	Effertz Group
362	Mertz, O'Kon and Zboncak
363	Jerde PLC
364	Prosacco, Blanda and Predovic
365	Williamson LLC
366	Prohaska-Kub
367	Collier, Ryan and O'Kon
368	Schulist Ltd
369	Medhurst, Batz and Parker
370	Fritsch-Cummings
371	Marks Inc
372	Cole, Beatty and Wolf
373	Corwin Group
374	Koelpin-Gulgowski
375	Zulauf-Little
376	Greenfelder-Cummings
377	Price Group
378	Frami, Kemmer and Konopelski
379	Ritchie PLC
380	Ankunding, Zboncak and Gleason
381	Hackett, Veum and Murazik
382	Hane-Okuneva
383	Lebsack-Corkery
384	Boehm-Lindgren
385	Dach, McKenzie and Crist
386	Ratke and Sons
387	Hoeger LLC
388	Murray, Aufderhar and Corwin
389	Leffler PLC
390	Kassulke, Walker and Pagac
391	Rutherford LLC
392	Huels and Sons
393	Kerluke, Huels and Thompson
394	Jerde, McDermott and Hirthe
395	Zemlak Inc
396	Walter, Heidenreich and Hahn
397	Kulas-Fadel
398	Towne LLC
399	Brekke, Zemlak and Mraz
400	Schmidt-Morar
401	Kerluke-Stark
402	Rice, Fay and Willms
403	King-Stanton
404	Reilly and Sons
405	Batz-Murazik
406	Monahan, Skiles and Rohan
407	Greenholt Group
408	Kihn PLC
409	Ziemann-Mueller
410	Cruickshank, Kovacek and Moore
411	Schulist-Block
412	Sawayn Ltd
413	Sanford Group
414	Koch-Gutmann
415	Dibbert Ltd
416	Moore Inc
417	Harris Group
418	Bailey, Krajcik and Prosacco
419	Farrell Inc
420	Ferry-Harvey
421	Armstrong LLC
422	Greenfelder-Denesik
423	Reilly, Ebert and Frami
424	Kozey-Kertzmann
425	Price-Kilback
426	Adams, Greenholt and Nader
427	Hoppe-Braun
428	Parker, Nolan and Koch
429	Hessel Ltd
430	Cartwright LLC
431	Kessler-Green
432	Reichel, Kovacek and Beier
433	Goodwin, Kilback and Bailey
434	Terry, Heaney and Yost
435	Murphy-Sauer
436	Gutmann, Rohan and Durgan
437	Dooley, Kihn and Padberg
438	Mueller LLC
439	Koepp, McLaughlin and Zemlak
440	Mertz Group
441	Gerhold-Turcotte
442	Hodkiewicz-Vandervort
443	Waters PLC
444	Krajcik, Wehner and Emmerich
445	Tromp-Reinger
446	Bins-Gleason
447	Luettgen LLC
448	McDermott Inc
449	Kiehn-Dibbert
450	Labadie, Nicolas and Hoeger
451	Hane-Bosco
452	Ernser, Rice and King
453	Reilly, Cremin and Anderson
454	Hettinger, Mraz and Thompson
455	Kreiger, Hodkiewicz and Rippin
456	Towne-Kutch
457	Kulas Inc
458	Lubowitz-Willms
459	Heathcote-Nikolaus
460	Corkery, Hintz and Stiedemann
461	Hahn, Roberts and Carter
462	Wiza, Rempel and Donnelly
463	Hessel-Berge
464	Beatty, Mohr and O'Connell
465	Adams-Veum
466	Wisozk, Cruickshank and Harris
467	Beer, Torphy and Ziemann
468	Purdy-Heathcote
469	Sipes, Walter and Kulas
470	Nader, Ullrich and Langworth
471	Rolfson PLC
472	Braun Ltd
473	Herman, Hessel and Anderson
474	Upton-Collins
475	Larkin-Hintz
476	Runte and Sons
477	Greenfelder LLC
478	Boehm-Upton
479	Swaniawski-Murphy
480	Heathcote-Ortiz
481	Barrows and Sons
482	Huels-O'Conner
483	Pollich-Boehm
484	Hahn Ltd
485	Ferry LLC
486	Larson-Beatty
487	Bogan-Kutch
488	Koepp, Rath and Block
489	Mayert, Carter and Schinner
490	Hartmann Inc
491	Hermann-Weimann
492	Gislason-Corkery
493	Hoppe, Pollich and Legros
494	Trantow, Windler and Pouros
495	Turcotte-Johnston
496	Gibson-Ryan
497	Walter-Green
498	Douglas Ltd
499	Casper, Pfannerstill and Halvorson
500	Watsica PLC
501	Steuber-Cummings
502	Lesch-Schaden
503	Jacobson, Gleason and Lang
504	Hermann, Orn and Smith
505	Ankunding, Dooley and Block
506	Paucek and Sons
507	Friesen-Haag
508	Schuppe, Toy and Zboncak
509	Barton, Klein and Ullrich
510	Kunde LLC
511	Huels LLC
512	Johns, Lind and Altenwerth
513	Friesen, Howell and Stiedemann
514	Champlin-Rosenbaum
515	Veum-Larkin
516	Wuckert, Hoppe and Bernier
517	Rau and Sons
518	Heathcote PLC
519	Rempel, Little and Effertz
520	Schroeder, Tromp and Mueller
521	Dicki, Emmerich and Batz
522	Klein Group
523	Langworth PLC
524	Botsford, Volkman and Monahan
525	Weber-Fay
526	Wolff LLC
527	Hill and Sons
528	Spinka Inc
529	Armstrong-Quitzon
530	Romaguera, Roob and Rutherford
531	Runolfsdottir-Tromp
532	Mertz-Nikolaus
533	Rutherford LLC
534	Grady, Denesik and Lesch
535	Jaskolski, Harber and Lueilwitz
536	Langosh Inc
537	Jacobi and Sons
538	Herman Ltd
539	Treutel-Haley
540	Orn, O'Hara and Kirlin
541	Jacobs, Stokes and Kemmer
542	Fisher and Sons
543	Botsford, O'Conner and D'Amore
544	Schultz-Bashirian
545	Reilly Group
546	Rau-Block
547	Harvey Ltd
548	Thompson Inc
549	Beahan Group
550	Hirthe-Kunde
551	Pacocha, Lang and Schinner
552	Lehner-Erdman
553	Bashirian and Sons
554	Buckridge-Cruickshank
555	Lehner, Marquardt and Will
556	O'Reilly Ltd
557	Stehr-Blick
558	Pouros-Steuber
559	Stokes-Franecki
560	Gleichner, Johnston and Gibson
561	Gottlieb-Rice
562	Treutel and Sons
563	Berge, Reichel and Jakubowski
564	Pacocha, Shields and Barrows
565	Casper and Sons
566	Berge-Kiehn
567	Swaniawski Ltd
568	Kohler and Sons
569	Botsford-Luettgen
570	Kuvalis, Heaney and McClure
571	Grimes and Sons
572	Lindgren LLC
573	Will, Heller and Kihn
574	Veum Group
575	Heidenreich, Murray and Blick
576	Wunsch-Corkery
577	Terry-Schulist
578	Nikolaus-Block
579	Hamill-Hagenes
580	Legros Group
581	Hamill-Rodriguez
582	Carroll-O'Hara
583	Gottlieb-Collins
584	Donnelly-Lemke
585	Fisher, Yundt and Gaylord
586	Feest Group
587	Kunde, Feil and Konopelski
588	Emmerich PLC
589	Bechtelar-Bins
590	Bradtke, Pfannerstill and Heidenreich
591	Spinka, Kassulke and Kautzer
592	Hoeger-Tillman
593	Leffler Ltd
594	Luettgen PLC
595	Hayes-Cronin
596	Dooley-Nitzsche
597	Beer, Cassin and West
598	McLaughlin, Lueilwitz and Hirthe
599	Swaniawski, Nienow and Crooks
600	Bahringer PLC
601	Haag-Friesen
602	Rippin, Wolff and DuBuque
603	Von LLC
604	Hegmann, Feil and Wuckert
605	Kessler and Sons
606	Schowalter-Kshlerin
607	Stroman Ltd
608	Feeney, Morar and Jakubowski
609	Schowalter, Rogahn and Becker
610	Casper LLC
611	Rath Ltd
612	Sawayn, Lemke and Kautzer
613	Lueilwitz, Williamson and Grant
614	Hoeger-Bergstrom
615	Orn Group
616	Oberbrunner Inc
617	Willms Ltd
618	Moore, Gibson and Herman
619	Kris-Legros
620	Stanton Ltd
621	Schmitt, Rau and Stehr
622	Vandervort, Bogisich and Fay
623	Mraz, Schaden and Bashirian
624	Kuhn PLC
625	Collins-Hudson
626	Haag, Kuhn and Rohan
627	Auer-Hahn
628	Mante Ltd
629	Lynch-Fay
630	Schmitt, Herzog and Towne
631	Predovic PLC
632	DuBuque and Sons
633	Stoltenberg, Mann and Koss
634	Daniel Ltd
635	Ruecker, Heathcote and Tillman
636	Pouros-Pfeffer
637	Towne Inc
638	Fahey and Sons
639	Mueller Group
640	Hyatt, Langworth and Lockman
641	Labadie, Gibson and Christiansen
642	Maggio-Murphy
643	Shields, Brekke and Swift
644	Kassulke LLC
645	Brekke, Pfannerstill and Medhurst
646	Simonis-Schumm
647	Hand Ltd
648	Maggio, Dibbert and Ledner
649	Rosenbaum-Kuhlman
650	Welch-Schmidt
651	Prosacco-Lindgren
652	Macejkovic-Ward
653	Sipes, Rippin and Walter
654	Effertz, Lueilwitz and Turner
655	Orn Inc
656	Connelly PLC
657	Prosacco, Rau and Anderson
658	Wolff-Streich
659	Hintz-Bartell
660	Morar, Wilderman and Berge
661	Schultz, Braun and Wyman
662	Conroy-Roob
663	Hegmann-Crist
664	Wyman, Howell and Cruickshank
665	Connelly Group
666	Kerluke, Baumbach and Kiehn
667	Jerde LLC
668	Jones, Murphy and Beahan
669	Farrell LLC
670	Dach, McCullough and Mertz
671	Kreiger Group
672	Hyatt-Jaskolski
673	Pacocha Inc
674	Treutel, Champlin and Haley
675	McDermott and Sons
676	Miller, VonRueden and Gleichner
677	Prohaska-Turner
678	Carroll Group
679	Kohler, Steuber and Bailey
680	Effertz-Hamill
681	Walker Inc
682	Hill-Heaney
683	Kris, Barton and Pacocha
684	Effertz-Volkman
685	Welch, Leuschke and Hauck
686	Tremblay Ltd
687	Mayer Group
688	Miller Inc
689	McDermott-Harris
690	Harvey Ltd
691	Collier, Schuster and Heaney
692	Corwin-Heidenreich
693	Satterfield, Hand and Kihn
694	VonRueden-Flatley
695	Schamberger-Marks
696	Ernser and Sons
697	Hayes and Sons
698	Wintheiser, Aufderhar and Hagenes
699	Skiles PLC
700	Reichert, Leannon and Rosenbaum
701	Swaniawski, Ward and Huel
702	Kiehn Ltd
703	Koepp-Kub
704	Moen Inc
705	Feil, Hickle and Schamberger
706	Walter and Sons
707	O'Kon, Dickens and Rice
708	Bogisich PLC
709	Schimmel PLC
710	Kiehn, Fisher and Hand
711	Padberg and Sons
712	Gibson, Ondricka and Ledner
713	Pacocha Ltd
714	Stracke and Sons
715	McClure, Cormier and Hirthe
716	Kovacek, Grady and Heller
717	Bogan-Wolf
718	Medhurst-Schiller
719	Jacobson Ltd
720	Turner and Sons
721	Howell, Shanahan and Stanton
722	Pollich LLC
723	Kovacek, Konopelski and Cartwright
724	Oberbrunner Inc
725	Jacobson, Greenfelder and Miller
726	Tremblay, Gleichner and Oberbrunner
727	Grimes, Hilpert and Langworth
728	Funk-Okuneva
729	Hermiston, Fritsch and Crist
730	Hand, Bergnaum and Hane
731	Schmitt-Frami
732	Graham, Effertz and Vandervort
733	Barrows LLC
734	Kessler, Kiehn and Wilkinson
735	Haag-Gleason
736	Wolf-Prosacco
737	Klein-Mraz
738	Halvorson Inc
739	Roob Group
740	Keebler, Stokes and Hintz
741	Ward-Homenick
742	Bradtke-Block
743	Romaguera-Reichert
744	VonRueden, Bosco and Thiel
745	Rohan, Huels and Padberg
746	Hansen, Cummings and Schoen
747	Lubowitz-Runolfsson
748	Weissnat and Sons
749	McClure, Heathcote and Rolfson
750	Ankunding-Steuber
751	Sawayn, Corwin and Ratke
752	McLaughlin, Oberbrunner and Emard
753	Lakin, Kohler and Larson
754	Senger Inc
755	Nader-Rohan
756	Satterfield LLC
757	Aufderhar-Cronin
758	O'Conner, Beatty and Okuneva
759	Hane, Fritsch and Deckow
760	Upton-Kemmer
761	Abernathy Inc
762	Ryan, Wolf and Littel
763	Gerhold, Hettinger and Morissette
764	West-Johns
765	Lesch, Boyer and Haag
766	Satterfield Ltd
767	Nicolas-Hammes
768	Kilback, Monahan and Lynch
769	Steuber and Sons
770	Medhurst-Green
771	Luettgen LLC
772	Heidenreich, Nienow and Hand
773	Heathcote-McKenzie
774	O'Hara PLC
775	Kohler, Lindgren and Hagenes
776	Crist-Walsh
777	Abernathy Group
778	Strosin Group
779	Batz, Bogan and Kunze
780	Lesch-Mertz
781	Renner, Greenfelder and Kertzmann
782	Leuschke, Nolan and Bartoletti
783	Boyer-Strosin
784	Conroy-Zemlak
785	Pfeffer LLC
786	Prohaska-Jacobi
787	Beahan and Sons
788	Hoeger, Connelly and Steuber
789	Mitchell-Mueller
790	Pouros, Wilkinson and Aufderhar
791	Okuneva, Hahn and Marvin
792	Weimann PLC
793	Boehm-Skiles
794	Jerde Group
795	Cole, Tillman and Leffler
796	VonRueden-Borer
797	Erdman LLC
798	Wolf, Veum and Wisozk
799	Kunze-Weimann
800	Kerluke-Lockman
801	Kunze-Effertz
802	Grant Inc
803	Bins, Will and Kuphal
804	Lang-Lindgren
805	Gleichner-Champlin
806	Rath-Schuppe
807	Cronin-Oberbrunner
808	Brekke, Beahan and Boehm
809	Kirlin PLC
810	Brekke, Ryan and Kub
811	Roob, Greenholt and Leuschke
812	Stehr-Williamson
813	Jacobi-D'Amore
814	Lebsack, Smitham and Douglas
815	Gutmann Ltd
816	Wuckert-Rohan
817	Kemmer Ltd
818	Hamill, Lockman and Wuckert
819	Kihn and Sons
820	Wisoky and Sons
821	Schiller, Durgan and Mayert
822	Strosin Ltd
823	Okuneva LLC
824	Leannon Ltd
825	Bogan LLC
826	Cummerata Ltd
827	Schinner Ltd
828	Satterfield and Sons
829	Klein-Roberts
830	Quigley Group
831	Lakin-Dicki
832	Pacocha, Stamm and Schaden
833	Kertzmann Ltd
834	Kirlin, Collier and Bernier
835	Casper PLC
836	Bartoletti PLC
837	Bradtke, Wehner and McDermott
838	Dicki, Reichert and Weissnat
839	Mosciski-Strosin
840	Mohr Inc
841	Conroy Ltd
842	Pfannerstill, Spinka and Konopelski
843	Green and Sons
844	Torphy-Kozey
845	Fisher Inc
846	Green Inc
847	Gerlach Inc
848	Thompson-Hermiston
849	Cruickshank-Bosco
850	Cronin PLC
851	Vandervort, Kassulke and Rowe
852	Labadie, Bogan and Bosco
853	McKenzie Ltd
854	Reichert-McDermott
855	Collier LLC
856	Ward-Huel
857	Mraz, Parker and Hill
858	Hoeger-Herman
859	Hermiston Ltd
860	Kirlin-Hagenes
861	Wisoky-Block
862	Schaden-Kilback
863	Bauch, Abbott and King
864	Wyman, Feil and Carroll
865	Hilpert-Wolf
866	Bogan-Steuber
867	Deckow and Sons
868	Jacobson Group
869	Prohaska, Prosacco and Rempel
870	Heathcote, Pfannerstill and Smith
871	Heaney and Sons
872	Anderson-Koelpin
873	Moen-Stiedemann
874	Lakin-Leffler
875	Larson, Johnson and Yundt
876	Brown Group
877	Stracke-Cronin
878	Ritchie, Tromp and Ebert
879	Johns, Huels and Pacocha
880	Zieme, Boyer and Buckridge
881	Pfannerstill, Donnelly and Prosacco
882	Mills, Wilderman and Stroman
883	Bednar-Lueilwitz
884	Champlin-Sporer
885	Kreiger PLC
886	Mayer-Nitzsche
887	Gorczany and Sons
888	Crooks LLC
889	Murphy, Treutel and Block
890	Olson Group
891	Crooks, Mante and Heaney
892	Heathcote LLC
893	Shields-Satterfield
894	Pfannerstill, Rowe and Schoen
895	Kemmer, Collier and Bernier
896	Durgan, Abbott and Zemlak
897	Rice, Hessel and Walsh
898	McClure, Windler and Towne
899	Zemlak, Ullrich and Bahringer
900	Wisozk-Bartoletti
901	Harris Ltd
902	Jacobs-Kiehn
903	Block PLC
904	Marvin-Conn
905	Braun Group
906	Dietrich, Waelchi and Grant
907	Tillman, Langosh and Pagac
908	Hills, Orn and Haley
909	Kshlerin, Auer and Paucek
910	Reinger Inc
911	Treutel, Bechtelar and Hammes
912	Kuphal-Kautzer
913	Barrows Group
914	Nitzsche-Frami
915	Balistreri-Frami
916	Swift-Kozey
917	Stoltenberg LLC
918	Walker and Sons
919	Wiza and Sons
920	Toy LLC
921	Rice-Gleichner
922	Runolfsson Group
923	O'Conner, Hirthe and Armstrong
924	Bins, Considine and Howe
925	Nikolaus PLC
926	Pacocha, Kirlin and Gibson
927	Frami and Sons
928	Feeney PLC
929	Pacocha LLC
930	Block-Kerluke
931	Schimmel, Haag and Wiza
932	Zulauf-Welch
933	Gleason-Stracke
934	Olson, Harvey and Jakubowski
935	Kohler-Rutherford
936	Hills Group
937	Bode, Homenick and Grady
938	Huel, Walker and Emmerich
939	Maggio Ltd
940	Streich-Abbott
941	Braun LLC
942	Torp and Sons
943	Von, Zulauf and Waelchi
944	Ziemann LLC
945	Weimann, Eichmann and Murray
946	Ernser, Wilkinson and Schultz
947	Treutel Inc
948	Bergnaum, Kihn and Feeney
949	Nienow, Jacobi and Bahringer
950	Medhurst-Rosenbaum
951	Kassulke-Rogahn
952	Pfeffer-Zieme
953	Crooks, Glover and Jast
954	Pagac, Pacocha and Howell
955	Lynch Inc
956	Johns-Becker
957	McClure, Willms and Douglas
958	Kilback, Beahan and Hermann
959	Erdman-Schulist
960	Turcotte LLC
961	Kessler, Harber and Williamson
962	Daugherty Inc
963	Franecki, Gorczany and Jenkins
964	Vandervort, Hodkiewicz and Schaden
965	Gulgowski-Mertz
966	Morissette Group
967	Johnson PLC
968	Heidenreich-Adams
969	Miller Inc
970	Metz-Oberbrunner
971	Moore, Ziemann and Ankunding
972	Stiedemann Group
973	Sauer, Bogisich and Harvey
974	Lebsack-Bailey
975	Quitzon-Keebler
976	Rosenbaum Inc
977	Konopelski Ltd
978	Willms, Schuster and Farrell
979	Emmerich and Sons
980	Farrell LLC
981	Morar Ltd
982	Johns LLC
983	Sanford, Howe and Parisian
984	Zemlak-Nienow
985	Nienow-Schmeler
986	Lesch-Heller
987	Medhurst, Windler and Wuckert
988	Jakubowski-Bruen
989	McClure-Douglas
990	Kulas Ltd
991	Wintheiser-Howell
992	Swaniawski-Stoltenberg
993	Rice, Dietrich and Kuphal
994	Muller, Rowe and Keeling
995	Emmerich-Larkin
996	Pouros LLC
997	Bartoletti-Jones
998	Glover-Grady
999	Nitzsche and Sons
1000	Rohan, Leannon and Ernser
1001	Harber-Jacobi
1002	Hane, Homenick and Howell
1003	Mraz, Rutherford and Jakubowski
1004	Braun Inc
1005	Sawayn-Lowe
1006	Kozey, Marks and Larkin
1007	Lynch Group
1008	Herzog, Durgan and Roob
1009	Frami LLC
1010	Torphy-Cronin
1011	Lubowitz-Schowalter
1012	Padberg-Nolan
1013	Hilpert, Keebler and Gottlieb
1014	Boyle-Nader
1015	DuBuque and Sons
1016	Streich Ltd
1017	Cole-Gusikowski
1018	Tremblay-Pfannerstill
1019	Beer-Wuckert
1020	Bruen, Beahan and Wintheiser
1021	Crist Group
1022	Tillman, Treutel and Luettgen
1023	Macejkovic-Kuvalis
1024	Carroll, Skiles and Conroy
1025	Wehner-Abshire
1026	Terry Inc
1027	Osinski-Pollich
1028	Mueller Group
1029	Nolan, Balistreri and Williamson
1030	Koss-Rippin
1031	Hahn Ltd
1032	Sipes, Hane and Feil
1033	Macejkovic Group
1034	Wiegand, Schowalter and Ankunding
1035	Lindgren PLC
1036	Champlin and Sons
1037	Cassin Group
1038	Kiehn PLC
1039	Bartell Group
1040	Baumbach, Herzog and Steuber
1041	Collins, Emmerich and Grady
1042	Frami Group
1043	Rodriguez, Rath and Stoltenberg
1044	Hill-Gulgowski
1045	Becker-Littel
1046	O'Reilly, Harvey and Bartell
1047	Keebler-Dietrich
1048	Simonis and Sons
1049	Padberg-Lynch
1050	O'Kon, Christiansen and Gorczany
1051	Jast-Walter
1052	Weimann, Romaguera and Ruecker
1053	Howell, Stark and Hayes
1054	Satterfield-Gaylord
1055	Buckridge-Larson
1056	Gutmann, Stracke and McDermott
1057	Macejkovic LLC
1058	Bahringer-O'Reilly
1059	Wyman-Hand
1060	Orn, Schumm and Wilkinson
1061	Gutmann, Maggio and Gutmann
1062	Armstrong-Waters
1063	Haag PLC
1064	Lehner, Crist and Romaguera
1065	Parisian-Hudson
1066	Corwin-Crooks
1067	Ratke Ltd
1068	Champlin, Wisoky and Rempel
1069	Gerhold and Sons
1070	Jacobson Inc
1071	Runte-Marquardt
1072	Rodriguez, Gutmann and Murray
1073	Wilkinson LLC
1074	Kling-Boyle
1075	Huels, Adams and Von
1076	Rodriguez-Wisozk
1077	White-Mertz
1078	Wolf-Ullrich
1079	Wintheiser Ltd
1080	Stokes, Morissette and Kerluke
1081	Bogisich-Hane
1082	Gislason, Casper and Bailey
1083	Eichmann Group
1084	Nader-Hand
1085	Gaylord-Bergstrom
1086	Gerhold and Sons
1087	Dickens-Kshlerin
1088	Cronin PLC
1089	Cartwright, Beier and Smitham
1090	Jones PLC
1091	Hayes Inc
1092	McLaughlin, Kozey and McLaughlin
1093	Brakus-Medhurst
1094	Rippin, Zulauf and Kerluke
1095	Haley-Vandervort
1096	Towne, Johnston and Anderson
1097	Blanda, Howe and Zieme
1098	Mayert, Harber and Hammes
1099	Hill, Feeney and Rodriguez
1100	Stiedemann Group
1101	Welch, Towne and Harris
1102	Mraz Ltd
1103	Baumbach, Kreiger and Toy
1104	Johnston-Gulgowski
1105	Abbott, Schimmel and Heathcote
1106	Yundt-Hauck
1107	Windler-Mitchell
1108	D'Amore-Gutkowski
1109	Morar Inc
1110	VonRueden-Littel
1111	Fritsch, Dicki and Senger
1112	Wiza-Mann
1113	Yost Group
1114	Kiehn, Miller and Waters
1115	Mitchell LLC
1116	Gusikowski Ltd
1117	Langworth Group
1118	Huel-Raynor
1119	Wiza, Wolff and Reinger
1120	Denesik, Wolf and Lockman
1121	Boyer-McKenzie
1122	Gulgowski and Sons
1123	Harber, Reichel and Erdman
1124	Watsica-Klocko
1125	Ebert-Kautzer
1126	Yundt-Koepp
1127	Abbott-Kris
1128	Rosenbaum-Larkin
1129	Kohler Ltd
1130	Kutch, Zemlak and Howe
1131	Wintheiser, Langosh and McCullough
1132	Monahan-Jones
1133	Nitzsche, Friesen and Beatty
1134	Marks and Sons
1135	Quigley, Williamson and Walker
1136	Bode-Kihn
1137	Turcotte, Ullrich and Bauch
1138	Olson, Gaylord and Gerlach
1139	Heller, Olson and Champlin
1140	Kreiger Ltd
1141	Turcotte and Sons
1142	Muller-Bayer
1143	Konopelski, Bradtke and Hoeger
1144	Moore-Rice
1145	Smith, Schneider and Abbott
1146	Wolff-Jones
1147	Romaguera, Gibson and Watsica
1148	Robel and Sons
1149	Shanahan-Romaguera
1150	Ryan-McGlynn
1151	Parker-Lebsack
1152	Carroll Group
1153	Zieme and Sons
1154	Orn Inc
1155	Torphy, Blanda and Huel
1156	Glover Ltd
1157	Haag Inc
1158	Stehr-Stanton
1159	Lakin Group
1160	Botsford-Kirlin
1161	Braun and Sons
1162	Jaskolski LLC
1163	Romaguera-Green
1164	Zboncak, Herzog and White
1165	Reichert-Stroman
1166	Reilly and Sons
1167	Bahringer and Sons
1168	Beahan Ltd
1169	Bogan-Bosco
1170	Gottlieb LLC
1171	Koelpin, Schuppe and Kilback
1172	Prohaska Inc
1173	Paucek, Dibbert and Stoltenberg
1174	Heller-Hills
1175	Swift-Sporer
1176	Dicki and Sons
1177	Will, Mayer and Rutherford
1178	Wiza, Stoltenberg and Jacobi
1179	Brown-Fritsch
1180	Schuppe, Lakin and Champlin
1181	Quigley LLC
1182	Reilly, Kemmer and Cruickshank
1183	Oberbrunner-Reilly
1184	Ziemann-Mosciski
1185	Homenick-Goodwin
1186	Halvorson-Schaden
1187	Hamill-Kerluke
1188	Crona-Jones
1189	Kuhic, Mertz and Mohr
1190	Ward Group
1191	Jerde, Nicolas and McCullough
1192	Rice LLC
1193	Willms, Ward and Weissnat
1194	Goldner, Macejkovic and Thiel
1195	McLaughlin-Doyle
1196	Champlin-Bailey
1197	Dach Ltd
1198	Pouros Group
1199	Quitzon-Prohaska
1200	Grant PLC
1201	Hahn-Crona
1202	Marquardt, Hettinger and Schmeler
1203	Dooley-Mitchell
1204	Bosco-Stokes
1205	Kuhlman, Conn and Crist
1206	Orn-Weissnat
1207	Gibson Group
1208	VonRueden PLC
1209	Sipes-Ratke
1210	Weber-Williamson
1211	Gaylord-Greenholt
1212	Kautzer-Morissette
1213	Heaney, Jaskolski and Prohaska
1214	Bernhard Ltd
1215	Kunze PLC
1216	Hackett-Wolf
1217	Emmerich PLC
1218	Rolfson Group
1219	Harris-Kuhlman
1220	Heidenreich, Baumbach and Abshire
1221	Kuhlman and Sons
1222	Stehr, Boyle and Blanda
1223	Sauer, Brekke and Lang
1224	Kohler, Klein and Rohan
1225	Mraz-Sauer
1226	Abbott PLC
1227	Abernathy, Johnson and Spinka
1228	Kuhn and Sons
1229	Schowalter Ltd
1230	Ferry Ltd
1231	Heller Inc
1232	McLaughlin PLC
1233	Stracke PLC
1234	Lindgren-D'Amore
1235	Bauch, Orn and Jast
1236	Strosin-Batz
1237	Feest-Marquardt
1238	Prosacco, Davis and Volkman
1239	Kohler, Zieme and Hayes
1240	Kautzer, Lemke and Fay
1241	Cronin, Padberg and Rath
1242	Crooks-Jenkins
1243	Will-Kessler
1244	Douglas-Braun
1245	Rohan Inc
1246	Watsica-Hilpert
1247	Robel Inc
1248	Schaden-Kunze
1249	Borer Ltd
1250	Gibson Inc
1251	Miller-Orn
1252	Cartwright and Sons
1253	Marks-Abshire
1254	Dickinson Inc
1255	Ondricka-Purdy
1256	Wiegand-Runte
1257	Grant-Upton
1258	Kub, Kreiger and Schneider
1259	Dietrich, Russel and Dibbert
1260	Haag-Herman
1261	Schamberger Ltd
1262	Mueller and Sons
1263	Green Ltd
1264	Abernathy and Sons
1265	Kunde, Hammes and Pollich
1266	Brekke and Sons
1267	Hickle, Howell and Turner
1268	Johnson, Kshlerin and Harvey
1269	Pacocha, Pollich and Lakin
1270	Friesen-Nitzsche
1271	Nicolas LLC
1272	Graham Ltd
1273	Ernser-Pacocha
1274	Legros-Will
1275	Hilpert, Olson and Erdman
1276	Christiansen-Hoeger
1277	Gulgowski-West
1278	Kilback, Hudson and Tillman
1279	Hoeger PLC
1280	Muller Ltd
1281	Hoppe, Tremblay and McClure
1282	Balistreri Ltd
1283	Kessler-Trantow
1284	McKenzie Group
1285	Wolff-Kirlin
1286	Weber, Spinka and Shields
1287	Gutkowski, Schneider and Simonis
1288	Carter Inc
1289	Luettgen-Herzog
1290	Ritchie, Cruickshank and Bradtke
1291	Prosacco-Metz
1292	Botsford and Sons
1293	Orn, Okuneva and Kris
1294	Cruickshank-Boehm
1295	Sawayn, Barton and Sporer
1296	Gulgowski, O'Conner and Borer
1297	Beahan, McKenzie and Murazik
1298	Homenick, Robel and Waelchi
1299	Lynch Inc
1300	Sanford Inc
1301	Beier PLC
1302	Ondricka Inc
1303	Fisher, Cummings and Balistreri
1304	Ruecker, Botsford and Schroeder
1305	Hartmann-Kris
1306	McGlynn-Heidenreich
1307	Zulauf-Stamm
1308	Block Group
1309	West Inc
1310	Runolfsdottir and Sons
1311	Larkin and Sons
1312	Barrows LLC
1313	Auer Ltd
1314	Wolf-Huels
1315	Bogisich, Rolfson and Rice
1316	Dooley-Kozey
1317	Rowe, Feest and Doyle
1318	Pfannerstill, Schaefer and Sipes
1319	Lowe Group
1320	Kub, Skiles and Ortiz
1321	Marquardt PLC
1322	Thompson, Tromp and Koepp
1323	Dicki Inc
1324	Berge, Luettgen and Gutmann
1325	Hill and Sons
1326	Mueller, Champlin and Goldner
1327	Witting-Christiansen
1328	Brakus, Lynch and Ankunding
1329	Hermiston Inc
1330	Rau, Weimann and Schmeler
1331	Gutkowski-Becker
1332	VonRueden-Adams
1333	Stehr-Predovic
1334	Grady, Lockman and Schmitt
1335	Marvin-Hammes
1336	Botsford, Daniel and Wisozk
1337	Schinner-Graham
1338	Casper and Sons
1339	Barton, Raynor and Haag
1340	Boehm, Koepp and Pagac
1341	Ullrich-Goyette
1342	Cartwright-Ratke
1343	Abernathy Group
1344	Veum Group
1345	Ullrich and Sons
1346	Lueilwitz PLC
1347	Wisozk PLC
1348	Grady, Miller and Kulas
1349	Quigley-Grady
1350	Gleason, Nader and Witting
1351	Heaney-Willms
1352	Johns, Reinger and Hodkiewicz
1353	Abernathy, Konopelski and Maggio
1354	Jerde-Wuckert
1355	Stiedemann LLC
1356	Goyette-Luettgen
1357	Towne-Cruickshank
1358	Zemlak-Eichmann
1359	Becker-Kilback
1360	Blanda Ltd
1361	Marvin-Schuppe
1362	Ratke and Sons
1363	Wunsch and Sons
1364	McLaughlin-Wyman
1365	Kuhn-Renner
1366	Kulas, Pfannerstill and Funk
1367	Kling LLC
1368	Braun, Luettgen and Kulas
1369	Kulas, Ebert and Ebert
1370	Schoen-Quigley
1371	Koch, Yundt and Weimann
1372	Windler, Swaniawski and Kilback
1373	Fisher, Kiehn and Dach
1374	Emmerich, Wintheiser and Raynor
1375	Klein-Harvey
1376	Ruecker-Mills
1377	Kuhn-Johnson
1378	Macejkovic Group
1379	Ullrich, Anderson and Bogisich
1380	Huel, Bergstrom and Zemlak
1381	Wiza and Sons
1382	McDermott-Cassin
1383	Harvey Ltd
1384	Harris, Connelly and Torphy
1385	Crist Group
1386	Hand, Sawayn and Hermann
1387	Dicki-Buckridge
1388	Koss-Mitchell
1389	Abbott PLC
1390	Hansen-Ledner
1391	Gorczany, Cronin and Lang
1392	Spinka, Hoeger and McLaughlin
1393	Lindgren Inc
1394	Hirthe Group
1395	Spencer PLC
1396	Schneider, Jacobson and Kassulke
1397	Lindgren-Gusikowski
1398	Schuster-Labadie
1399	Zieme Inc
1400	Gerlach Inc
1401	Thompson, Hansen and Gorczany
1402	Kunze PLC
1403	Corkery-Reichert
1404	Reilly Group
1405	Lesch PLC
1406	Waters, Wilderman and Bode
1407	Hettinger Inc
1408	Hauck, Stamm and Halvorson
1409	McDermott, Abshire and Steuber
1410	Gleason-Adams
1411	Beahan-Ledner
1412	Bashirian, Gibson and Maggio
1413	Fisher-Spinka
1414	Wisoky-Stracke
1415	Durgan-Kunde
1416	Nikolaus-Bailey
1417	Weissnat-Oberbrunner
1418	Feil LLC
1419	Fahey, Walsh and Lind
1420	Wiza-Lang
1421	Vandervort-Towne
1422	Bartell, Champlin and Flatley
1423	Becker-Bosco
1424	Carter, Deckow and Langworth
1425	Fritsch-Stoltenberg
1426	Tromp, Kub and McLaughlin
1427	Breitenberg, Sauer and Farrell
1428	Konopelski, Strosin and Oberbrunner
1429	Swaniawski, Hauck and Abbott
1430	Fay, Lowe and Jakubowski
1431	Nolan and Sons
1432	Lindgren-Turner
1433	Douglas-Bashirian
1434	Raynor, Thompson and Torp
1435	O'Connell, Steuber and Gusikowski
1436	Vandervort LLC
1437	Klocko-Towne
1438	Funk Group
1439	Daniel-Koch
1440	Parisian-Wuckert
1441	Price-Keeling
1442	Heathcote-Goodwin
1443	D'Amore and Sons
1444	Rempel LLC
1445	Gulgowski-Spencer
1446	Olson, Swift and Daugherty
1447	Christiansen, Becker and Buckridge
1448	Rau, Grimes and Mohr
1449	Blick-Hackett
1450	Streich Group
1451	Bailey-Schowalter
1452	Swaniawski, Bednar and Stokes
1453	Abbott, Ziemann and Streich
1454	Schoen Group
1455	Armstrong Inc
1456	Beahan, Shanahan and Rolfson
1457	Towne-Reichel
1458	Jacobson Inc
1459	Connelly-McLaughlin
1460	Pouros-Donnelly
1461	Hagenes-Douglas
1462	Haag and Sons
1463	Kemmer-Huel
1464	Schoen Ltd
1465	Funk-Schaefer
1466	Kertzmann LLC
1467	Metz, VonRueden and Kilback
1468	Murphy, Roberts and Mann
1469	Reilly, Kling and Buckridge
1470	Spinka-Nitzsche
1471	Littel Inc
1472	Koch Group
1473	Waelchi-Adams
1474	Hoppe and Sons
1475	Mraz LLC
1476	Aufderhar Inc
1477	Mills Inc
1478	Kshlerin-Grant
1479	Daniel, White and Olson
1480	Daugherty PLC
1481	Tremblay and Sons
1482	Goyette, Kertzmann and Pacocha
1483	Botsford and Sons
1484	Volkman Ltd
1485	Torp, Schiller and Hodkiewicz
1486	Kemmer Ltd
1487	Kuhlman, Cole and Ritchie
1488	King, Klocko and Abshire
1489	Zemlak-Thiel
1490	Schroeder-Larson
1491	Kulas, Romaguera and McClure
1492	Abernathy, Bailey and Hirthe
1493	Cormier-Bednar
1494	Miller, Monahan and McKenzie
1495	Kunze Group
1496	Halvorson-Schaefer
1497	Lind-Feil
1498	Kuvalis-Douglas
1499	Stark, Halvorson and Treutel
1500	Pacocha, Swaniawski and Lehner
1501	Ebert Ltd
1502	Wisozk and Sons
1503	Howe PLC
1504	Kautzer Ltd
1505	Lesch Inc
1506	Witting-Morissette
1507	Jacobs-Windler
1508	Heathcote-Miller
1509	Boyer, Cronin and Bartell
1510	Corkery-Wehner
1511	Johns-Pacocha
1512	Bashirian Inc
1513	Macejkovic-Davis
1514	Mitchell Group
1515	Thompson, Cole and Reinger
1516	Heller-Harvey
1517	Bins LLC
1518	Renner, VonRueden and Reichert
1519	Wiza LLC
1520	Willms, Tromp and Daniel
1521	Cummings, O'Reilly and Russel
1522	Mills Group
1523	Friesen-Grant
1524	Marvin-Kohler
1525	Zemlak, Quigley and Torphy
1526	Roob, Harris and Greenholt
1527	Kilback, Botsford and Sporer
1528	O'Reilly, Kulas and Romaguera
1529	Pagac-Konopelski
1530	Jacobs-Hills
1531	Treutel, Braun and Upton
1532	Haley and Sons
1533	Swaniawski-Crona
1534	Greenfelder-Cummings
1535	Lind, Murphy and Dietrich
1536	Ferry Group
1537	Gerhold LLC
1538	Cummings, Harvey and Hammes
1539	Monahan, Emard and Rogahn
1540	Waters PLC
1541	Olson-Morissette
1542	Turcotte Inc
1543	Luettgen-Larkin
1544	Vandervort Ltd
1545	Goodwin Ltd
1546	McDermott-Bahringer
1547	Hand, Flatley and Orn
1548	Ziemann, Blick and Jacobi
1549	Schmeler-Towne
1550	Dibbert, Sauer and Funk
1551	Koepp, Koch and Dach
1552	Ernser-Kohler
1553	Tillman-Rutherford
1554	Lubowitz-Kovacek
1555	Kling, Lynch and Hansen
1556	Ward-Mills
1557	Rippin, Muller and Borer
1558	O'Kon, Schultz and Mohr
1559	Lebsack and Sons
1560	Kuhn-Reichel
1561	Donnelly, Kiehn and Denesik
1562	Bashirian, Hegmann and Romaguera
1563	Gottlieb, Sipes and Hegmann
1564	Schultz Group
1565	Harvey, Adams and Schmitt
1566	Hudson-Barrows
1567	Heller, Olson and Schroeder
1568	Rempel, Schimmel and Skiles
1569	Waelchi LLC
1570	Denesik PLC
1571	Emard, Goodwin and Borer
1572	Torp, Keebler and Robel
1573	Aufderhar, Skiles and Hammes
1574	Runolfsson-Nikolaus
1575	Kihn-Pfeffer
1576	Schaden, Adams and Ullrich
1577	Kris, Kihn and Hayes
1578	Upton, Hickle and Schmeler
1579	Gutkowski PLC
1580	Larkin, Kiehn and Greenfelder
1581	Morissette and Sons
1582	Blanda-Howell
1583	McGlynn, Fahey and O'Kon
1584	Hettinger, Wilderman and Powlowski
1585	Padberg-Schaden
1586	Johnston-Langosh
1587	Nikolaus Group
1588	Kuhic, Harvey and Stokes
1589	Glover-Wilderman
1590	Kessler, Miller and Herman
1591	Kovacek LLC
1592	Hartmann-Ledner
1593	McKenzie-Thompson
1594	Mann, Becker and Parisian
1595	Kunze Group
1596	Mohr-Abbott
1597	Mohr PLC
1598	Runolfsson, Wintheiser and Breitenberg
1599	McKenzie, Muller and Leffler
1600	Torphy, Padberg and Schaefer
1601	Johns-Ondricka
1602	Lesch, Reilly and Cummings
1603	McCullough-O'Connell
1604	Mayert Group
1605	Rogahn Ltd
1606	Hagenes PLC
1607	Dare and Sons
1608	Barrows-Marquardt
1609	Erdman, Stiedemann and Rau
1610	Reichert LLC
1611	Price-Lakin
1612	Pollich and Sons
1613	Mayer, Hirthe and Kiehn
1614	Purdy Group
1615	Johnson-Stracke
1616	Heidenreich LLC
1617	Kirlin-Olson
1618	Sporer-Bruen
1619	Hoeger and Sons
1620	Quigley Inc
1621	Bergnaum, Ebert and Bode
1622	Rodriguez, Mayert and Koch
1623	Gutmann, Runolfsson and Wintheiser
1624	Rodriguez, Miller and Osinski
1625	Reichert Ltd
1626	Ledner PLC
1627	Beier-Kemmer
1628	Boyle Ltd
1629	Jaskolski, Breitenberg and Mraz
1630	Kub, Carroll and Wolf
1631	Hudson-Osinski
1632	Beer, Bogan and Hand
1633	Welch Inc
1634	Bartoletti Ltd
1635	Fisher, Donnelly and Padberg
1636	Russel, Jenkins and Bergstrom
1637	Morissette, Crist and O'Kon
1638	Romaguera and Sons
1639	Bosco, Hickle and Bartell
1640	Mueller-Wuckert
1641	Erdman, Bednar and Halvorson
1642	Schumm, Shanahan and Brakus
1643	Will, Labadie and Beer
1644	Brakus-Glover
1645	Koss-Conroy
1646	Rau Group
1647	Thiel, Sauer and McDermott
1648	Mohr and Sons
1649	Fay Ltd
1650	Dietrich, Stanton and Balistreri
1651	Reichel Group
1652	Ledner-Fadel
1653	Zboncak, Shields and Wuckert
1654	Donnelly-Runolfsdottir
1655	Ferry-Bins
1656	Cruickshank-McGlynn
1657	Greenfelder and Sons
1658	Grady, Boyer and Jones
1659	Casper, Barton and Kreiger
1660	Schimmel-Considine
1661	Bashirian Inc
1662	Douglas Inc
1663	Boyle Group
1664	Armstrong-Jones
1665	Hickle, Rutherford and Ritchie
1666	Harris-Lemke
1667	Marks PLC
1668	Bosco-Conn
1669	Gorczany, Cole and Luettgen
1670	Walter LLC
1671	Rohan and Sons
1672	Orn Inc
1673	Breitenberg Ltd
1674	Zemlak Ltd
1675	Schinner LLC
1676	Runte-Morar
1677	Schaefer-Ullrich
1678	Dickinson Ltd
1679	Beahan Ltd
1680	Hill-Dach
1681	Stehr Inc
1682	Tromp Inc
1683	Dickinson Inc
1684	Veum-Beier
1685	Kuvalis Inc
1686	Heller Inc
1687	Wunsch, Vandervort and Leannon
1688	Abshire and Sons
1689	Bartell Inc
1690	Wilderman Ltd
1691	Reinger-Okuneva
1692	Windler-Reichert
1693	Davis and Sons
1694	Huels Ltd
1695	Gutkowski, Bernier and Padberg
1696	Wehner Inc
1697	Kuphal, Sporer and Skiles
1698	Wehner-Lemke
1699	Keebler-Pollich
1700	Douglas, Bode and Reynolds
1701	Legros-Beer
1702	Ledner, Lockman and Klein
1703	Champlin Ltd
1704	Labadie, Hermiston and Marks
1705	Kautzer, Walker and Hoeger
1706	Marks LLC
1707	Spinka-Hahn
1708	Senger-Franecki
1709	Hettinger Inc
1710	Spinka, Kessler and Connelly
1711	Eichmann Group
1712	Hegmann Group
1713	Gorczany Inc
1714	Dare, Marks and Cummings
1715	Padberg, Bernhard and Bednar
1716	Gerlach PLC
1717	Williamson Inc
1718	Rowe-Nienow
1719	Cormier-Labadie
1720	Feest-Willms
1721	Dach-Smith
1722	Treutel, Runolfsson and Beahan
1723	Sporer Ltd
1724	Ziemann, Blick and Hane
1725	Klocko-Connelly
1726	Casper-Robel
1727	Armstrong-Roob
1728	Carroll, Boyle and Conn
1729	Parisian PLC
1730	Dare, Walker and Rath
1731	Effertz, Ward and Champlin
1732	Kerluke, Mertz and Kunze
1733	Adams-Pfannerstill
1734	Runolfsdottir-Ratke
1735	Gaylord Ltd
1736	Goldner, Mitchell and Blanda
1737	Hammes, Stroman and Schuppe
1738	Ortiz, Sawayn and Harber
1739	Bogan, Renner and Mitchell
1740	Rau PLC
1741	Nolan, Stanton and Hessel
1742	Langosh, Bartoletti and Toy
1743	Monahan LLC
1744	Schroeder-Block
1745	Kemmer, Streich and Sipes
1746	Champlin and Sons
1747	Marvin, Quigley and Kessler
1748	Wolf, Herman and Prosacco
1749	Dickens, O'Kon and Frami
1750	Schaden-Yost
1751	Ryan-Cummings
1752	Bogan, Langworth and Cummings
1753	Wehner and Sons
1754	Gerlach PLC
1755	Lesch-Jast
1756	Crona-Corwin
1757	Ritchie Inc
1758	Beier-Strosin
1759	Sauer, Bailey and Orn
1760	Cummerata, Hayes and Treutel
1761	Corkery Inc
1762	Wolff-Frami
1763	Reichert-Vandervort
1764	Powlowski, Borer and Pfannerstill
1765	Lubowitz-Oberbrunner
1766	Walter-Fisher
1767	Little-Hickle
1768	Huel-Howe
1769	Luettgen-Ondricka
1770	Nader Inc
1771	Schimmel-Mitchell
1772	Trantow and Sons
1773	Paucek-Rowe
1774	Auer, Ratke and Brekke
1775	Kub Inc
1776	Lind Ltd
1777	Bauch-Mills
1778	Conroy Inc
1779	Hamill and Sons
1780	Wiegand, Marks and Anderson
1781	Medhurst-Funk
1782	Walker Inc
1783	Stokes-Durgan
1784	Kulas Ltd
1785	Lang-Abernathy
1786	Dibbert, Cartwright and Mayer
1787	Ledner-Stroman
1788	Schultz-Cassin
1789	Bogisich-Jacobson
1790	Marvin and Sons
1791	Feeney-Morissette
1792	Morar-Brown
1793	Bosco-Simonis
1794	Williamson-Emard
1795	Wehner-Rogahn
1796	Barton and Sons
1797	Huels Group
1798	Conroy LLC
1799	Bins and Sons
1800	Wyman, Boehm and Ziemann
1801	Gusikowski-Bartoletti
1802	Hahn-Cole
1803	Hegmann-Cormier
1804	Kerluke-Trantow
1805	Schamberger, Dicki and Bruen
1806	Lakin, Carroll and Feeney
1807	O'Kon-Dare
1808	Cassin Ltd
1809	Weissnat, Rutherford and Haley
1810	Ruecker, Jacobi and Gutmann
1811	Schuppe-Shanahan
1812	Block, Predovic and Schowalter
1813	Gleichner-Leffler
1814	Nienow PLC
1815	Luettgen-Goldner
1816	Kuhn, Weber and Gutkowski
1817	Abshire, Fisher and Cremin
1818	Wolf, Homenick and Nolan
1819	Hyatt-Erdman
1820	Farrell, Daniel and Schaefer
1821	Dare-Olson
1822	Hauck and Sons
1823	Crooks PLC
1824	Goyette Group
1825	Rodriguez LLC
1826	Padberg-Miller
1827	Goldner, Gutmann and Donnelly
1828	Green and Sons
1829	Schaefer Inc
1830	Stanton-Feest
1831	Grant-Nicolas
1832	Glover and Sons
1833	Torphy, Fritsch and Lesch
1834	Bednar, Cronin and Johns
1835	Schroeder Group
1836	Daniel LLC
1837	Brown, Lynch and Hackett
1838	Abernathy, Goodwin and Kuphal
1839	Wintheiser, Smith and Schoen
1840	Lesch PLC
1841	Russel-Walter
1842	Hirthe Ltd
1843	Kerluke PLC
1844	Brakus, Rohan and Lueilwitz
1845	Gleason LLC
1846	Hill-Gorczany
1847	Nitzsche, Price and Weissnat
1848	Parisian Group
1849	Kemmer Group
1850	Wintheiser LLC
1851	Hessel PLC
1852	Ziemann and Sons
1853	Breitenberg-Tremblay
1854	Hagenes, Feil and Hill
1855	Marquardt-Williamson
1856	Kovacek and Sons
1857	Cole-Boyle
1858	Kuvalis and Sons
1859	Hill Group
1860	Feeney and Sons
1861	Watsica Inc
1862	Murray, Legros and Pagac
1863	Crona and Sons
1864	Wolff, Schinner and Hodkiewicz
1865	Bahringer-Prosacco
1866	Jacobs Inc
1867	Haag LLC
1868	Ullrich, Witting and Bailey
1869	Jakubowski and Sons
1870	Purdy Group
1871	Shanahan-Roberts
1872	Rowe-Raynor
1873	Wilderman-Kunde
1874	Quigley-Gorczany
1875	Kassulke Ltd
1876	Mueller, Altenwerth and Moen
1877	Treutel and Sons
1878	Tromp, Renner and Kuphal
1879	Hintz and Sons
1880	Bernhard LLC
1881	Beatty, Rau and Will
1882	Reilly-Ward
1883	Hayes, Toy and Mraz
1884	Stoltenberg-Pacocha
1885	Jacobi Inc
1886	Greenfelder-Lueilwitz
1887	Kessler-Daugherty
1888	Prohaska Ltd
1889	Lemke, Wiza and Wisoky
1890	Christiansen-Frami
1891	Paucek, Lemke and Adams
1892	Morar, Pfeffer and Kovacek
1893	Shanahan, O'Conner and Heidenreich
1894	Ledner, Boyer and Herzog
1895	Jones-Mayert
1896	Skiles and Sons
1897	Stoltenberg-Hodkiewicz
1898	Doyle Group
1899	Buckridge, Weissnat and Schumm
1900	Mueller-Mante
1901	Treutel, Larson and Windler
1902	Simonis, Considine and Streich
1903	Erdman-Legros
1904	Gusikowski and Sons
1905	Koelpin-Spinka
1906	Heathcote, McGlynn and Hauck
1907	Koelpin-Bergnaum
1908	Gottlieb, Bayer and Glover
1909	Trantow-Mertz
1910	Morissette Ltd
1911	Jerde and Sons
1912	Block-Rutherford
1913	Strosin-Morar
1914	Streich Ltd
1915	Boehm-Monahan
1916	King-Sawayn
1917	Pagac, Hessel and Koepp
1918	Hoeger PLC
1919	Braun, O'Connell and Lehner
1920	Connelly Group
1921	Rempel and Sons
1922	Quitzon-Toy
1923	Bins Inc
1924	Gibson LLC
1925	Von-Prohaska
1926	Howell, Bartoletti and Mertz
1927	Ledner, Douglas and Ryan
1928	VonRueden LLC
1929	Mayer, Emard and Watsica
1930	Wunsch Inc
1931	Daniel, Hintz and Hahn
1932	Gutkowski-Schaefer
1933	Marks-Ullrich
1934	Moen Group
1935	Boyer-Rowe
1936	Hettinger, Harvey and Purdy
1937	Fadel, Leffler and Haley
1938	Welch, Runolfsson and Crona
1939	Willms-Okuneva
1940	Runolfsdottir PLC
1941	Moore, Pollich and Raynor
1942	Schmidt, Volkman and Cormier
1943	Muller Inc
1944	Hauck and Sons
1945	Bosco, Toy and Buckridge
1946	Lang-Funk
1947	Cummings, Breitenberg and Effertz
1948	Satterfield and Sons
1949	Terry, Toy and Kreiger
1950	Cummings, Auer and Haley
1951	Schuppe and Sons
1952	O'Conner Ltd
1953	Kuphal-Goldner
1954	Weber PLC
1955	Daugherty, Collins and McLaughlin
1956	Kuhn LLC
1957	Nolan Group
1958	Heathcote PLC
1959	Fahey and Sons
1960	Thiel Ltd
1961	Tremblay LLC
1962	Cronin Group
1963	Boehm PLC
1964	Murray, Schneider and Schiller
1965	Stracke, Mosciski and Morar
1966	Pacocha, Douglas and Konopelski
1967	Collins Inc
1968	Altenwerth, Beer and Cruickshank
1969	Lindgren, Hudson and McLaughlin
1970	Bradtke-Orn
1971	Jones-Larson
1972	Schaden PLC
1973	Rosenbaum, Kihn and Bergstrom
1974	Daugherty PLC
1975	Anderson Group
1976	D'Amore-Bashirian
1977	Lubowitz, Runte and Herzog
1978	Jaskolski, Nitzsche and O'Hara
1979	Stracke Group
1980	Schimmel-Carter
1981	West Group
1982	Stiedemann-Romaguera
1983	Roob, Bogisich and Johnson
1984	Lowe, Huels and Kuhlman
1985	Auer, Greenholt and Schinner
1986	Bogisich Inc
1987	Grant, Hermann and Krajcik
1988	Klocko, Feest and Powlowski
1989	Mayer Ltd
1990	Hodkiewicz LLC
1991	Heaney-Little
1992	Boyer Ltd
1993	McDermott, Orn and Mills
1994	Yost Inc
1995	Herman, Powlowski and Crooks
1996	Welch, Macejkovic and Weber
1997	Hudson-Hermiston
1998	Stoltenberg and Sons
1999	Nitzsche, Greenfelder and Gislason
2000	Stanton, Hahn and Borer
2001	Crooks, Ratke and Hermiston
2002	Schmeler-Kuphal
\.


--
-- Data for Name: company_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.company_category (id, company_id, category_id) FROM stdin;
1	1	183
2	1	238
3	1	287
4	2	157
5	2	236
6	2	121
7	2	239
8	3	220
9	3	78
10	3	217
11	3	40
12	4	290
13	4	39
14	4	243
15	4	18
16	5	175
17	5	261
18	5	122
19	5	278
20	6	284
21	6	142
22	6	248
23	6	216
24	7	264
25	7	185
26	7	119
27	7	300
28	8	273
29	8	73
30	8	115
31	8	30
32	9	28
33	9	292
34	9	249
35	9	282
36	10	214
37	10	225
38	10	106
39	10	287
40	11	261
41	11	262
42	11	95
43	11	217
44	12	154
45	12	192
46	12	268
47	12	297
48	13	187
49	13	239
50	13	60
51	13	32
52	14	65
53	14	224
54	14	279
55	14	70
56	15	290
57	15	245
58	15	149
59	15	153
60	16	250
61	16	95
62	16	268
63	16	64
64	17	8
65	17	117
66	17	75
67	17	29
68	18	113
69	18	163
70	18	260
71	18	275
72	19	47
73	19	40
74	19	67
75	19	179
76	20	211
77	20	157
78	20	300
79	20	251
80	21	34
81	21	139
82	21	108
83	21	206
84	22	45
85	22	184
86	22	298
87	22	159
88	23	2
89	23	191
90	23	83
91	23	249
92	24	291
93	24	195
94	24	164
95	24	35
96	25	24
97	25	17
98	25	218
99	25	276
100	26	234
101	26	173
102	26	6
103	26	98
104	27	137
105	27	188
106	27	16
107	27	293
108	28	137
109	28	288
110	28	240
111	28	13
112	29	151
113	29	94
114	29	20
115	29	168
116	30	33
117	30	40
118	30	248
119	30	247
120	31	22
121	31	143
122	31	233
123	31	117
124	32	159
125	32	144
126	32	194
127	32	98
128	33	69
129	33	182
130	33	271
131	33	190
132	34	47
133	34	131
134	34	274
135	34	128
136	35	300
137	35	299
138	35	122
139	36	192
140	36	61
141	36	164
142	36	48
143	37	280
144	37	252
145	37	249
146	37	189
147	38	241
148	38	45
149	38	144
150	38	190
151	39	121
152	39	277
153	39	182
154	39	233
155	40	163
156	40	62
157	40	224
158	40	124
159	41	267
160	41	238
161	41	272
162	41	135
163	42	283
164	42	274
165	42	10
166	42	202
167	43	55
168	43	149
169	43	226
170	43	66
171	44	277
172	44	261
173	44	40
174	44	50
175	45	236
176	45	150
177	45	174
178	45	160
179	46	157
180	46	203
181	46	282
182	46	239
183	47	53
184	47	259
185	47	216
186	47	16
187	48	105
188	48	156
189	48	114
190	48	232
191	49	231
192	49	268
193	49	6
194	49	205
195	50	237
196	50	277
197	50	261
198	50	246
199	51	174
200	51	138
201	51	284
202	51	212
203	52	158
204	52	33
205	52	183
206	52	248
207	53	221
208	53	29
209	53	198
210	54	240
211	54	124
212	54	220
213	54	96
214	55	246
215	55	275
216	55	255
217	55	41
218	56	293
219	56	168
220	56	105
221	56	14
222	57	3
223	57	110
224	57	78
225	57	229
226	58	68
227	58	32
228	58	153
229	58	57
230	59	132
231	59	32
232	59	91
233	59	130
234	60	197
235	60	73
236	60	196
237	60	262
238	61	96
239	61	222
240	61	49
241	61	94
242	62	218
243	62	291
244	62	215
245	62	85
246	63	256
247	63	157
248	63	81
249	63	175
250	64	57
251	64	78
252	64	205
253	64	157
254	65	257
255	65	124
256	65	292
257	65	113
258	66	124
259	66	282
260	66	76
261	66	150
262	67	160
263	67	113
264	67	195
265	67	55
266	68	52
267	68	194
268	68	215
269	68	93
270	69	9
271	69	32
272	69	177
273	69	50
274	70	123
275	70	54
276	70	8
277	70	16
278	71	125
279	71	33
280	71	231
281	71	294
282	72	176
283	72	281
284	72	264
285	72	94
286	73	249
287	73	57
288	73	9
289	73	190
290	74	239
291	74	77
292	74	284
293	74	117
294	75	186
295	75	98
296	75	133
297	75	15
298	76	112
299	76	243
300	76	188
301	76	122
302	77	223
303	77	293
304	77	39
305	77	166
306	78	273
307	78	238
308	78	200
309	78	142
310	79	153
311	79	6
312	79	75
313	79	73
314	80	204
315	80	293
316	80	219
317	80	27
318	81	258
319	81	15
320	81	207
321	81	65
322	82	294
323	82	42
324	82	197
325	82	89
326	83	189
327	83	194
328	83	289
329	83	179
330	84	23
331	84	259
332	84	7
333	84	77
334	85	153
335	85	20
336	85	223
337	85	268
338	86	297
339	86	104
340	86	42
341	86	213
342	87	21
343	87	283
344	87	231
345	87	135
346	88	235
347	88	4
348	88	80
349	88	134
350	89	54
351	89	123
352	89	180
353	89	41
354	90	144
355	90	22
356	90	293
357	90	25
358	91	297
359	91	31
360	91	177
361	91	81
362	92	30
363	92	117
364	92	296
365	92	229
366	93	156
367	93	252
368	93	271
369	93	287
370	94	247
371	94	224
372	94	81
373	94	18
374	95	89
375	95	298
376	95	229
377	95	171
378	96	146
379	96	211
380	96	290
381	96	253
382	97	29
383	97	229
384	97	167
385	97	110
386	98	95
387	98	160
388	98	224
389	98	97
390	99	289
391	99	145
392	99	64
393	99	9
394	100	259
395	100	114
396	100	65
397	100	240
398	101	252
399	101	231
400	101	142
401	101	256
402	102	241
403	102	118
404	102	163
405	102	61
406	103	277
407	103	82
408	103	113
409	103	57
410	104	24
411	104	100
412	104	107
413	104	179
414	105	86
415	105	52
416	105	188
417	105	107
418	106	126
419	106	239
420	106	104
421	106	246
422	107	44
423	107	176
424	107	38
425	107	191
426	108	299
427	108	63
428	108	261
429	108	283
430	109	7
431	109	114
432	109	213
433	109	92
434	110	167
435	110	191
436	110	224
437	110	226
438	111	86
439	111	1
440	111	12
441	111	159
442	112	110
443	112	204
444	112	173
445	112	300
446	113	203
447	113	41
448	113	221
449	113	22
450	114	240
451	114	177
452	114	198
453	114	55
454	115	225
455	115	67
456	115	93
457	115	86
458	116	224
459	116	219
460	116	154
461	116	232
462	117	216
463	117	49
464	117	208
465	117	130
466	118	222
467	118	286
468	118	292
469	118	135
470	119	97
471	119	77
472	119	234
473	119	216
474	120	39
475	120	10
476	120	36
477	120	163
478	121	77
479	121	287
480	121	19
481	121	52
482	122	132
483	122	146
484	122	156
485	122	277
486	123	31
487	123	72
488	123	175
489	123	241
490	124	181
491	124	232
492	124	19
493	125	86
494	125	176
495	125	216
496	125	197
497	126	277
498	126	4
499	126	293
500	126	97
501	127	54
502	127	30
503	127	199
504	127	110
505	128	62
506	128	197
507	128	235
508	128	52
509	129	129
510	129	156
511	129	162
512	129	224
513	130	74
514	130	122
515	130	71
516	130	66
517	131	253
518	131	91
519	131	206
520	131	287
521	132	5
522	132	218
523	132	143
524	132	295
525	133	185
526	133	260
527	133	61
528	133	248
529	134	210
530	134	212
531	134	224
532	134	299
533	135	13
534	135	213
535	135	18
536	135	92
537	136	42
538	136	204
539	136	240
540	136	280
541	137	116
542	137	154
543	137	200
544	137	182
545	138	17
546	138	1
547	138	33
548	138	210
549	139	243
550	139	69
551	139	174
552	139	187
553	140	292
554	140	267
555	140	297
556	140	88
557	141	156
558	141	15
559	141	211
560	141	128
561	142	189
562	142	275
563	142	190
564	142	195
565	143	34
566	143	286
567	143	195
568	143	17
569	144	258
570	144	3
571	144	16
572	144	58
573	145	264
574	145	89
575	145	124
576	145	229
577	146	195
578	146	128
579	146	138
580	146	59
581	147	142
582	147	119
583	147	56
584	148	26
585	148	161
586	148	39
587	149	23
588	149	233
589	149	101
590	149	253
591	150	286
592	150	202
593	150	147
594	150	244
595	151	127
596	151	225
597	151	138
598	151	51
599	152	91
600	152	71
601	152	75
602	152	248
603	153	163
604	153	240
605	153	259
606	153	279
607	154	112
608	154	241
609	154	231
610	154	283
611	155	239
612	155	261
613	155	275
614	155	221
615	156	174
616	156	17
617	156	144
618	156	148
619	157	199
620	157	112
621	157	41
622	157	204
623	158	226
624	158	25
625	158	186
626	158	84
627	159	178
628	159	186
629	159	266
630	159	78
631	160	250
632	160	13
633	160	188
634	160	98
635	161	282
636	161	207
637	161	110
638	161	101
639	162	173
640	162	253
641	162	2
642	162	268
643	163	39
644	163	9
645	163	287
646	163	291
647	164	215
648	164	298
649	164	16
650	164	254
651	165	285
652	165	254
653	165	279
654	165	230
655	166	161
656	166	244
657	166	255
658	166	225
659	167	106
660	167	156
661	167	150
662	167	72
663	168	38
664	168	162
665	168	273
666	168	168
667	169	223
668	169	295
669	169	221
670	169	139
671	170	62
672	170	142
673	170	89
674	170	294
675	171	176
676	171	45
677	171	236
678	171	165
679	172	193
680	172	266
681	172	81
682	172	197
683	173	174
684	173	200
685	173	228
686	173	255
687	174	88
688	174	82
689	174	29
690	174	138
691	175	91
692	175	242
693	175	201
694	176	246
695	176	18
696	176	172
697	176	13
698	177	69
699	177	287
700	177	31
701	177	206
702	178	153
703	178	276
704	178	113
705	178	290
706	179	218
707	179	40
708	179	252
709	179	215
710	180	173
711	180	59
712	180	197
713	180	157
714	181	268
715	181	36
716	181	287
717	181	85
718	182	277
719	182	197
720	182	142
721	182	45
722	183	215
723	183	112
724	183	35
725	183	262
726	184	260
727	184	129
728	184	274
729	184	164
730	185	268
731	185	289
732	185	218
733	185	118
734	186	82
735	186	121
736	186	185
737	186	232
738	187	20
739	187	115
740	187	263
741	187	130
742	188	150
743	188	225
744	188	214
745	188	24
746	189	103
747	189	59
748	189	261
749	189	187
750	190	252
751	190	183
752	190	150
753	190	24
754	191	91
755	191	106
756	191	125
757	191	40
758	192	116
759	192	240
760	192	159
761	192	283
762	193	133
763	193	14
764	193	294
765	193	79
766	194	253
767	194	94
768	194	95
769	194	63
770	195	140
771	195	163
772	195	9
773	195	8
774	196	35
775	196	121
776	196	156
777	196	67
778	197	54
779	197	248
780	197	245
781	197	159
782	198	128
783	198	234
784	198	44
785	198	95
786	199	69
787	199	148
788	199	197
789	199	83
790	200	167
791	200	146
792	200	147
793	200	60
794	201	276
795	201	193
796	201	266
797	201	174
798	202	299
799	202	120
800	202	269
801	202	243
802	203	15
803	203	149
804	203	175
805	203	242
806	204	92
807	204	178
808	204	216
809	204	300
810	205	138
811	205	264
812	205	79
813	205	83
814	206	288
815	206	228
816	206	12
817	206	287
818	207	185
819	207	57
820	207	207
821	207	5
822	208	20
823	208	117
824	208	43
825	208	130
826	209	187
827	209	88
828	209	268
829	209	186
830	210	163
831	210	23
832	210	220
833	210	191
834	211	144
835	211	269
836	211	282
837	211	64
838	212	136
839	212	280
840	212	242
841	212	199
842	213	106
843	213	120
844	213	198
845	214	98
846	214	44
847	214	102
848	215	300
849	215	177
850	215	24
851	215	140
852	216	92
853	216	240
854	216	65
855	216	56
856	217	3
857	217	235
858	217	123
859	217	53
860	218	158
861	218	227
862	218	165
863	218	38
864	219	242
865	219	8
866	219	156
867	219	288
868	220	224
869	220	28
870	220	44
871	220	68
872	221	196
873	221	42
874	221	23
875	221	160
876	222	153
877	222	271
878	222	201
879	222	55
880	223	85
881	223	153
882	223	7
883	223	127
884	224	263
885	224	135
886	224	79
887	224	130
888	225	297
889	225	150
890	225	167
891	225	237
892	226	276
893	226	282
894	226	191
895	226	94
896	227	67
897	227	90
898	227	20
899	227	301
900	228	259
901	228	161
902	228	45
903	228	82
904	229	143
905	229	65
906	229	54
907	229	29
908	230	119
909	230	287
910	230	258
911	230	176
912	231	275
913	231	247
914	231	40
915	231	293
916	232	224
917	232	69
918	232	243
919	232	109
920	233	238
921	233	161
922	233	215
923	233	196
924	234	286
925	234	257
926	234	1
927	234	198
928	235	6
929	235	258
930	235	190
931	235	79
932	236	4
933	236	135
934	236	210
935	236	41
936	237	300
937	237	293
938	237	213
939	237	11
940	238	299
941	238	279
942	238	211
943	238	190
944	239	200
945	239	113
946	239	197
947	239	17
948	240	243
949	240	129
950	240	69
951	240	70
952	241	136
953	241	227
954	241	115
955	241	269
956	242	19
957	242	180
958	242	277
959	242	22
960	243	39
961	243	145
962	243	296
963	243	84
964	244	11
965	244	51
966	244	178
967	244	186
968	245	102
969	245	162
970	245	271
971	245	47
972	246	31
973	246	182
974	246	23
975	246	282
976	247	251
977	247	202
978	247	66
979	247	250
980	248	229
981	248	233
982	248	78
983	248	257
984	249	1
985	249	29
986	249	21
987	249	239
988	250	97
989	250	208
990	250	86
991	250	264
992	251	297
993	251	161
994	251	96
995	251	236
996	252	209
997	252	223
998	252	181
999	252	274
1000	253	258
1001	253	194
1002	253	274
1003	253	54
1004	254	133
1005	254	292
1006	254	253
1007	254	286
1008	255	53
1009	255	295
1010	255	94
1011	255	41
1012	256	165
1013	256	45
1014	256	48
1015	256	16
1016	257	160
1017	257	300
1018	257	80
1019	257	228
1020	258	206
1021	258	196
1022	258	301
1023	258	18
1024	259	144
1025	259	176
1026	259	97
1027	259	225
1028	260	71
1029	260	149
1030	260	248
1031	260	188
1032	261	154
1033	261	85
1034	261	90
1035	261	44
1036	262	106
1037	262	40
1038	262	203
1039	262	274
1040	263	111
1041	263	4
1042	263	263
1043	263	139
1044	264	178
1045	264	227
1046	264	87
1047	264	65
1048	265	278
1049	265	200
1050	265	158
1051	265	275
1052	266	180
1053	266	293
1054	266	219
1055	266	203
1056	267	176
1057	267	52
1058	267	102
1059	267	194
1060	268	213
1061	268	145
1062	268	133
1063	268	114
1064	269	154
1065	269	116
1066	269	7
1067	269	207
1068	270	41
1069	270	169
1070	270	66
1071	270	122
1072	271	120
1073	271	173
1074	271	82
1075	271	8
1076	272	156
1077	272	299
1078	272	94
1079	272	301
1080	273	284
1081	273	54
1082	273	258
1083	273	260
1084	274	136
1085	274	242
1086	274	190
1087	274	150
1088	275	152
1089	275	79
1090	275	55
1091	275	92
1092	276	265
1093	276	126
1094	276	70
1095	276	11
1096	277	14
1097	277	87
1098	277	298
1099	277	170
1100	278	60
1101	278	24
1102	278	207
1103	278	295
1104	279	235
1105	279	130
1106	279	210
1107	279	198
1108	280	228
1109	280	226
1110	280	104
1111	280	197
1112	281	198
1113	281	89
1114	281	30
1115	281	277
1116	282	153
1117	282	249
1118	282	19
1119	282	301
1120	283	242
1121	283	34
1122	283	232
1123	283	35
1124	284	150
1125	284	227
1126	284	59
1127	284	61
1128	285	145
1129	285	157
1130	285	128
1131	285	12
1132	286	282
1133	286	164
1134	286	53
1135	286	48
1136	287	136
1137	287	8
1138	287	267
1139	287	35
1140	288	140
1141	288	277
1142	288	291
1143	288	264
1144	289	234
1145	289	45
1146	289	123
1147	290	213
1148	290	65
1149	290	168
1150	290	299
1151	291	27
1152	291	111
1153	291	237
1154	291	215
1155	292	43
1156	292	241
1157	292	153
1158	292	265
1159	293	177
1160	293	287
1161	293	283
1162	293	107
1163	294	221
1164	294	261
1165	294	220
1166	294	169
1167	295	46
1168	295	177
1169	295	273
1170	295	19
1171	296	248
1172	296	231
1173	296	145
1174	297	287
1175	297	26
1176	297	97
1177	297	153
1178	298	220
1179	298	130
1180	298	149
1181	298	204
1182	299	20
1183	299	42
1184	299	295
1185	299	238
1186	300	82
1187	300	266
1188	300	103
1189	300	120
1190	301	258
1191	301	279
1192	301	59
1193	301	172
1194	302	99
1195	302	50
1196	302	194
1197	302	255
1198	303	290
1199	303	76
1200	303	14
1201	303	221
1202	304	286
1203	304	224
1204	304	288
1205	304	255
1206	305	293
1207	305	120
1208	305	160
1209	305	205
1210	306	126
1211	306	258
1212	306	235
1213	306	262
1214	307	252
1215	307	94
1216	307	29
1217	307	46
1218	308	244
1219	308	228
1220	308	263
1221	308	277
1222	309	110
1223	309	141
1224	309	88
1225	309	5
1226	310	153
1227	310	132
1228	310	190
1229	310	218
1230	311	135
1231	311	134
1232	311	167
1233	311	115
1234	312	29
1235	312	198
1236	312	42
1237	312	65
1238	313	120
1239	313	75
1240	313	118
1241	313	139
1242	314	123
1243	314	78
1244	314	151
1245	314	213
1246	315	168
1247	315	196
1248	315	299
1249	315	99
1250	316	104
1251	316	32
1252	316	54
1253	316	237
1254	317	194
1255	317	53
1256	317	130
1257	317	201
1258	318	238
1259	318	179
1260	318	300
1261	318	5
1262	319	170
1263	319	254
1264	319	12
1265	319	218
1266	320	97
1267	320	127
1268	320	1
1269	320	71
1270	321	44
1271	321	285
1272	321	171
1273	321	116
1274	322	190
1275	322	221
1276	322	136
1277	322	90
1278	323	46
1279	323	48
1280	323	101
1281	323	228
1282	324	62
1283	324	32
1284	324	257
1285	324	242
1286	325	145
1287	325	170
1288	325	292
1289	325	128
1290	326	242
1291	326	59
1292	326	122
1293	326	1
1294	327	120
1295	327	138
1296	327	71
1297	327	250
1298	328	159
1299	328	279
1300	328	163
1301	328	224
1302	329	291
1303	329	62
1304	329	141
1305	329	86
1306	330	161
1307	330	35
1308	330	58
1309	330	118
1310	331	121
1311	331	17
1312	331	72
1313	331	110
1314	332	274
1315	332	100
1316	332	141
1317	332	25
1318	333	233
1319	333	247
1320	333	207
1321	334	188
1322	334	266
1323	334	236
1324	334	173
1325	335	278
1326	335	17
1327	335	136
1328	335	24
1329	336	249
1330	336	43
1331	336	212
1332	336	253
1333	337	272
1334	337	261
1335	337	184
1336	337	291
1337	338	13
1338	338	9
1339	338	10
1340	338	48
1341	339	115
1342	339	126
1343	339	94
1344	339	20
1345	340	242
1346	340	207
1347	340	54
1348	340	218
1349	341	33
1350	341	255
1351	341	185
1352	341	283
1353	342	215
1354	342	161
1355	342	145
1356	342	16
1357	343	185
1358	343	285
1359	343	224
1360	343	37
1361	344	155
1362	344	126
1363	344	7
1364	344	132
1365	345	169
1366	345	21
1367	345	18
1368	345	138
1369	346	182
1370	346	256
1371	346	14
1372	346	11
1373	347	29
1374	347	235
1375	347	3
1376	347	11
1377	348	153
1378	348	17
1379	348	195
1380	348	298
1381	349	211
1382	349	250
1383	349	292
1384	349	3
1385	350	285
1386	350	295
1387	350	88
1388	350	81
1389	351	73
1390	351	60
1391	351	246
1392	351	36
1393	352	213
1394	352	2
1395	352	116
1396	352	41
1397	353	156
1398	353	168
1399	353	120
1400	353	161
1401	354	166
1402	354	232
1403	354	152
1404	354	58
1405	355	282
1406	355	47
1407	355	170
1408	355	210
1409	356	1
1410	356	184
1411	356	78
1412	356	27
1413	357	292
1414	357	24
1415	357	170
1416	357	132
1417	358	241
1418	358	101
1419	358	260
1420	358	18
1421	359	141
1422	359	233
1423	359	81
1424	359	200
1425	360	225
1426	360	5
1427	360	171
1428	360	244
1429	361	40
1430	361	8
1431	361	35
1432	361	265
1433	362	162
1434	362	214
1435	362	166
1436	362	15
1437	363	272
1438	363	289
1439	363	235
1440	363	201
1441	364	149
1442	364	259
1443	364	97
1444	364	136
1445	365	258
1446	365	234
1447	365	48
1448	365	98
1449	366	266
1450	366	193
1451	366	156
1452	366	179
1453	367	170
1454	367	89
1455	367	143
1456	367	121
1457	368	103
1458	368	209
1459	368	194
1460	368	200
1461	369	148
1462	369	185
1463	369	175
1464	369	112
1465	370	228
1466	370	164
1467	370	276
1468	370	182
1469	371	106
1470	371	237
1471	371	53
1472	371	239
1473	372	48
1474	372	19
1475	372	265
1476	372	64
1477	373	267
1478	373	148
1479	373	140
1480	373	77
1481	374	196
1482	374	65
1483	374	171
1484	374	267
1485	375	203
1486	375	264
1487	375	98
1488	375	10
1489	376	270
1490	376	133
1491	376	71
1492	376	4
1493	377	293
1494	377	205
1495	377	179
1496	377	134
1497	378	190
1498	378	230
1499	378	171
1500	378	92
1501	379	128
1502	379	158
1503	379	36
1504	379	142
1505	380	103
1506	380	94
1507	380	3
1508	380	193
1509	381	255
1510	381	103
1511	381	180
1512	381	231
1513	382	75
1514	382	261
1515	382	53
1516	382	3
1517	383	35
1518	383	146
1519	383	230
1520	384	77
1521	384	175
1522	384	217
1523	384	281
1524	385	240
1525	385	31
1526	385	237
1527	385	121
1528	386	230
1529	386	208
1530	386	106
1531	386	88
1532	387	197
1533	387	162
1534	387	170
1535	387	134
1536	388	125
1537	388	185
1538	388	241
1539	388	20
1540	389	97
1541	389	27
1542	389	157
1543	389	239
1544	390	249
1545	390	19
1546	390	287
1547	390	265
1548	391	178
1549	391	251
1550	391	210
1551	391	184
1552	392	11
1553	392	10
1554	392	206
1555	392	111
1556	393	226
1557	393	146
1558	393	223
1559	394	12
1560	394	128
1561	394	15
1562	394	179
1563	395	280
1564	395	21
1565	395	2
1566	395	171
1567	396	194
1568	396	273
1569	396	111
1570	396	192
1571	397	196
1572	397	88
1573	397	87
1574	397	273
1575	398	11
1576	398	108
1577	398	57
1578	398	103
1579	399	164
1580	399	6
1581	399	60
1582	399	131
1583	400	192
1584	400	298
1585	400	197
1586	400	173
1587	401	235
1588	401	91
1589	401	206
1590	401	80
1591	402	185
1592	402	270
1593	402	217
1594	402	35
1595	403	183
1596	403	269
1597	403	201
1598	403	264
1599	404	7
1600	404	115
1601	404	221
1602	404	198
1603	405	246
1604	405	164
1605	405	293
1606	405	87
1607	406	245
1608	406	109
1609	406	159
1610	406	223
1611	407	149
1612	407	231
1613	407	232
1614	407	269
1615	408	129
1616	408	128
1617	408	249
1618	408	239
1619	409	107
1620	409	65
1621	409	298
1622	410	52
1623	410	188
1624	410	36
1625	410	49
1626	411	74
1627	411	27
1628	411	75
1629	411	223
1630	412	174
1631	412	135
1632	412	69
1633	412	119
1634	413	25
1635	413	85
1636	413	215
1637	413	298
1638	414	191
1639	414	4
1640	414	89
1641	414	263
1642	415	236
1643	415	85
1644	415	11
1645	415	2
1646	416	44
1647	416	273
1648	416	68
1649	416	146
1650	417	49
1651	417	217
1652	417	296
1653	417	22
1654	418	61
1655	418	91
1656	418	287
1657	418	207
1658	419	205
1659	419	186
1660	419	258
1661	419	71
1662	420	123
1663	420	14
1664	420	73
1665	420	113
1666	421	21
1667	421	115
1668	421	283
1669	421	72
1670	422	274
1671	422	77
1672	422	190
1673	422	229
1674	423	33
1675	423	228
1676	423	203
1677	423	146
1678	424	214
1679	424	79
1680	424	143
1681	424	12
1682	425	164
1683	425	43
1684	425	62
1685	425	170
1686	426	123
1687	426	208
1688	426	287
1689	426	295
1690	427	167
1691	427	162
1692	427	144
1693	427	277
1694	428	210
1695	428	112
1696	428	270
1697	428	43
1698	429	71
1699	429	167
1700	429	174
1701	429	219
1702	430	174
1703	430	267
1704	430	198
1705	430	181
1706	431	201
1707	431	99
1708	431	80
1709	431	283
1710	432	83
1711	432	152
1712	432	265
1713	432	77
1714	433	175
1715	433	188
1716	433	207
1717	433	163
1718	434	278
1719	434	98
1720	434	8
1721	434	193
1722	435	10
1723	435	146
1724	435	73
1725	435	215
1726	436	109
1727	436	127
1728	436	3
1729	436	46
1730	437	224
1731	437	234
1732	437	208
1733	437	237
1734	438	194
1735	438	227
1736	438	98
1737	438	166
1738	439	216
1739	439	50
1740	439	248
1741	439	243
1742	440	202
1743	440	45
1744	440	15
1745	440	247
1746	441	151
1747	441	122
1748	441	292
1749	441	51
1750	442	136
1751	442	143
1752	442	62
1753	442	107
1754	443	132
1755	443	30
1756	443	226
1757	443	8
1758	444	68
1759	444	107
1760	444	97
1761	444	9
1762	445	260
1763	445	109
1764	445	231
1765	445	147
1766	446	118
1767	446	98
1768	446	241
1769	446	22
1770	447	124
1771	447	127
1772	447	42
1773	447	147
1774	448	69
1775	448	131
1776	448	65
1777	448	21
1778	449	1
1779	449	209
1780	449	12
1781	449	43
1782	450	196
1783	450	70
1784	450	185
1785	450	93
1786	451	63
1787	451	258
1788	451	29
1789	451	221
1790	452	132
1791	452	230
1792	452	218
1793	452	256
1794	453	103
1795	453	294
1796	453	25
1797	453	243
1798	454	90
1799	454	115
1800	454	79
1801	454	48
1802	455	285
1803	455	53
1804	455	298
1805	455	16
1806	456	261
1807	456	122
1808	456	179
1809	456	63
1810	457	114
1811	457	274
1812	457	54
1813	457	51
1814	458	38
1815	458	235
1816	458	94
1817	458	258
1818	459	215
1819	459	299
1820	459	278
1821	459	203
1822	460	34
1823	460	270
1824	460	32
1825	460	239
1826	461	13
1827	461	40
1828	461	149
1829	461	232
1830	462	180
1831	462	5
1832	462	9
1833	462	218
1834	463	168
1835	463	78
1836	463	30
1837	463	21
1838	464	39
1839	464	254
1840	464	214
1841	464	187
1842	465	253
1843	465	230
1844	465	57
1845	465	131
1846	466	244
1847	466	126
1848	466	100
1849	466	88
1850	467	133
1851	467	145
1852	467	265
1853	467	222
1854	468	107
1855	468	171
1856	468	117
1857	468	214
1858	469	287
1859	469	3
1860	469	251
1861	469	183
1862	470	166
1863	470	205
1864	470	3
1865	470	100
1866	471	293
1867	471	35
1868	471	238
1869	471	76
1870	472	50
1871	472	107
1872	472	283
1873	472	36
1874	473	198
1875	473	211
1876	473	48
1877	474	213
1878	474	249
1879	474	254
1880	474	15
1881	475	102
1882	475	118
1883	475	59
1884	475	78
1885	476	93
1886	476	60
1887	476	299
1888	476	42
1889	477	205
1890	477	3
1891	477	160
1892	477	49
1893	478	263
1894	478	47
1895	478	138
1896	478	241
1897	479	155
1898	479	9
1899	479	50
1900	479	261
1901	480	58
1902	480	188
1903	480	9
1904	480	291
1905	481	132
1906	481	109
1907	481	68
1908	481	100
1909	482	119
1910	482	35
1911	482	203
1912	482	159
1913	483	153
1914	483	3
1915	483	27
1916	483	194
1917	484	114
1918	484	74
1919	484	157
1920	484	292
1921	485	102
1922	485	38
1923	485	106
1924	485	294
1925	486	115
1926	486	22
1927	486	286
1928	486	207
1929	487	155
1930	487	138
1931	487	278
1932	487	149
1933	488	147
1934	488	187
1935	488	52
1936	488	182
1937	489	176
1938	489	23
1939	489	141
1940	489	139
1941	490	4
1942	490	164
1943	490	122
1944	490	188
1945	491	159
1946	491	34
1947	491	246
1948	491	288
1949	492	211
1950	492	197
1951	492	87
1952	492	103
1953	493	160
1954	493	290
1955	493	16
1956	493	170
1957	494	183
1958	494	151
1959	494	200
1960	494	263
1961	495	195
1962	495	295
1963	495	178
1964	495	204
1965	496	16
1966	496	185
1967	496	31
1968	496	35
1969	497	31
1970	497	28
1971	497	204
1972	497	137
1973	498	101
1974	498	137
1975	498	172
1976	498	21
1977	499	212
1978	499	277
1979	499	176
1980	499	191
1981	500	266
1982	500	209
1983	500	195
1984	500	294
1985	501	206
1986	501	76
1987	501	80
1988	501	55
1989	502	249
1990	502	34
1991	502	59
1992	502	204
1993	503	234
1994	503	78
1995	503	182
1996	503	265
1997	504	30
1998	504	243
1999	504	158
2000	504	286
2001	505	10
2002	505	260
2003	505	3
2004	505	242
2005	506	238
2006	506	219
2007	506	144
2008	506	72
2009	507	175
2010	507	143
2011	507	99
2012	507	301
2013	508	208
2014	508	107
2015	508	287
2016	508	199
2017	509	211
2018	509	78
2019	509	122
2020	509	270
2021	510	289
2022	510	25
2023	510	113
2024	510	35
2025	511	47
2026	511	158
2027	511	124
2028	511	11
2029	512	262
2030	512	43
2031	512	182
2032	512	184
2033	513	27
2034	513	161
2035	513	218
2036	513	120
2037	514	105
2038	514	220
2039	514	145
2040	514	8
2041	515	134
2042	515	48
2043	515	103
2044	515	114
2045	516	23
2046	516	154
2047	516	76
2048	516	180
2049	517	256
2050	517	83
2051	517	112
2052	517	167
2053	518	2
2054	518	26
2055	518	200
2056	518	72
2057	519	230
2058	519	178
2059	519	129
2060	519	88
2061	520	292
2062	520	295
2063	520	207
2064	520	43
2065	521	3
2066	521	121
2067	521	66
2068	521	23
2069	522	85
2070	522	177
2071	522	184
2072	522	297
2073	523	194
2074	523	102
2075	523	158
2076	523	271
2077	524	261
2078	524	275
2079	524	193
2080	524	212
2081	525	283
2082	525	248
2083	525	230
2084	525	193
2085	526	142
2086	526	213
2087	526	245
2088	526	223
2089	527	230
2090	527	27
2091	527	234
2092	527	148
2093	528	19
2094	528	259
2095	528	159
2096	528	18
2097	529	156
2098	529	205
2099	529	175
2100	529	298
2101	530	20
2102	530	287
2103	530	50
2104	530	81
2105	531	219
2106	531	193
2107	531	255
2108	531	3
2109	532	301
2110	532	126
2111	532	187
2112	532	26
2113	533	42
2114	533	179
2115	533	152
2116	533	26
2117	534	225
2118	534	257
2119	534	60
2120	534	197
2121	535	128
2122	535	259
2123	535	177
2124	535	231
2125	536	128
2126	536	164
2127	536	73
2128	536	149
2129	537	18
2130	537	275
2131	537	94
2132	537	284
2133	538	98
2134	538	299
2135	538	275
2136	538	97
2137	539	101
2138	539	6
2139	539	150
2140	539	94
2141	540	152
2142	540	185
2143	540	8
2144	540	210
2145	541	237
2146	541	273
2147	541	209
2148	541	130
2149	542	85
2150	542	40
2151	542	1
2152	542	125
2153	543	95
2154	543	43
2155	543	34
2156	543	166
2157	544	87
2158	544	42
2159	544	116
2160	544	292
2161	545	255
2162	545	108
2163	545	67
2164	545	147
2165	546	161
2166	546	261
2167	546	184
2168	546	9
2169	547	205
2170	547	71
2171	547	73
2172	547	211
2173	548	28
2174	548	219
2175	548	104
2176	548	185
2177	549	228
2178	549	202
2179	549	144
2180	549	151
2181	550	79
2182	550	265
2183	550	241
2184	550	13
2185	551	193
2186	551	285
2187	551	132
2188	551	19
2189	552	174
2190	552	190
2191	552	112
2192	552	51
2193	553	155
2194	553	39
2195	553	145
2196	553	219
2197	554	76
2198	554	210
2199	554	96
2200	554	108
2201	555	102
2202	555	56
2203	555	175
2204	555	4
2205	556	153
2206	556	220
2207	556	115
2208	556	119
2209	557	113
2210	557	3
2211	557	241
2212	557	207
2213	558	82
2214	558	179
2215	558	122
2216	558	164
2217	559	121
2218	559	130
2219	559	12
2220	559	51
2221	560	26
2222	560	130
2223	560	238
2224	560	247
2225	561	164
2226	561	167
2227	561	26
2228	561	47
2229	562	196
2230	562	49
2231	562	34
2232	562	287
2233	563	89
2234	563	116
2235	563	64
2236	563	101
2237	564	288
2238	564	160
2239	564	145
2240	564	158
2241	565	12
2242	565	90
2243	565	252
2244	565	107
2245	566	80
2246	566	138
2247	566	161
2248	566	29
2249	567	36
2250	567	55
2251	567	185
2252	567	35
2253	568	98
2254	568	223
2255	568	26
2256	568	264
2257	569	84
2258	569	1
2259	569	193
2260	569	184
2261	570	128
2262	570	40
2263	570	36
2264	570	281
2265	571	213
2266	571	180
2267	571	145
2268	571	10
2269	572	111
2270	572	175
2271	572	69
2272	572	10
2273	573	97
2274	573	99
2275	573	272
2276	573	250
2277	574	184
2278	574	133
2279	574	94
2280	574	264
2281	575	102
2282	575	167
2283	575	138
2284	575	131
2285	576	163
2286	576	174
2287	576	95
2288	576	189
2289	577	9
2290	577	184
2291	577	10
2292	577	149
2293	578	263
2294	578	43
2295	578	91
2296	578	216
2297	579	199
2298	579	270
2299	579	161
2300	579	6
2301	580	124
2302	580	246
2303	580	68
2304	580	284
2305	581	39
2306	581	241
2307	581	243
2308	581	129
2309	582	183
2310	582	295
2311	582	278
2312	582	239
2313	583	135
2314	583	284
2315	583	207
2316	583	27
2317	584	179
2318	584	124
2319	584	194
2320	584	80
2321	585	92
2322	585	157
2323	585	166
2324	585	123
2325	586	172
2326	586	164
2327	586	10
2328	586	129
2329	587	155
2330	587	12
2331	587	222
2332	587	128
2333	588	6
2334	588	109
2335	588	235
2336	588	25
2337	589	187
2338	589	90
2339	589	252
2340	589	103
2341	590	34
2342	590	56
2343	590	285
2344	590	39
2345	591	3
2346	591	188
2347	591	190
2348	591	128
2349	592	54
2350	592	123
2351	592	100
2352	592	104
2353	593	121
2354	593	72
2355	593	26
2356	593	22
2357	594	158
2358	594	111
2359	594	154
2360	594	179
2361	595	135
2362	595	209
2363	595	231
2364	595	220
2365	596	234
2366	596	87
2367	596	269
2368	596	73
2369	597	252
2370	597	206
2371	597	123
2372	597	2
2373	598	129
2374	598	222
2375	598	105
2376	598	237
2377	599	4
2378	599	207
2379	599	134
2380	599	109
2381	600	257
2382	600	219
2383	600	97
2384	600	12
2385	601	79
2386	601	232
2387	601	240
2388	601	245
2389	602	37
2390	602	200
2391	602	21
2392	602	149
2393	603	57
2394	603	211
2395	603	71
2396	603	31
2397	604	111
2398	604	128
2399	604	12
2400	604	247
2401	605	243
2402	605	36
2403	605	186
2404	605	301
2405	606	178
2406	606	248
2407	606	228
2408	606	173
2409	607	127
2410	607	175
2411	607	14
2412	607	145
2413	608	122
2414	608	290
2415	608	152
2416	608	26
2417	609	300
2418	609	236
2419	609	125
2420	609	179
2421	610	41
2422	610	242
2423	610	79
2424	610	68
2425	611	281
2426	611	182
2427	611	250
2428	611	269
2429	612	120
2430	612	202
2431	612	50
2432	612	182
2433	613	3
2434	613	238
2435	613	187
2436	613	172
2437	614	189
2438	614	18
2439	614	282
2440	614	57
2441	615	184
2442	615	70
2443	615	10
2444	615	231
2445	616	173
2446	616	189
2447	616	113
2448	616	28
2449	617	9
2450	617	168
2451	617	210
2452	617	151
2453	618	41
2454	618	28
2455	618	85
2456	618	67
2457	619	125
2458	619	273
2459	619	280
2460	619	248
2461	620	109
2462	620	164
2463	620	45
2464	620	174
2465	621	223
2466	621	151
2467	621	197
2468	622	270
2469	622	177
2470	622	12
2471	622	175
2472	623	49
2473	623	285
2474	623	28
2475	623	236
2476	624	57
2477	624	277
2478	624	149
2479	624	72
2480	625	50
2481	625	262
2482	625	291
2483	625	270
2484	626	188
2485	626	169
2486	626	110
2487	626	181
2488	627	79
2489	627	216
2490	627	140
2491	627	131
2492	628	228
2493	628	161
2494	628	122
2495	628	267
2496	629	249
2497	629	254
2498	629	98
2499	629	50
2500	630	2
2501	630	256
2502	630	113
2503	630	131
2504	631	68
2505	631	147
2506	631	178
2507	631	209
2508	632	15
2509	632	162
2510	632	236
2511	632	34
2512	633	51
2513	633	119
2514	633	255
2515	633	34
2516	634	208
2517	634	270
2518	634	11
2519	634	254
2520	635	24
2521	635	158
2522	635	277
2523	635	30
2524	636	250
2525	636	65
2526	636	140
2527	636	93
2528	637	28
2529	637	188
2530	637	64
2531	637	29
2532	638	218
2533	638	89
2534	638	189
2535	638	276
2536	639	68
2537	639	31
2538	639	45
2539	639	23
2540	640	295
2541	640	11
2542	640	43
2543	640	46
2544	641	164
2545	641	233
2546	641	38
2547	641	63
2548	642	280
2549	642	26
2550	642	21
2551	642	145
2552	643	53
2553	643	104
2554	643	284
2555	643	111
2556	644	48
2557	644	100
2558	644	165
2559	644	4
2560	645	252
2561	645	163
2562	645	57
2563	645	266
2564	646	6
2565	646	279
2566	646	154
2567	646	230
2568	647	57
2569	647	192
2570	647	112
2571	647	215
2572	648	77
2573	648	139
2574	648	91
2575	649	189
2576	649	238
2577	649	74
2578	649	291
2579	650	48
2580	650	43
2581	650	163
2582	650	114
2583	651	32
2584	651	101
2585	651	184
2586	651	120
2587	652	166
2588	652	206
2589	652	4
2590	652	167
2591	653	13
2592	653	69
2593	653	142
2594	653	161
2595	654	184
2596	654	106
2597	654	298
2598	654	280
2599	655	6
2600	655	131
2601	655	109
2602	655	74
2603	656	135
2604	656	114
2605	656	150
2606	656	231
2607	657	36
2608	657	260
2609	657	96
2610	657	256
2611	658	168
2612	658	105
2613	658	77
2614	658	116
2615	659	59
2616	659	36
2617	659	300
2618	659	202
2619	660	233
2620	660	290
2621	660	247
2622	660	50
2623	661	148
2624	661	86
2625	661	55
2626	661	57
2627	662	83
2628	662	76
2629	662	146
2630	662	131
2631	663	136
2632	663	112
2633	663	185
2634	663	244
2635	664	163
2636	664	231
2637	664	70
2638	664	91
2639	665	257
2640	665	81
2641	665	35
2642	665	118
2643	666	37
2644	666	60
2645	666	199
2646	666	248
2647	667	265
2648	667	237
2649	667	145
2650	667	300
2651	668	216
2652	668	103
2653	668	28
2654	668	14
2655	669	292
2656	669	187
2657	669	279
2658	669	46
2659	670	193
2660	670	136
2661	670	121
2662	670	124
2663	671	168
2664	671	260
2665	671	138
2666	671	3
2667	672	103
2668	672	205
2669	672	72
2670	672	153
2671	673	294
2672	673	261
2673	673	31
2674	673	103
2675	674	118
2676	674	46
2677	674	69
2678	674	151
2679	675	113
2680	675	85
2681	675	102
2682	675	231
2683	676	224
2684	676	218
2685	676	108
2686	676	193
2687	677	211
2688	677	44
2689	677	51
2690	677	23
2691	678	241
2692	678	166
2693	678	257
2694	678	189
2695	679	135
2696	679	233
2697	679	84
2698	679	248
2699	680	165
2700	680	207
2701	680	200
2702	680	248
2703	681	8
2704	681	251
2705	681	213
2706	681	228
2707	682	98
2708	682	166
2709	682	210
2710	682	27
2711	683	285
2712	683	143
2713	683	78
2714	683	240
2715	684	263
2716	684	49
2717	684	201
2718	684	35
2719	685	269
2720	685	257
2721	685	233
2722	685	127
2723	686	290
2724	686	235
2725	686	87
2726	686	214
2727	687	86
2728	687	142
2729	687	97
2730	687	110
2731	688	1
2732	688	219
2733	688	225
2734	688	189
2735	689	228
2736	689	163
2737	689	140
2738	689	153
2739	690	31
2740	690	159
2741	690	117
2742	690	140
2743	691	148
2744	691	301
2745	691	217
2746	691	26
2747	692	259
2748	692	114
2749	692	227
2750	693	150
2751	693	82
2752	693	42
2753	693	22
2754	694	267
2755	694	255
2756	694	9
2757	694	93
2758	695	14
2759	695	176
2760	695	21
2761	695	238
2762	696	18
2763	696	152
2764	696	142
2765	696	258
2766	697	284
2767	697	46
2768	697	268
2769	697	289
2770	698	287
2771	698	249
2772	698	158
2773	698	51
2774	699	270
2775	699	258
2776	699	117
2777	699	99
2778	700	128
2779	700	208
2780	700	108
2781	700	66
2782	701	43
2783	701	3
2784	701	125
2785	701	96
2786	702	193
2787	702	81
2788	702	5
2789	702	160
2790	703	156
2791	703	163
2792	703	84
2793	703	144
2794	704	76
2795	704	44
2796	704	105
2797	704	207
2798	705	210
2799	705	238
2800	705	57
2801	705	101
2802	706	147
2803	706	245
2804	706	186
2805	706	230
2806	707	131
2807	707	237
2808	707	281
2809	707	196
2810	708	286
2811	708	17
2812	708	264
2813	708	165
2814	709	88
2815	709	23
2816	709	220
2817	709	31
2818	710	149
2819	710	131
2820	710	115
2821	710	90
2822	711	71
2823	711	184
2824	711	185
2825	711	41
2826	712	182
2827	712	188
2828	712	146
2829	712	42
2830	713	163
2831	713	200
2832	713	94
2833	713	44
2834	714	199
2835	714	13
2836	714	183
2837	714	197
2838	715	86
2839	715	46
2840	715	23
2841	715	135
2842	716	67
2843	716	240
2844	716	68
2845	716	120
2846	717	28
2847	717	177
2848	717	141
2849	717	65
2850	718	152
2851	718	261
2852	718	255
2853	718	180
2854	719	278
2855	719	31
2856	719	198
2857	719	222
2858	720	30
2859	720	237
2860	720	213
2861	720	205
2862	721	3
2863	721	116
2864	721	197
2865	721	299
2866	722	57
2867	722	88
2868	722	102
2869	722	56
2870	723	62
2871	723	246
2872	723	52
2873	723	193
2874	724	94
2875	724	16
2876	724	261
2877	724	118
2878	725	171
2879	725	72
2880	725	274
2881	725	137
2882	726	67
2883	726	211
2884	726	190
2885	726	198
2886	727	15
2887	727	292
2888	727	95
2889	727	11
2890	728	250
2891	728	128
2892	728	67
2893	728	218
2894	729	9
2895	729	263
2896	729	287
2897	729	96
2898	730	260
2899	730	240
2900	730	252
2901	730	1
2902	731	256
2903	731	98
2904	731	150
2905	732	262
2906	732	33
2907	732	282
2908	732	19
2909	733	8
2910	733	90
2911	733	273
2912	733	104
2913	734	186
2914	734	141
2915	734	64
2916	734	177
2917	735	285
2918	735	297
2919	735	144
2920	735	92
2921	736	139
2922	736	247
2923	736	221
2924	736	233
2925	737	223
2926	737	131
2927	737	242
2928	737	108
2929	738	243
2930	738	278
2931	738	204
2932	738	187
2933	739	104
2934	739	9
2935	739	113
2936	739	138
2937	740	219
2938	740	252
2939	740	268
2940	741	146
2941	741	227
2942	741	296
2943	741	123
2944	742	270
2945	742	167
2946	742	13
2947	742	239
2948	743	3
2949	743	97
2950	743	151
2951	743	25
2952	744	72
2953	744	163
2954	744	168
2955	744	18
2956	745	122
2957	745	23
2958	745	204
2959	745	196
2960	746	178
2961	746	177
2962	746	251
2963	746	263
2964	747	76
2965	747	63
2966	747	9
2967	747	110
2968	748	48
2969	748	130
2970	748	8
2971	748	194
2972	749	241
2973	749	2
2974	749	101
2975	749	116
2976	750	63
2977	750	257
2978	750	71
2979	750	198
2980	751	85
2981	751	56
2982	751	288
2983	751	261
2984	752	237
2985	752	76
2986	752	64
2987	753	191
2988	753	238
2989	753	99
2990	753	78
2991	754	168
2992	754	22
2993	754	89
2994	754	60
2995	755	251
2996	755	214
2997	755	175
2998	755	157
2999	756	10
3000	756	103
3001	756	9
3002	756	253
3003	757	293
3004	757	111
3005	757	41
3006	757	177
3007	758	75
3008	758	113
3009	758	301
3010	758	255
3011	759	297
3012	759	145
3013	759	50
3014	759	13
3015	760	33
3016	760	56
3017	760	298
3018	760	135
3019	761	30
3020	761	26
3021	761	77
3022	761	131
3023	762	129
3024	762	56
3025	762	42
3026	762	68
3027	763	179
3028	763	249
3029	763	32
3030	763	215
3031	764	220
3032	764	23
3033	764	142
3034	764	107
3035	765	59
3036	765	12
3037	765	242
3038	765	42
3039	766	117
3040	766	128
3041	766	77
3042	766	26
3043	767	67
3044	767	130
3045	767	299
3046	767	18
3047	768	123
3048	768	104
3049	768	9
3050	768	133
3051	769	94
3052	769	152
3053	769	137
3054	769	39
3055	770	47
3056	770	174
3057	770	124
3058	770	97
3059	771	254
3060	771	142
3061	771	188
3062	771	112
3063	772	153
3064	772	301
3065	772	124
3066	772	4
3067	773	242
3068	773	121
3069	773	260
3070	773	50
3071	774	233
3072	774	282
3073	774	20
3074	774	16
3075	775	115
3076	775	297
3077	775	35
3078	775	150
3079	776	98
3080	776	265
3081	776	202
3082	776	267
3083	777	173
3084	777	260
3085	777	110
3086	777	116
3087	778	268
3088	778	27
3089	778	263
3090	778	194
3091	779	43
3092	779	100
3093	779	137
3094	779	99
3095	780	121
3096	780	239
3097	780	212
3098	780	289
3099	781	158
3100	781	10
3101	781	151
3102	781	274
3103	782	239
3104	782	47
3105	782	99
3106	782	225
3107	783	121
3108	783	159
3109	783	160
3110	783	62
3111	784	164
3112	784	91
3113	784	277
3114	784	274
3115	785	185
3116	785	54
3117	785	52
3118	785	281
3119	786	182
3120	786	136
3121	786	117
3122	786	147
3123	787	280
3124	787	264
3125	787	91
3126	787	180
3127	788	244
3128	788	71
3129	788	27
3130	788	241
3131	789	48
3132	789	13
3133	789	11
3134	789	114
3135	790	187
3136	790	163
3137	790	223
3138	790	52
3139	791	47
3140	791	103
3141	791	53
3142	791	74
3143	792	183
3144	792	301
3145	792	295
3146	792	172
3147	793	289
3148	793	107
3149	793	132
3150	793	246
3151	794	83
3152	794	258
3153	794	97
3154	794	170
3155	795	273
3156	795	136
3157	795	235
3158	795	123
3159	796	217
3160	796	211
3161	796	152
3162	796	208
3163	797	3
3164	797	192
3165	797	241
3166	797	43
3167	798	251
3168	798	4
3169	798	30
3170	798	11
3171	799	276
3172	799	110
3173	799	173
3174	799	36
3175	800	208
3176	800	289
3177	800	260
3178	800	88
3179	801	128
3180	801	71
3181	801	286
3182	801	119
3183	802	241
3184	802	164
3185	802	236
3186	802	73
3187	803	96
3188	803	11
3189	803	123
3190	803	262
3191	804	289
3192	804	37
3193	804	281
3194	804	153
3195	805	182
3196	805	1
3197	805	172
3198	806	75
3199	806	59
3200	806	8
3201	806	1
3202	807	296
3203	807	79
3204	807	124
3205	807	99
3206	808	23
3207	808	265
3208	808	197
3209	808	263
3210	809	23
3211	809	71
3212	809	132
3213	809	251
3214	810	85
3215	810	267
3216	810	29
3217	810	32
3218	811	212
3219	811	158
3220	811	63
3221	811	62
3222	812	131
3223	812	159
3224	812	32
3225	812	75
3226	813	87
3227	813	163
3228	813	252
3229	813	82
3230	814	137
3231	814	142
3232	814	100
3233	814	252
3234	815	60
3235	815	46
3236	815	259
3237	815	6
3238	816	222
3239	816	221
3240	816	162
3241	816	47
3242	817	289
3243	817	253
3244	817	264
3245	817	10
3246	818	74
3247	818	262
3248	818	161
3249	818	184
3250	819	219
3251	819	12
3252	819	56
3253	820	132
3254	820	209
3255	820	255
3256	820	40
3257	821	279
3258	821	184
3259	821	222
3260	821	44
3261	822	187
3262	822	17
3263	822	229
3264	822	239
3265	823	5
3266	823	90
3267	823	250
3268	823	301
3269	824	51
3270	824	194
3271	824	229
3272	824	189
3273	825	256
3274	825	76
3275	825	125
3276	825	180
3277	826	227
3278	826	263
3279	826	110
3280	826	235
3281	827	299
3282	827	102
3283	827	37
3284	827	137
3285	828	266
3286	828	167
3287	828	88
3288	828	215
3289	829	179
3290	829	25
3291	829	151
3292	829	83
3293	830	19
3294	830	91
3295	830	103
3296	830	47
3297	831	142
3298	831	224
3299	831	115
3300	831	267
3301	832	109
3302	832	113
3303	832	87
3304	832	162
3305	833	139
3306	833	278
3307	833	55
3308	833	206
3309	834	258
3310	834	128
3311	834	88
3312	834	42
3313	835	142
3314	835	113
3315	835	290
3316	835	120
3317	836	230
3318	836	110
3319	836	93
3320	836	112
3321	837	73
3322	837	177
3323	837	144
3324	837	67
3325	838	12
3326	838	98
3327	838	110
3328	838	57
3329	839	88
3330	839	295
3331	839	11
3332	839	24
3333	840	17
3334	840	252
3335	840	22
3336	840	24
3337	841	153
3338	841	25
3339	841	2
3340	841	254
3341	842	224
3342	842	85
3343	842	301
3344	842	132
3345	843	213
3346	843	218
3347	843	161
3348	843	176
3349	844	175
3350	844	92
3351	844	150
3352	844	253
3353	845	151
3354	845	99
3355	845	227
3356	845	252
3357	846	87
3358	846	219
3359	846	256
3360	846	135
3361	847	73
3362	847	231
3363	847	147
3364	847	47
3365	848	182
3366	848	178
3367	848	216
3368	848	196
3369	849	5
3370	849	14
3371	849	6
3372	849	202
3373	850	141
3374	850	95
3375	850	237
3376	850	40
3377	851	127
3378	851	285
3379	851	277
3380	851	28
3381	852	67
3382	852	125
3383	852	189
3384	852	214
3385	853	225
3386	853	97
3387	853	98
3388	853	300
3389	854	40
3390	854	139
3391	854	274
3392	854	53
3393	855	289
3394	855	104
3395	855	10
3396	855	222
3397	856	215
3398	856	32
3399	856	111
3400	856	205
3401	857	35
3402	857	64
3403	857	296
3404	857	74
3405	858	254
3406	858	57
3407	858	281
3408	858	179
3409	859	190
3410	859	102
3411	859	299
3412	859	203
3413	860	232
3414	860	92
3415	860	263
3416	860	175
3417	861	176
3418	861	11
3419	861	212
3420	861	38
3421	862	282
3422	862	74
3423	862	26
3424	862	273
3425	863	93
3426	863	108
3427	863	222
3428	863	287
3429	864	23
3430	864	264
3431	864	282
3432	864	148
3433	865	250
3434	865	209
3435	865	143
3436	865	108
3437	866	151
3438	866	77
3439	866	210
3440	866	142
3441	867	228
3442	867	43
3443	867	202
3444	867	282
3445	868	278
3446	868	285
3447	868	173
3448	868	49
3449	869	226
3450	869	43
3451	869	75
3452	869	90
3453	870	10
3454	870	40
3455	870	4
3456	870	226
3457	871	190
3458	871	91
3459	871	61
3460	871	225
3461	872	87
3462	872	7
3463	872	151
3464	872	142
3465	873	263
3466	873	187
3467	873	26
3468	873	293
3469	874	215
3470	874	297
3471	874	6
3472	874	92
3473	875	283
3474	875	131
3475	875	246
3476	875	210
3477	876	95
3478	876	25
3479	876	278
3480	876	73
3481	877	166
3482	877	103
3483	877	143
3484	877	24
3485	878	241
3486	878	294
3487	878	152
3488	878	76
3489	879	137
3490	879	42
3491	879	56
3492	879	260
3493	880	281
3494	880	235
3495	880	207
3496	880	79
3497	881	123
3498	881	278
3499	881	201
3500	881	8
3501	882	159
3502	882	194
3503	882	90
3504	882	168
3505	883	196
3506	883	102
3507	883	183
3508	883	122
3509	884	228
3510	884	242
3511	884	163
3512	884	90
3513	885	274
3514	885	275
3515	885	257
3516	885	224
3517	886	254
3518	886	261
3519	886	80
3520	886	200
3521	887	235
3522	887	36
3523	887	101
3524	887	67
3525	888	255
3526	888	85
3527	888	269
3528	888	44
3529	889	280
3530	889	143
3531	889	15
3532	889	113
3533	890	122
3534	890	1
3535	890	295
3536	890	170
3537	891	10
3538	891	134
3539	891	194
3540	891	239
3541	892	76
3542	892	217
3543	892	170
3544	892	18
3545	893	132
3546	893	276
3547	893	268
3548	893	277
3549	894	159
3550	894	206
3551	894	280
3552	894	37
3553	895	198
3554	895	242
3555	895	281
3556	895	163
3557	896	185
3558	896	98
3559	896	195
3560	896	107
3561	897	81
3562	897	158
3563	897	17
3564	897	131
3565	898	160
3566	898	13
3567	898	153
3568	898	14
3569	899	184
3570	899	34
3571	899	37
3572	899	53
3573	900	139
3574	900	198
3575	900	105
3576	900	78
3577	901	245
3578	901	155
3579	901	32
3580	901	257
3581	902	169
3582	902	88
3583	902	58
3584	902	170
3585	903	234
3586	903	162
3587	903	254
3588	903	128
3589	904	58
3590	904	240
3591	904	300
3592	904	13
3593	905	35
3594	905	90
3595	905	235
3596	905	190
3597	906	65
3598	906	168
3599	906	69
3600	906	78
3601	907	289
3602	907	86
3603	907	148
3604	907	33
3605	908	172
3606	908	64
3607	908	73
3608	908	278
3609	909	280
3610	909	84
3611	909	34
3612	909	277
3613	910	69
3614	910	253
3615	910	115
3616	910	233
3617	911	296
3618	911	136
3619	911	41
3620	911	271
3621	912	76
3622	912	49
3623	912	7
3624	912	67
3625	913	155
3626	913	25
3627	913	4
3628	913	10
3629	914	106
3630	914	66
3631	914	117
3632	914	116
3633	915	86
3634	915	192
3635	915	190
3636	915	147
3637	916	175
3638	916	147
3639	916	285
3640	916	137
3641	917	219
3642	917	93
3643	917	97
3644	917	34
3645	918	156
3646	918	187
3647	918	19
3648	918	209
3649	919	99
3650	919	54
3651	919	207
3652	919	264
3653	920	191
3654	920	148
3655	920	51
3656	920	199
3657	921	90
3658	921	66
3659	921	85
3660	921	44
3661	922	191
3662	922	129
3663	922	221
3664	922	153
3665	923	251
3666	923	75
3667	923	46
3668	923	235
3669	924	64
3670	924	293
3671	924	255
3672	924	120
3673	925	33
3674	925	76
3675	925	207
3676	925	282
3677	926	41
3678	926	119
3679	926	81
3680	926	22
3681	927	224
3682	927	139
3683	927	17
3684	927	239
3685	928	12
3686	928	9
3687	928	225
3688	928	143
3689	929	193
3690	929	66
3691	929	170
3692	929	152
3693	930	95
3694	930	92
3695	930	176
3696	930	250
3697	931	119
3698	931	191
3699	931	80
3700	931	10
3701	932	237
3702	932	96
3703	932	205
3704	932	89
3705	933	50
3706	933	190
3707	933	174
3708	933	66
3709	934	185
3710	934	17
3711	934	211
3712	934	190
3713	935	48
3714	935	278
3715	935	184
3716	935	202
3717	936	258
3718	936	287
3719	936	66
3720	936	257
3721	937	258
3722	937	47
3723	937	132
3724	937	32
3725	938	83
3726	938	218
3727	938	101
3728	938	141
3729	939	53
3730	939	5
3731	939	167
3732	939	283
3733	940	219
3734	940	107
3735	940	237
3736	940	176
3737	941	245
3738	941	293
3739	941	8
3740	941	39
3741	942	265
3742	942	207
3743	942	82
3744	942	278
3745	943	147
3746	943	258
3747	943	208
3748	943	281
3749	944	67
3750	944	86
3751	944	122
3752	944	88
3753	945	102
3754	945	151
3755	945	255
3756	945	35
3757	946	113
3758	946	63
3759	946	249
3760	946	33
3761	947	234
3762	947	60
3763	947	231
3764	947	3
3765	948	139
3766	948	24
3767	948	37
3768	948	235
3769	949	14
3770	949	120
3771	949	100
3772	949	61
3773	950	178
3774	950	295
3775	950	14
3776	950	74
3777	951	263
3778	951	34
3779	951	267
3780	951	79
3781	952	95
3782	952	48
3783	952	102
3784	952	130
3785	953	30
3786	953	219
3787	953	252
3788	953	237
3789	954	243
3790	954	233
3791	954	259
3792	954	218
3793	955	88
3794	955	234
3795	955	59
3796	955	133
3797	956	236
3798	956	207
3799	956	99
3800	956	70
3801	957	282
3802	957	20
3803	957	160
3804	957	247
3805	958	30
3806	958	117
3807	958	196
3808	958	287
3809	959	99
3810	959	113
3811	959	165
3812	959	15
3813	960	260
3814	960	292
3815	960	9
3816	960	154
3817	961	151
3818	961	124
3819	961	293
3820	961	78
3821	962	101
3822	962	255
3823	962	219
3824	962	37
3825	963	45
3826	963	88
3827	963	130
3828	963	49
3829	964	232
3830	964	227
3831	964	188
3832	964	67
3833	965	222
3834	965	296
3835	965	221
3836	965	158
3837	966	155
3838	966	34
3839	966	127
3840	966	119
3841	967	64
3842	967	87
3843	967	151
3844	967	208
3845	968	296
3846	968	57
3847	968	237
3848	968	150
3849	969	26
3850	969	268
3851	969	39
3852	969	86
3853	970	37
3854	970	99
3855	970	31
3856	970	265
3857	971	55
3858	971	180
3859	971	107
3860	971	260
3861	972	3
3862	972	4
3863	972	117
3864	972	271
3865	973	19
3866	973	250
3867	973	258
3868	973	204
3869	974	290
3870	974	210
3871	974	69
3872	974	262
3873	975	122
3874	975	251
3875	975	300
3876	975	160
3877	976	75
3878	976	171
3879	976	296
3880	976	8
3881	977	187
3882	977	174
3883	977	32
3884	977	15
3885	978	21
3886	978	261
3887	978	12
3888	978	29
3889	979	98
3890	979	173
3891	979	16
3892	979	207
3893	980	86
3894	980	174
3895	980	215
3896	980	146
3897	981	242
3898	981	95
3899	981	137
3900	981	203
3901	982	18
3902	982	207
3903	982	170
3904	982	225
3905	983	108
3906	983	251
3907	983	112
3908	983	64
3909	984	235
3910	984	110
3911	984	55
3912	984	223
3913	985	99
3914	985	48
3915	985	39
3916	985	230
3917	986	87
3918	986	280
3919	986	127
3920	986	82
3921	987	106
3922	987	148
3923	987	58
3924	987	9
3925	988	208
3926	988	56
3927	988	257
3928	988	90
3929	989	125
3930	989	180
3931	989	292
3932	989	242
3933	990	162
3934	990	44
3935	990	96
3936	990	217
3937	991	94
3938	991	180
3939	991	165
3940	991	194
3941	992	248
3942	992	2
3943	992	291
3944	992	220
3945	993	149
3946	993	298
3947	993	285
3948	993	216
3949	994	10
3950	994	38
3951	994	182
3952	994	262
3953	995	250
3954	995	11
3955	995	166
3956	995	101
3957	996	70
3958	996	202
3959	996	53
3960	996	39
3961	997	222
3962	997	215
3963	997	77
3964	997	65
3965	998	78
3966	998	46
3967	998	136
3968	998	173
3969	999	74
3970	999	298
3971	999	243
3972	999	255
3973	1000	205
3974	1000	134
3975	1000	214
3976	1000	277
3977	1001	259
3978	1001	229
3979	1001	170
3980	1001	12
3981	1002	390
3982	1002	545
3983	1002	376
3984	1002	415
3985	1003	456
3986	1003	454
3987	1003	413
3988	1003	475
3989	1004	453
3990	1004	434
3991	1004	366
3992	1004	417
3993	1005	474
3994	1005	304
3995	1005	400
3996	1005	503
3997	1006	374
3998	1006	576
3999	1006	410
4000	1006	388
4001	1007	318
4002	1007	312
4003	1007	536
4004	1007	376
4005	1008	445
4006	1008	316
4007	1008	414
4008	1008	547
4009	1009	487
4010	1009	564
4011	1009	367
4012	1009	503
4013	1010	457
4014	1010	529
4015	1010	462
4016	1010	538
4017	1011	528
4018	1011	451
4019	1011	577
4020	1011	354
4021	1012	580
4022	1012	457
4023	1012	542
4024	1012	305
4025	1013	423
4026	1013	400
4027	1013	408
4028	1013	336
4029	1014	321
4030	1014	429
4031	1014	486
4032	1014	357
4033	1015	508
4034	1015	372
4035	1015	368
4036	1015	595
4037	1016	439
4038	1016	391
4039	1016	326
4040	1016	582
4041	1017	439
4042	1017	385
4043	1017	371
4044	1017	419
4045	1018	383
4046	1018	482
4047	1018	398
4048	1018	599
4049	1019	440
4050	1019	313
4051	1019	530
4052	1019	447
4053	1020	518
4054	1020	565
4055	1020	329
4056	1020	598
4057	1021	537
4058	1021	512
4059	1021	432
4060	1021	500
4061	1022	392
4062	1022	598
4063	1022	310
4064	1022	502
4065	1023	554
4066	1023	561
4067	1023	474
4068	1023	519
4069	1024	519
4070	1024	589
4071	1024	352
4072	1024	462
4073	1025	589
4074	1025	391
4075	1025	588
4076	1025	593
4077	1026	563
4078	1026	594
4079	1026	476
4080	1026	522
4081	1027	401
4082	1027	386
4083	1027	535
4084	1027	531
4085	1028	593
4086	1028	583
4087	1028	319
4088	1028	385
4089	1029	385
4090	1029	450
4091	1029	446
4092	1029	372
4093	1030	302
4094	1030	547
4095	1030	471
4096	1030	461
4097	1031	551
4098	1031	421
4099	1031	489
4100	1031	536
4101	1032	589
4102	1032	494
4103	1032	530
4104	1032	497
4105	1033	318
4106	1033	552
4107	1033	516
4108	1033	451
4109	1034	322
4110	1034	323
4111	1034	377
4112	1034	409
4113	1035	348
4114	1035	427
4115	1035	373
4116	1035	523
4117	1036	394
4118	1036	331
4119	1036	579
4120	1036	384
4121	1037	371
4122	1037	314
4123	1037	441
4124	1037	413
4125	1038	405
4126	1038	514
4127	1038	529
4128	1038	504
4129	1039	585
4130	1039	575
4131	1039	582
4132	1039	477
4133	1040	409
4134	1040	515
4135	1040	308
4136	1040	311
4137	1041	597
4138	1041	329
4139	1041	525
4140	1041	590
4141	1042	588
4142	1042	600
4143	1042	347
4144	1042	561
4145	1043	553
4146	1043	558
4147	1043	454
4148	1043	392
4149	1044	456
4150	1044	362
4151	1044	529
4152	1044	581
4153	1045	583
4154	1045	414
4155	1045	347
4156	1045	573
4157	1046	554
4158	1046	533
4159	1046	377
4160	1046	356
4161	1047	307
4162	1047	438
4163	1047	586
4164	1047	569
4165	1048	329
4166	1048	424
4167	1048	326
4168	1048	456
4169	1049	573
4170	1049	497
4171	1049	346
4172	1049	394
4173	1050	421
4174	1050	494
4175	1050	559
4176	1050	464
4177	1051	535
4178	1051	566
4179	1051	431
4180	1051	329
4181	1052	537
4182	1052	597
4183	1052	392
4184	1052	542
4185	1053	529
4186	1053	543
4187	1053	509
4188	1053	307
4189	1054	519
4190	1054	356
4191	1054	534
4192	1054	384
4193	1055	504
4194	1055	542
4195	1055	495
4196	1055	577
4197	1056	443
4198	1056	592
4199	1056	546
4200	1056	365
4201	1057	448
4202	1057	384
4203	1057	517
4204	1057	349
4205	1058	491
4206	1058	356
4207	1058	420
4208	1058	498
4209	1059	329
4210	1059	447
4211	1059	415
4212	1059	598
4213	1060	582
4214	1060	396
4215	1060	409
4216	1060	533
4217	1061	404
4218	1061	345
4219	1061	309
4220	1061	322
4221	1062	491
4222	1062	352
4223	1062	430
4224	1062	355
4225	1063	377
4226	1063	585
4227	1063	530
4228	1063	479
4229	1064	465
4230	1064	468
4231	1064	569
4232	1064	483
4233	1065	325
4234	1065	439
4235	1065	491
4236	1065	328
4237	1066	512
4238	1066	596
4239	1066	559
4240	1066	494
4241	1067	427
4242	1067	362
4243	1067	419
4244	1067	511
4245	1068	516
4246	1068	334
4247	1068	601
4248	1068	594
4249	1069	465
4250	1069	422
4251	1069	487
4252	1069	561
4253	1070	445
4254	1070	542
4255	1070	468
4256	1071	550
4257	1071	350
4258	1071	427
4259	1071	418
4260	1072	506
4261	1072	483
4262	1072	422
4263	1072	486
4264	1073	385
4265	1073	589
4266	1073	573
4267	1073	597
4268	1074	567
4269	1074	312
4270	1074	456
4271	1074	522
4272	1075	416
4273	1075	394
4274	1075	502
4275	1076	499
4276	1076	380
4277	1076	372
4278	1076	562
4279	1077	508
4280	1077	407
4281	1077	597
4282	1077	536
4283	1078	569
4284	1078	510
4285	1078	597
4286	1078	374
4287	1079	323
4288	1079	419
4289	1079	305
4290	1079	338
4291	1080	514
4292	1080	367
4293	1080	401
4294	1080	391
4295	1081	515
4296	1081	596
4297	1081	431
4298	1081	384
4299	1082	499
4300	1082	594
4301	1082	380
4302	1082	557
4303	1083	522
4304	1083	383
4305	1083	318
4306	1083	521
4307	1084	438
4308	1084	435
4309	1084	496
4310	1084	519
4311	1085	317
4312	1085	544
4313	1085	311
4314	1085	579
4315	1086	547
4316	1086	455
4317	1086	397
4318	1086	410
4319	1087	563
4320	1087	567
4321	1087	458
4322	1087	439
4323	1088	514
4324	1088	532
4325	1088	339
4326	1088	400
4327	1089	351
4328	1089	323
4329	1089	373
4330	1089	537
4331	1090	543
4332	1090	307
4333	1090	596
4334	1090	548
4335	1091	574
4336	1091	483
4337	1091	501
4338	1091	326
4339	1092	540
4340	1092	455
4341	1092	488
4342	1092	543
4343	1093	320
4344	1093	354
4345	1093	541
4346	1093	371
4347	1094	535
4348	1094	538
4349	1094	482
4350	1094	574
4351	1095	592
4352	1095	523
4353	1095	528
4354	1095	404
4355	1096	312
4356	1096	455
4357	1096	423
4358	1096	340
4359	1097	459
4360	1097	417
4361	1097	408
4362	1097	562
4363	1098	372
4364	1098	543
4365	1098	419
4366	1098	580
4367	1099	352
4368	1099	347
4369	1099	407
4370	1099	535
4371	1100	360
4372	1100	412
4373	1100	330
4374	1100	363
4375	1101	399
4376	1101	348
4377	1101	558
4378	1101	553
4379	1102	336
4380	1102	427
4381	1102	545
4382	1102	389
4383	1103	548
4384	1103	414
4385	1103	431
4386	1103	552
4387	1104	472
4388	1104	557
4389	1104	512
4390	1104	483
4391	1105	577
4392	1105	370
4393	1105	332
4394	1105	567
4395	1106	578
4396	1106	503
4397	1106	514
4398	1106	497
4399	1107	522
4400	1107	562
4401	1107	468
4402	1107	483
4403	1108	415
4404	1108	388
4405	1108	495
4406	1108	395
4407	1109	338
4408	1109	592
4409	1109	348
4410	1109	586
4411	1110	424
4412	1110	379
4413	1110	349
4414	1110	304
4415	1111	356
4416	1111	510
4417	1111	325
4418	1111	353
4419	1112	454
4420	1112	396
4421	1112	524
4422	1112	378
4423	1113	379
4424	1113	452
4425	1113	523
4426	1113	447
4427	1114	415
4428	1114	576
4429	1114	469
4430	1114	474
4431	1115	560
4432	1115	444
4433	1115	428
4434	1115	456
4435	1116	504
4436	1116	361
4437	1116	462
4438	1116	557
4439	1117	346
4440	1117	566
4441	1117	459
4442	1117	441
4443	1118	558
4444	1118	565
4445	1118	534
4446	1118	344
4447	1119	398
4448	1119	462
4449	1119	510
4450	1119	334
4451	1120	404
4452	1120	382
4453	1120	538
4454	1120	467
4455	1121	485
4456	1121	384
4457	1121	425
4458	1121	397
4459	1122	484
4460	1122	384
4461	1122	334
4462	1122	396
4463	1123	333
4464	1123	518
4465	1123	390
4466	1123	429
4467	1124	530
4468	1124	337
4469	1124	586
4470	1124	371
4471	1125	344
4472	1125	521
4473	1125	577
4474	1125	367
4475	1126	580
4476	1126	590
4477	1126	497
4478	1126	573
4479	1127	539
4480	1127	555
4481	1127	395
4482	1127	382
4483	1128	454
4484	1128	600
4485	1128	597
4486	1128	572
4487	1129	381
4488	1129	460
4489	1129	333
4490	1129	410
4491	1130	347
4492	1130	562
4493	1130	343
4494	1130	342
4495	1131	370
4496	1131	325
4497	1131	362
4498	1131	506
4499	1132	497
4500	1132	415
4501	1132	390
4502	1132	381
4503	1133	353
4504	1133	414
4505	1133	439
4506	1133	415
4507	1134	509
4508	1134	469
4509	1134	310
4510	1134	426
4511	1135	325
4512	1135	391
4513	1135	359
4514	1135	534
4515	1136	492
4516	1136	413
4517	1136	371
4518	1136	310
4519	1137	520
4520	1137	497
4521	1137	492
4522	1137	406
4523	1138	321
4524	1138	496
4525	1138	342
4526	1138	567
4527	1139	581
4528	1139	383
4529	1139	450
4530	1139	413
4531	1140	412
4532	1140	327
4533	1140	323
4534	1140	324
4535	1141	413
4536	1141	355
4537	1141	341
4538	1141	411
4539	1142	448
4540	1142	427
4541	1142	381
4542	1142	457
4543	1143	530
4544	1143	553
4545	1143	319
4546	1143	336
4547	1144	399
4548	1144	396
4549	1144	381
4550	1144	576
4551	1145	546
4552	1145	489
4553	1145	474
4554	1145	358
4555	1146	452
4556	1146	526
4557	1146	426
4558	1146	460
4559	1147	355
4560	1147	381
4561	1147	346
4562	1147	423
4563	1148	537
4564	1148	502
4565	1148	477
4566	1148	455
4567	1149	306
4568	1149	346
4569	1149	367
4570	1149	558
4571	1150	342
4572	1150	487
4573	1150	410
4574	1150	481
4575	1151	423
4576	1151	428
4577	1151	335
4578	1151	440
4579	1152	302
4580	1152	444
4581	1152	565
4582	1152	472
4583	1153	486
4584	1153	588
4585	1153	540
4586	1153	412
4587	1154	304
4588	1154	595
4589	1154	345
4590	1154	314
4591	1155	408
4592	1155	588
4593	1155	489
4594	1155	436
4595	1156	357
4596	1156	539
4597	1156	430
4598	1156	524
4599	1157	308
4600	1157	505
4601	1157	596
4602	1157	397
4603	1158	538
4604	1158	470
4605	1158	558
4606	1158	394
4607	1159	414
4608	1159	317
4609	1159	321
4610	1159	426
4611	1160	474
4612	1160	557
4613	1160	384
4614	1160	570
4615	1161	569
4616	1161	350
4617	1161	371
4618	1161	402
4619	1162	452
4620	1162	382
4621	1162	330
4622	1162	557
4623	1163	556
4624	1163	336
4625	1163	353
4626	1163	505
4627	1164	588
4628	1164	528
4629	1164	442
4630	1164	403
4631	1165	579
4632	1165	455
4633	1165	512
4634	1165	354
4635	1166	329
4636	1166	594
4637	1166	499
4638	1166	478
4639	1167	542
4640	1167	454
4641	1167	311
4642	1167	458
4643	1168	315
4644	1168	406
4645	1168	304
4646	1168	514
4647	1169	497
4648	1169	575
4649	1169	548
4650	1169	558
4651	1170	362
4652	1170	561
4653	1170	305
4654	1170	347
4655	1171	432
4656	1171	477
4657	1171	359
4658	1171	525
4659	1172	566
4660	1172	330
4661	1172	394
4662	1172	483
4663	1173	348
4664	1173	379
4665	1173	496
4666	1173	426
4667	1174	437
4668	1174	569
4669	1174	304
4670	1174	521
4671	1175	446
4672	1175	509
4673	1175	367
4674	1175	467
4675	1176	354
4676	1176	473
4677	1176	337
4678	1176	540
4679	1177	385
4680	1177	490
4681	1177	339
4682	1177	390
4683	1178	549
4684	1178	578
4685	1178	555
4686	1178	450
4687	1179	358
4688	1179	305
4689	1179	315
4690	1179	484
4691	1180	347
4692	1180	326
4693	1180	521
4694	1180	553
4695	1181	336
4696	1181	551
4697	1181	552
4698	1181	533
4699	1182	557
4700	1182	465
4701	1182	458
4702	1182	600
4703	1183	574
4704	1183	525
4705	1183	326
4706	1183	568
4707	1184	460
4708	1184	309
4709	1184	369
4710	1184	392
4711	1185	363
4712	1185	302
4713	1185	544
4714	1185	462
4715	1186	584
4716	1186	600
4717	1186	504
4718	1186	404
4719	1187	374
4720	1187	314
4721	1187	359
4722	1187	485
4723	1188	329
4724	1188	353
4725	1188	435
4726	1188	484
4727	1189	534
4728	1189	400
4729	1189	521
4730	1189	423
4731	1190	318
4732	1190	424
4733	1190	483
4734	1190	563
4735	1191	334
4736	1191	389
4737	1191	447
4738	1191	382
4739	1192	446
4740	1192	474
4741	1192	511
4742	1192	471
4743	1193	416
4744	1193	349
4745	1193	536
4746	1193	415
4747	1194	463
4748	1194	327
4749	1194	366
4750	1194	544
4751	1195	395
4752	1195	518
4753	1195	325
4754	1195	509
4755	1196	378
4756	1196	346
4757	1196	523
4758	1196	435
4759	1197	393
4760	1197	327
4761	1197	452
4762	1197	559
4763	1198	305
4764	1198	355
4765	1198	493
4766	1198	427
4767	1199	424
4768	1199	452
4769	1199	443
4770	1199	503
4771	1200	499
4772	1200	508
4773	1200	407
4774	1200	360
4775	1201	472
4776	1201	361
4777	1201	552
4778	1201	589
4779	1202	310
4780	1202	439
4781	1202	332
4782	1203	484
4783	1203	519
4784	1203	505
4785	1203	308
4786	1204	581
4787	1204	377
4788	1204	482
4789	1204	597
4790	1205	462
4791	1205	370
4792	1205	565
4793	1205	418
4794	1206	556
4795	1206	514
4796	1206	346
4797	1206	320
4798	1207	414
4799	1207	339
4800	1207	557
4801	1207	492
4802	1208	353
4803	1208	554
4804	1208	398
4805	1208	517
4806	1209	470
4807	1209	332
4808	1209	579
4809	1209	370
4810	1210	520
4811	1210	422
4812	1210	517
4813	1210	502
4814	1211	471
4815	1211	524
4816	1211	363
4817	1211	342
4818	1212	587
4819	1212	530
4820	1212	395
4821	1212	412
4822	1213	496
4823	1213	384
4824	1213	314
4825	1213	477
4826	1214	415
4827	1214	592
4828	1214	554
4829	1214	452
4830	1215	347
4831	1215	373
4832	1215	586
4833	1215	450
4834	1216	351
4835	1216	479
4836	1216	585
4837	1216	515
4838	1217	591
4839	1217	513
4840	1217	334
4841	1217	568
4842	1218	439
4843	1218	474
4844	1218	578
4845	1218	308
4846	1219	544
4847	1219	546
4848	1219	392
4849	1219	304
4850	1220	343
4851	1220	481
4852	1220	431
4853	1220	516
4854	1221	503
4855	1221	365
4856	1221	555
4857	1221	364
4858	1222	420
4859	1222	474
4860	1222	513
4861	1222	602
4862	1223	574
4863	1223	336
4864	1223	364
4865	1223	492
4866	1224	560
4867	1224	364
4868	1224	320
4869	1224	348
4870	1225	541
4871	1225	487
4872	1225	438
4873	1225	305
4874	1226	387
4875	1226	329
4876	1226	476
4877	1226	596
4878	1227	302
4879	1227	515
4880	1227	365
4881	1227	536
4882	1228	534
4883	1228	438
4884	1228	551
4885	1228	318
4886	1229	537
4887	1229	402
4888	1229	591
4889	1229	419
4890	1230	563
4891	1230	580
4892	1230	468
4893	1230	378
4894	1231	318
4895	1231	435
4896	1231	480
4897	1232	309
4898	1232	540
4899	1232	505
4900	1232	378
4901	1233	396
4902	1233	562
4903	1233	601
4904	1233	431
4905	1234	337
4906	1234	330
4907	1234	440
4908	1234	339
4909	1235	483
4910	1235	425
4911	1235	362
4912	1235	372
4913	1236	468
4914	1236	585
4915	1236	357
4916	1236	451
4917	1237	418
4918	1237	388
4919	1237	430
4920	1237	345
4921	1238	337
4922	1238	504
4923	1238	551
4924	1238	454
4925	1239	553
4926	1239	390
4927	1239	585
4928	1239	592
4929	1240	571
4930	1240	450
4931	1240	419
4932	1240	530
4933	1241	493
4934	1241	336
4935	1241	595
4936	1241	400
4937	1242	544
4938	1242	527
4939	1242	594
4940	1242	302
4941	1243	529
4942	1243	453
4943	1243	315
4944	1243	324
4945	1244	534
4946	1244	515
4947	1244	363
4948	1244	548
4949	1245	362
4950	1245	383
4951	1245	584
4952	1245	365
4953	1246	522
4954	1246	437
4955	1246	411
4956	1246	321
4957	1247	537
4958	1247	392
4959	1247	324
4960	1247	561
4961	1248	464
4962	1248	409
4963	1248	509
4964	1248	548
4965	1249	387
4966	1249	396
4967	1249	418
4968	1249	471
4969	1250	320
4970	1250	482
4971	1250	578
4972	1250	460
4973	1251	591
4974	1251	334
4975	1251	337
4976	1251	450
4977	1252	512
4978	1252	568
4979	1252	576
4980	1252	360
4981	1253	353
4982	1253	421
4983	1253	600
4984	1253	327
4985	1254	544
4986	1254	366
4987	1254	369
4988	1254	587
4989	1255	420
4990	1255	522
4991	1255	499
4992	1255	534
4993	1256	494
4994	1256	550
4995	1256	500
4996	1256	440
4997	1257	491
4998	1257	308
4999	1257	327
5000	1257	526
5001	1258	529
5002	1258	456
5003	1258	368
5004	1258	445
5005	1259	432
5006	1259	471
5007	1259	488
5008	1259	556
5009	1260	513
5010	1260	584
5011	1260	596
5012	1260	404
5013	1261	377
5014	1261	541
5015	1261	556
5016	1261	481
5017	1262	583
5018	1262	448
5019	1262	476
5020	1262	372
5021	1263	519
5022	1263	367
5023	1263	378
5024	1263	371
5025	1264	549
5026	1264	378
5027	1264	461
5028	1264	482
5029	1265	324
5030	1265	387
5031	1265	401
5032	1265	513
5033	1266	444
5034	1266	538
5035	1266	475
5036	1266	369
5037	1267	422
5038	1267	470
5039	1267	600
5040	1267	391
5041	1268	432
5042	1268	351
5043	1268	400
5044	1268	311
5045	1269	485
5046	1269	590
5047	1269	490
5048	1269	470
5049	1270	519
5050	1270	534
5051	1270	560
5052	1270	398
5053	1271	556
5054	1271	384
5055	1271	473
5056	1271	599
5057	1272	467
5058	1272	597
5059	1272	583
5060	1272	450
5061	1273	523
5062	1273	527
5063	1273	402
5064	1273	327
5065	1274	383
5066	1274	568
5067	1274	534
5068	1274	486
5069	1275	574
5070	1275	399
5071	1275	330
5072	1275	404
5073	1276	366
5074	1276	510
5075	1276	387
5076	1276	425
5077	1277	330
5078	1277	451
5079	1277	459
5080	1277	388
5081	1278	550
5082	1278	574
5083	1278	416
5084	1278	396
5085	1279	475
5086	1279	356
5087	1279	566
5088	1279	598
5089	1280	305
5090	1280	344
5091	1280	577
5092	1280	330
5093	1281	302
5094	1281	375
5095	1281	556
5096	1281	320
5097	1282	373
5098	1282	536
5099	1282	408
5100	1282	424
5101	1283	374
5102	1283	509
5103	1283	446
5104	1283	427
5105	1284	382
5106	1284	328
5107	1284	566
5108	1284	448
5109	1285	441
5110	1285	549
5111	1285	587
5112	1285	329
5113	1286	540
5114	1286	552
5115	1286	508
5116	1286	599
5117	1287	425
5118	1287	424
5119	1287	418
5120	1287	380
5121	1288	445
5122	1288	304
5123	1288	599
5124	1288	420
5125	1289	487
5126	1289	595
5127	1289	593
5128	1289	320
5129	1290	376
5130	1290	463
5131	1290	313
5132	1290	477
5133	1291	443
5134	1291	371
5135	1291	449
5136	1291	475
5137	1292	537
5138	1292	486
5139	1292	396
5140	1292	376
5141	1293	409
5142	1293	431
5143	1293	538
5144	1293	330
5145	1294	396
5146	1294	512
5147	1294	478
5148	1294	323
5149	1295	335
5150	1295	543
5151	1295	485
5152	1295	516
5153	1296	484
5154	1296	339
5155	1296	564
5156	1297	587
5157	1297	313
5158	1297	350
5159	1297	555
5160	1298	523
5161	1298	566
5162	1298	537
5163	1298	553
5164	1299	553
5165	1299	469
5166	1299	337
5167	1299	314
5168	1300	383
5169	1300	350
5170	1300	327
5171	1300	363
5172	1301	512
5173	1301	472
5174	1301	505
5175	1301	344
5176	1302	418
5177	1302	494
5178	1302	497
5179	1302	307
5180	1303	519
5181	1303	449
5182	1303	429
5183	1303	406
5184	1304	597
5185	1304	391
5186	1304	528
5187	1304	543
5188	1305	464
5189	1305	332
5190	1305	532
5191	1305	565
5192	1306	408
5193	1306	397
5194	1306	343
5195	1306	500
5196	1307	563
5197	1307	565
5198	1307	594
5199	1307	590
5200	1308	525
5201	1308	509
5202	1308	363
5203	1308	476
5204	1309	414
5205	1309	339
5206	1309	584
5207	1309	394
5208	1310	577
5209	1310	345
5210	1310	578
5211	1310	517
5212	1311	365
5213	1311	445
5214	1311	473
5215	1311	397
5216	1312	450
5217	1312	540
5218	1312	420
5219	1312	302
5220	1313	576
5221	1313	364
5222	1313	512
5223	1313	418
5224	1314	583
5225	1314	378
5226	1314	362
5227	1314	468
5228	1315	320
5229	1315	514
5230	1315	524
5231	1315	522
5232	1316	372
5233	1316	479
5234	1316	340
5235	1316	317
5236	1317	479
5237	1317	548
5238	1317	407
5239	1317	400
5240	1318	565
5241	1318	592
5242	1318	478
5243	1318	341
5244	1319	429
5245	1319	326
5246	1319	458
5247	1319	600
5248	1320	336
5249	1320	485
5250	1320	339
5251	1320	492
5252	1321	547
5253	1321	415
5254	1321	509
5255	1321	369
5256	1322	408
5257	1322	599
5258	1322	418
5259	1322	333
5260	1323	424
5261	1323	602
5262	1323	502
5263	1323	441
5264	1324	402
5265	1324	501
5266	1324	384
5267	1324	507
5268	1325	511
5269	1325	457
5270	1325	466
5271	1325	516
5272	1326	416
5273	1326	307
5274	1326	518
5275	1326	365
5276	1327	333
5277	1327	600
5278	1327	518
5279	1327	599
5280	1328	444
5281	1328	321
5282	1328	307
5283	1328	398
5284	1329	573
5285	1329	316
5286	1329	468
5287	1329	360
5288	1330	362
5289	1330	478
5290	1330	312
5291	1330	386
5292	1331	572
5293	1331	366
5294	1331	393
5295	1331	395
5296	1332	525
5297	1332	399
5298	1332	397
5299	1332	541
5300	1333	577
5301	1333	484
5302	1333	548
5303	1333	310
5304	1334	403
5305	1334	390
5306	1334	443
5307	1334	431
5308	1335	482
5309	1335	598
5310	1335	316
5311	1335	422
5312	1336	478
5313	1336	578
5314	1336	581
5315	1336	491
5316	1337	570
5317	1337	568
5318	1337	564
5319	1337	428
5320	1338	324
5321	1338	377
5322	1338	577
5323	1338	594
5324	1339	404
5325	1339	454
5326	1339	478
5327	1339	584
5328	1340	386
5329	1340	475
5330	1340	541
5331	1340	587
5332	1341	597
5333	1341	527
5334	1341	373
5335	1341	423
5336	1342	345
5337	1342	340
5338	1342	585
5339	1342	448
5340	1343	525
5341	1343	417
5342	1343	494
5343	1343	433
5344	1344	503
5345	1344	416
5346	1344	394
5347	1344	347
5348	1345	597
5349	1345	512
5350	1345	508
5351	1345	424
5352	1346	524
5353	1346	421
5354	1346	303
5355	1346	340
5356	1347	421
5357	1347	568
5358	1347	594
5359	1347	422
5360	1348	393
5361	1348	509
5362	1348	444
5363	1348	322
5364	1349	474
5365	1349	407
5366	1349	543
5367	1349	557
5368	1350	365
5369	1350	492
5370	1350	480
5371	1350	350
5372	1351	376
5373	1351	425
5374	1351	520
5375	1351	541
5376	1352	500
5377	1352	518
5378	1352	479
5379	1352	597
5380	1353	516
5381	1353	323
5382	1353	405
5383	1353	439
5384	1354	575
5385	1354	480
5386	1354	517
5387	1354	524
5388	1355	383
5389	1355	460
5390	1355	562
5391	1355	438
5392	1356	363
5393	1356	440
5394	1356	438
5395	1356	387
5396	1357	375
5397	1357	401
5398	1357	384
5399	1357	435
5400	1358	458
5401	1358	527
5402	1358	368
5403	1358	540
5404	1359	317
5405	1359	434
5406	1359	460
5407	1359	399
5408	1360	484
5409	1360	578
5410	1360	458
5411	1360	376
5412	1361	307
5413	1361	432
5414	1361	596
5415	1361	338
5416	1362	515
5417	1362	397
5418	1362	506
5419	1362	536
5420	1363	344
5421	1363	404
5422	1363	314
5423	1363	498
5424	1364	436
5425	1364	399
5426	1364	464
5427	1364	338
5428	1365	357
5429	1365	526
5430	1365	474
5431	1365	543
5432	1366	337
5433	1366	485
5434	1366	411
5435	1366	344
5436	1367	556
5437	1367	360
5438	1367	401
5439	1367	336
5440	1368	405
5441	1368	354
5442	1368	582
5443	1368	579
5444	1369	321
5445	1369	440
5446	1369	359
5447	1369	413
5448	1370	303
5449	1370	462
5450	1370	601
5451	1370	576
5452	1371	310
5453	1371	565
5454	1371	497
5455	1371	600
5456	1372	576
5457	1372	534
5458	1372	443
5459	1372	569
5460	1373	326
5461	1373	588
5462	1373	350
5463	1373	503
5464	1374	513
5465	1374	583
5466	1374	328
5467	1374	469
5468	1375	545
5469	1375	576
5470	1375	389
5471	1375	485
5472	1376	313
5473	1376	481
5474	1376	409
5475	1376	364
5476	1377	535
5477	1377	495
5478	1377	501
5479	1377	447
5480	1378	434
5481	1378	432
5482	1378	305
5483	1378	370
5484	1379	557
5485	1379	550
5486	1379	575
5487	1379	413
5488	1380	424
5489	1380	506
5490	1380	307
5491	1380	368
5492	1381	326
5493	1381	559
5494	1381	555
5495	1381	460
5496	1382	469
5497	1382	395
5498	1382	525
5499	1382	560
5500	1383	589
5501	1383	399
5502	1383	584
5503	1383	353
5504	1384	551
5505	1384	475
5506	1384	302
5507	1384	352
5508	1385	342
5509	1385	595
5510	1385	492
5511	1385	318
5512	1386	482
5513	1386	489
5514	1386	458
5515	1386	393
5516	1387	373
5517	1387	524
5518	1387	500
5519	1387	358
5520	1388	547
5521	1388	366
5522	1388	397
5523	1388	410
5524	1389	549
5525	1389	492
5526	1389	560
5527	1389	332
5528	1390	398
5529	1390	500
5530	1390	551
5531	1390	457
5532	1391	489
5533	1391	571
5534	1391	359
5535	1391	432
5536	1392	312
5537	1392	340
5538	1392	589
5539	1392	489
5540	1393	352
5541	1393	474
5542	1393	422
5543	1393	593
5544	1394	412
5545	1394	383
5546	1394	452
5547	1394	525
5548	1395	525
5549	1395	357
5550	1395	589
5551	1395	336
5552	1396	601
5553	1396	586
5554	1396	561
5555	1396	497
5556	1397	311
5557	1397	338
5558	1397	491
5559	1397	340
5560	1398	377
5561	1398	563
5562	1398	343
5563	1398	320
5564	1399	544
5565	1399	550
5566	1399	474
5567	1399	570
5568	1400	506
5569	1400	433
5570	1400	501
5571	1400	381
5572	1401	481
5573	1401	366
5574	1401	423
5575	1401	454
5576	1402	360
5577	1402	519
5578	1402	457
5579	1402	520
5580	1403	586
5581	1403	481
5582	1403	530
5583	1403	473
5584	1404	393
5585	1404	457
5586	1404	353
5587	1404	485
5588	1405	502
5589	1405	515
5590	1405	498
5591	1405	415
5592	1406	536
5593	1406	571
5594	1406	569
5595	1406	441
5596	1407	374
5597	1407	585
5598	1407	407
5599	1407	311
5600	1408	490
5601	1408	431
5602	1408	342
5603	1408	443
5604	1409	554
5605	1409	521
5606	1409	509
5607	1409	537
5608	1410	433
5609	1410	519
5610	1410	305
5611	1410	583
5612	1411	320
5613	1411	314
5614	1411	600
5615	1411	361
5616	1412	484
5617	1412	367
5618	1412	573
5619	1412	312
5620	1413	451
5621	1413	585
5622	1413	466
5623	1413	483
5624	1414	312
5625	1414	538
5626	1414	425
5627	1414	419
5628	1415	383
5629	1415	582
5630	1415	569
5631	1415	440
5632	1416	596
5633	1416	529
5634	1416	406
5635	1416	541
5636	1417	527
5637	1417	457
5638	1417	568
5639	1417	357
5640	1418	495
5641	1418	357
5642	1418	536
5643	1418	501
5644	1419	580
5645	1419	400
5646	1419	343
5647	1419	414
5648	1420	394
5649	1420	503
5650	1420	358
5651	1420	587
5652	1421	494
5653	1421	318
5654	1421	330
5655	1421	436
5656	1422	532
5657	1422	337
5658	1422	307
5659	1422	552
5660	1423	597
5661	1423	526
5662	1423	429
5663	1423	475
5664	1424	583
5665	1424	314
5666	1424	483
5667	1424	306
5668	1425	473
5669	1425	515
5670	1425	587
5671	1425	476
5672	1426	495
5673	1426	373
5674	1426	387
5675	1426	540
5676	1427	315
5677	1427	419
5678	1427	323
5679	1427	471
5680	1428	354
5681	1428	480
5682	1428	583
5683	1428	384
5684	1429	339
5685	1429	404
5686	1429	330
5687	1429	508
5688	1430	594
5689	1430	536
5690	1430	558
5691	1430	414
5692	1431	595
5693	1431	556
5694	1431	431
5695	1431	419
5696	1432	480
5697	1432	575
5698	1432	460
5699	1432	407
5700	1433	523
5701	1433	392
5702	1433	397
5703	1433	506
5704	1434	357
5705	1434	559
5706	1434	503
5707	1434	565
5708	1435	303
5709	1435	584
5710	1435	309
5711	1435	320
5712	1436	450
5713	1436	453
5714	1436	437
5715	1436	357
5716	1437	409
5717	1437	437
5718	1437	525
5719	1437	380
5720	1438	572
5721	1438	383
5722	1438	369
5723	1438	473
5724	1439	480
5725	1439	406
5726	1439	361
5727	1439	502
5728	1440	350
5729	1440	424
5730	1440	327
5731	1440	347
5732	1441	549
5733	1441	348
5734	1441	380
5735	1441	388
5736	1442	372
5737	1442	583
5738	1442	595
5739	1442	466
5740	1443	556
5741	1443	353
5742	1443	308
5743	1443	479
5744	1444	507
5745	1444	361
5746	1444	362
5747	1444	458
5748	1445	336
5749	1445	314
5750	1445	353
5751	1445	359
5752	1446	443
5753	1446	493
5754	1446	413
5755	1446	518
5756	1447	440
5757	1447	500
5758	1447	472
5759	1447	403
5760	1448	564
5761	1448	407
5762	1448	462
5763	1448	467
5764	1449	451
5765	1449	563
5766	1449	328
5767	1449	322
5768	1450	303
5769	1450	523
5770	1450	524
5771	1450	600
5772	1451	465
5773	1451	599
5774	1451	516
5775	1451	532
5776	1452	302
5777	1452	327
5778	1452	588
5779	1453	430
5780	1453	322
5781	1453	428
5782	1454	440
5783	1454	441
5784	1454	591
5785	1454	496
5786	1455	583
5787	1455	325
5788	1455	500
5789	1455	366
5790	1456	425
5791	1456	383
5792	1456	601
5793	1456	343
5794	1457	504
5795	1457	345
5796	1457	564
5797	1457	557
5798	1458	543
5799	1458	329
5800	1458	419
5801	1458	333
5802	1459	518
5803	1459	389
5804	1459	573
5805	1459	566
5806	1460	521
5807	1460	314
5808	1460	386
5809	1460	326
5810	1461	532
5811	1461	497
5812	1461	539
5813	1461	478
5814	1462	486
5815	1462	457
5816	1462	514
5817	1462	425
5818	1463	442
5819	1463	307
5820	1463	445
5821	1463	547
5822	1464	423
5823	1464	408
5824	1464	559
5825	1464	504
5826	1465	509
5827	1465	437
5828	1465	534
5829	1465	475
5830	1466	531
5831	1466	442
5832	1466	489
5833	1466	585
5834	1467	473
5835	1467	310
5836	1467	580
5837	1467	384
5838	1468	382
5839	1468	432
5840	1468	412
5841	1468	541
5842	1469	506
5843	1469	312
5844	1469	455
5845	1469	347
5846	1470	551
5847	1470	400
5848	1470	536
5849	1470	319
5850	1471	400
5851	1471	438
5852	1471	360
5853	1471	481
5854	1472	595
5855	1472	538
5856	1472	564
5857	1472	407
5858	1473	488
5859	1473	380
5860	1473	470
5861	1473	512
5862	1474	435
5863	1474	573
5864	1474	518
5865	1474	482
5866	1475	433
5867	1475	479
5868	1475	526
5869	1475	427
5870	1476	512
5871	1476	478
5872	1476	381
5873	1476	561
5874	1477	362
5875	1477	313
5876	1477	506
5877	1477	329
5878	1478	524
5879	1478	439
5880	1478	504
5881	1478	335
5882	1479	321
5883	1479	394
5884	1479	328
5885	1479	410
5886	1480	432
5887	1480	570
5888	1480	352
5889	1480	305
5890	1481	584
5891	1481	411
5892	1481	516
5893	1481	355
5894	1482	589
5895	1482	426
5896	1482	348
5897	1482	583
5898	1483	377
5899	1483	461
5900	1483	456
5901	1483	375
5902	1484	541
5903	1484	308
5904	1484	411
5905	1484	406
5906	1485	472
5907	1485	559
5908	1485	500
5909	1486	554
5910	1486	556
5911	1486	403
5912	1486	470
5913	1487	320
5914	1487	360
5915	1487	327
5916	1487	509
5917	1488	362
5918	1488	598
5919	1488	444
5920	1488	413
5921	1489	578
5922	1489	340
5923	1489	360
5924	1489	595
5925	1490	395
5926	1490	436
5927	1490	413
5928	1490	364
5929	1491	415
5930	1491	385
5931	1491	475
5932	1491	434
5933	1492	564
5934	1492	471
5935	1492	536
5936	1492	483
5937	1493	373
5938	1493	358
5939	1493	594
5940	1493	374
5941	1494	474
5942	1494	579
5943	1494	542
5944	1494	375
5945	1495	471
5946	1495	394
5947	1495	329
5948	1495	591
5949	1496	443
5950	1496	517
5951	1496	532
5952	1496	315
5953	1497	317
5954	1497	346
5955	1497	539
5956	1497	482
5957	1498	345
5958	1498	557
5959	1498	443
5960	1498	446
5961	1499	446
5962	1499	306
5963	1499	368
5964	1499	554
5965	1500	431
5966	1500	383
5967	1500	516
5968	1500	368
5969	1501	551
5970	1501	349
5971	1501	342
5972	1501	581
5973	1502	521
5974	1502	468
5975	1502	511
5976	1502	596
5977	1503	515
5978	1503	374
5979	1503	475
5980	1503	602
5981	1504	396
5982	1504	539
5983	1504	487
5984	1504	329
5985	1505	364
5986	1505	517
5987	1505	303
5988	1505	560
5989	1506	601
5990	1506	538
5991	1506	555
5992	1506	564
5993	1507	334
5994	1507	497
5995	1507	432
5996	1507	494
5997	1508	518
5998	1508	399
5999	1508	363
6000	1508	505
6001	1509	561
6002	1509	523
6003	1509	569
6004	1509	551
6005	1510	599
6006	1510	556
6007	1510	410
6008	1510	439
6009	1511	534
6010	1511	507
6011	1511	335
6012	1512	496
6013	1512	481
6014	1512	516
6015	1512	333
6016	1513	397
6017	1513	429
6018	1513	539
6019	1513	590
6020	1514	514
6021	1514	420
6022	1514	408
6023	1514	430
6024	1515	480
6025	1515	368
6026	1515	342
6027	1515	363
6028	1516	350
6029	1516	524
6030	1516	490
6031	1516	502
6032	1517	339
6033	1517	561
6034	1517	532
6035	1517	486
6036	1518	304
6037	1518	412
6038	1518	593
6039	1518	404
6040	1519	324
6041	1519	586
6042	1519	510
6043	1519	326
6044	1520	312
6045	1520	577
6046	1520	409
6047	1520	380
6048	1521	598
6049	1521	321
6050	1521	397
6051	1521	536
6052	1522	522
6053	1522	544
6054	1522	415
6055	1522	355
6056	1523	422
6057	1523	523
6058	1523	435
6059	1523	567
6060	1524	543
6061	1524	357
6062	1524	303
6063	1524	532
6064	1525	401
6065	1525	554
6066	1525	306
6067	1525	582
6068	1526	472
6069	1526	527
6070	1526	385
6071	1526	421
6072	1527	563
6073	1527	571
6074	1527	535
6075	1527	582
6076	1528	549
6077	1528	326
6078	1528	521
6079	1528	542
6080	1529	332
6081	1529	331
6082	1529	367
6083	1529	433
6084	1530	361
6085	1530	334
6086	1530	562
6087	1530	550
6088	1531	480
6089	1531	568
6090	1531	404
6091	1531	339
6092	1532	439
6093	1532	553
6094	1532	587
6095	1532	451
6096	1533	577
6097	1533	381
6098	1533	440
6099	1533	421
6100	1534	338
6101	1534	486
6102	1534	597
6103	1534	593
6104	1535	351
6105	1535	494
6106	1535	578
6107	1535	499
6108	1536	481
6109	1536	398
6110	1536	544
6111	1536	332
6112	1537	565
6113	1537	566
6114	1537	524
6115	1537	314
6116	1538	380
6117	1538	388
6118	1538	393
6119	1538	387
6120	1539	528
6121	1539	442
6122	1539	393
6123	1539	329
6124	1540	513
6125	1540	305
6126	1540	545
6127	1540	480
6128	1541	419
6129	1541	344
6130	1541	416
6131	1541	516
6132	1542	466
6133	1542	591
6134	1542	536
6135	1542	335
6136	1543	562
6137	1543	362
6138	1543	548
6139	1543	421
6140	1544	488
6141	1544	476
6142	1544	507
6143	1544	363
6144	1545	401
6145	1545	520
6146	1545	466
6147	1545	350
6148	1546	594
6149	1546	429
6150	1546	360
6151	1546	352
6152	1547	419
6153	1547	315
6154	1547	493
6155	1547	583
6156	1548	326
6157	1548	403
6158	1548	585
6159	1548	399
6160	1549	440
6161	1549	486
6162	1549	356
6163	1549	325
6164	1550	363
6165	1550	479
6166	1550	581
6167	1550	570
6168	1551	598
6169	1551	584
6170	1551	450
6171	1551	423
6172	1552	559
6173	1552	546
6174	1552	334
6175	1552	423
6176	1553	442
6177	1553	392
6178	1553	374
6179	1553	361
6180	1554	430
6181	1554	595
6182	1554	321
6183	1554	318
6184	1555	583
6185	1555	560
6186	1555	530
6187	1555	420
6188	1556	487
6189	1556	555
6190	1556	371
6191	1556	573
6192	1557	491
6193	1557	395
6194	1557	451
6195	1557	435
6196	1558	411
6197	1558	418
6198	1558	537
6199	1558	526
6200	1559	511
6201	1559	452
6202	1559	421
6203	1559	567
6204	1560	494
6205	1560	518
6206	1560	562
6207	1560	404
6208	1561	313
6209	1561	359
6210	1561	456
6211	1561	573
6212	1562	549
6213	1562	499
6214	1562	466
6215	1562	396
6216	1563	499
6217	1563	591
6218	1563	491
6219	1563	337
6220	1564	576
6221	1564	449
6222	1564	504
6223	1564	468
6224	1565	563
6225	1565	397
6226	1565	464
6227	1565	429
6228	1566	382
6229	1566	583
6230	1566	432
6231	1566	430
6232	1567	311
6233	1567	373
6234	1567	599
6235	1567	455
6236	1568	378
6237	1568	469
6238	1568	365
6239	1568	321
6240	1569	558
6241	1569	573
6242	1569	389
6243	1569	310
6244	1570	570
6245	1570	334
6246	1570	594
6247	1570	355
6248	1571	520
6249	1571	549
6250	1571	526
6251	1571	522
6252	1572	308
6253	1572	581
6254	1572	484
6255	1572	323
6256	1573	550
6257	1573	334
6258	1573	471
6259	1573	465
6260	1574	310
6261	1574	505
6262	1574	519
6263	1574	426
6264	1575	509
6265	1575	426
6266	1575	423
6267	1575	484
6268	1576	401
6269	1576	537
6270	1576	357
6271	1576	462
6272	1577	429
6273	1577	442
6274	1577	338
6275	1577	451
6276	1578	310
6277	1578	449
6278	1578	318
6279	1578	543
6280	1579	356
6281	1579	319
6282	1579	534
6283	1579	443
6284	1580	454
6285	1580	409
6286	1580	333
6287	1580	428
6288	1581	455
6289	1581	305
6290	1581	434
6291	1581	340
6292	1582	410
6293	1582	412
6294	1582	437
6295	1582	536
6296	1583	597
6297	1583	361
6298	1583	484
6299	1583	399
6300	1584	304
6301	1584	305
6302	1584	555
6303	1584	360
6304	1585	350
6305	1585	446
6306	1585	477
6307	1585	311
6308	1586	339
6309	1586	517
6310	1586	409
6311	1586	303
6312	1587	454
6313	1587	347
6314	1587	497
6315	1587	394
6316	1588	599
6317	1588	598
6318	1588	315
6319	1588	519
6320	1589	415
6321	1589	432
6322	1589	400
6323	1589	592
6324	1590	320
6325	1590	328
6326	1590	440
6327	1590	514
6328	1591	602
6329	1591	390
6330	1591	582
6331	1591	522
6332	1592	519
6333	1592	303
6334	1592	590
6335	1592	305
6336	1593	491
6337	1593	497
6338	1593	507
6339	1593	580
6340	1594	572
6341	1594	568
6342	1594	343
6343	1594	420
6344	1595	385
6345	1595	311
6346	1595	389
6347	1595	425
6348	1596	403
6349	1596	571
6350	1596	434
6351	1596	370
6352	1597	315
6353	1597	433
6354	1597	401
6355	1597	528
6356	1598	425
6357	1598	308
6358	1598	358
6359	1598	414
6360	1599	381
6361	1599	438
6362	1599	424
6363	1599	340
6364	1600	528
6365	1600	370
6366	1600	424
6367	1600	416
6368	1601	492
6369	1601	471
6370	1601	341
6371	1601	418
6372	1602	399
6373	1602	431
6374	1602	375
6375	1602	384
6376	1603	371
6377	1603	440
6378	1603	330
6379	1603	523
6380	1604	333
6381	1604	316
6382	1604	550
6383	1604	539
6384	1605	555
6385	1605	539
6386	1605	337
6387	1605	544
6388	1606	399
6389	1606	545
6390	1606	590
6391	1606	376
6392	1607	395
6393	1607	392
6394	1607	513
6395	1608	574
6396	1608	596
6397	1608	360
6398	1608	362
6399	1609	428
6400	1609	323
6401	1609	430
6402	1609	506
6403	1610	456
6404	1610	578
6405	1610	425
6406	1610	309
6407	1611	526
6408	1611	418
6409	1611	353
6410	1611	485
6411	1612	471
6412	1612	312
6413	1612	593
6414	1612	363
6415	1613	401
6416	1613	477
6417	1613	569
6418	1613	522
6419	1614	454
6420	1614	567
6421	1614	528
6422	1614	428
6423	1615	466
6424	1615	458
6425	1615	501
6426	1615	476
6427	1616	341
6428	1616	486
6429	1616	455
6430	1616	586
6431	1617	435
6432	1617	483
6433	1617	551
6434	1617	533
6435	1618	476
6436	1618	408
6437	1618	488
6438	1618	310
6439	1619	470
6440	1619	476
6441	1619	574
6442	1619	397
6443	1620	396
6444	1620	588
6445	1620	571
6446	1620	339
6447	1621	356
6448	1621	414
6449	1621	336
6450	1621	304
6451	1622	495
6452	1622	369
6453	1622	439
6454	1622	379
6455	1623	328
6456	1623	338
6457	1623	521
6458	1623	394
6459	1624	414
6460	1624	322
6461	1624	420
6462	1624	575
6463	1625	351
6464	1625	383
6465	1625	584
6466	1625	353
6467	1626	558
6468	1626	397
6469	1626	471
6470	1626	382
6471	1627	552
6472	1627	596
6473	1627	494
6474	1627	443
6475	1628	550
6476	1628	386
6477	1628	526
6478	1628	506
6479	1629	354
6480	1629	535
6481	1629	461
6482	1629	487
6483	1630	527
6484	1630	400
6485	1630	375
6486	1630	382
6487	1631	321
6488	1631	513
6489	1631	596
6490	1631	480
6491	1632	461
6492	1632	454
6493	1632	464
6494	1632	500
6495	1633	499
6496	1633	306
6497	1633	434
6498	1633	431
6499	1634	473
6500	1634	551
6501	1634	415
6502	1634	446
6503	1635	373
6504	1635	434
6505	1635	507
6506	1635	447
6507	1636	368
6508	1636	543
6509	1636	440
6510	1636	464
6511	1637	432
6512	1637	411
6513	1637	460
6514	1637	581
6515	1638	362
6516	1638	406
6517	1638	344
6518	1638	462
6519	1639	478
6520	1639	523
6521	1639	399
6522	1639	458
6523	1640	583
6524	1640	488
6525	1640	397
6526	1640	466
6527	1641	539
6528	1641	595
6529	1641	390
6530	1641	344
6531	1642	478
6532	1642	530
6533	1642	471
6534	1642	557
6535	1643	546
6536	1643	393
6537	1643	390
6538	1643	468
6539	1644	503
6540	1644	398
6541	1644	458
6542	1644	332
6543	1645	577
6544	1645	580
6545	1645	599
6546	1645	414
6547	1646	517
6548	1646	339
6549	1646	316
6550	1646	469
6551	1647	500
6552	1647	430
6553	1647	485
6554	1647	526
6555	1648	473
6556	1648	439
6557	1648	416
6558	1648	311
6559	1649	390
6560	1649	436
6561	1649	401
6562	1649	503
6563	1650	455
6564	1650	594
6565	1650	338
6566	1650	490
6567	1651	495
6568	1651	442
6569	1651	532
6570	1651	593
6571	1652	488
6572	1652	342
6573	1652	424
6574	1652	472
6575	1653	596
6576	1653	320
6577	1653	578
6578	1653	526
6579	1654	449
6580	1654	525
6581	1654	359
6582	1654	556
6583	1655	348
6584	1655	571
6585	1655	496
6586	1655	517
6587	1656	434
6588	1656	312
6589	1656	321
6590	1656	433
6591	1657	326
6592	1657	391
6593	1657	376
6594	1657	520
6595	1658	540
6596	1658	567
6597	1658	323
6598	1658	533
6599	1659	479
6600	1659	530
6601	1659	323
6602	1659	395
6603	1660	417
6604	1660	404
6605	1660	547
6606	1660	421
6607	1661	565
6608	1661	416
6609	1661	324
6610	1661	489
6611	1662	484
6612	1662	420
6613	1662	475
6614	1662	457
6615	1663	596
6616	1663	578
6617	1663	598
6618	1663	478
6619	1664	531
6620	1664	333
6621	1664	315
6622	1664	312
6623	1665	509
6624	1665	498
6625	1665	451
6626	1665	476
6627	1666	562
6628	1666	403
6629	1666	530
6630	1666	559
6631	1667	456
6632	1667	338
6633	1667	527
6634	1667	399
6635	1668	311
6636	1668	568
6637	1668	310
6638	1668	554
6639	1669	538
6640	1669	516
6641	1669	534
6642	1669	358
6643	1670	528
6644	1670	453
6645	1670	591
6646	1670	393
6647	1671	357
6648	1671	589
6649	1671	328
6650	1671	402
6651	1672	476
6652	1672	545
6653	1672	480
6654	1672	563
6655	1673	430
6656	1673	588
6657	1673	534
6658	1673	495
6659	1674	463
6660	1674	587
6661	1674	569
6662	1674	387
6663	1675	493
6664	1675	592
6665	1675	410
6666	1675	526
6667	1676	406
6668	1676	598
6669	1676	440
6670	1676	457
6671	1677	592
6672	1677	311
6673	1677	444
6674	1677	308
6675	1678	419
6676	1678	571
6677	1678	371
6678	1678	494
6679	1679	585
6680	1679	502
6681	1679	564
6682	1679	558
6683	1680	582
6684	1680	440
6685	1680	374
6686	1681	531
6687	1681	451
6688	1681	578
6689	1681	330
6690	1682	461
6691	1682	325
6692	1682	331
6693	1682	478
6694	1683	440
6695	1683	571
6696	1683	363
6697	1683	444
6698	1684	305
6699	1684	334
6700	1684	518
6701	1684	590
6702	1685	493
6703	1685	360
6704	1685	524
6705	1685	526
6706	1686	395
6707	1686	557
6708	1686	547
6709	1686	374
6710	1687	534
6711	1687	575
6712	1687	599
6713	1687	586
6714	1688	429
6715	1688	518
6716	1688	482
6717	1689	487
6718	1689	307
6719	1689	358
6720	1689	393
6721	1690	313
6722	1690	491
6723	1690	480
6724	1690	517
6725	1691	593
6726	1691	546
6727	1691	435
6728	1691	521
6729	1692	514
6730	1692	327
6731	1692	547
6732	1692	588
6733	1693	417
6734	1693	508
6735	1693	600
6736	1693	443
6737	1694	455
6738	1694	473
6739	1694	534
6740	1694	517
6741	1695	403
6742	1695	321
6743	1695	467
6744	1695	418
6745	1696	319
6746	1696	499
6747	1696	588
6748	1696	569
6749	1697	468
6750	1697	336
6751	1697	369
6752	1697	309
6753	1698	500
6754	1698	402
6755	1698	324
6756	1698	549
6757	1699	315
6758	1699	407
6759	1699	573
6760	1699	504
6761	1700	466
6762	1700	316
6763	1700	558
6764	1700	586
6765	1701	557
6766	1701	332
6767	1701	511
6768	1701	520
6769	1702	523
6770	1702	581
6771	1702	328
6772	1702	585
6773	1703	312
6774	1703	580
6775	1703	588
6776	1703	325
6777	1704	422
6778	1704	466
6779	1704	399
6780	1704	434
6781	1705	546
6782	1705	441
6783	1705	454
6784	1706	347
6785	1706	478
6786	1706	392
6787	1706	457
6788	1707	395
6789	1707	565
6790	1707	580
6791	1707	329
6792	1708	502
6793	1708	559
6794	1708	508
6795	1708	477
6796	1709	392
6797	1709	586
6798	1709	510
6799	1709	537
6800	1710	505
6801	1710	586
6802	1710	473
6803	1710	478
6804	1711	559
6805	1711	556
6806	1711	403
6807	1711	467
6808	1712	486
6809	1712	356
6810	1712	455
6811	1712	448
6812	1713	391
6813	1713	520
6814	1713	421
6815	1713	376
6816	1714	430
6817	1714	507
6818	1714	423
6819	1714	581
6820	1715	413
6821	1715	380
6822	1715	410
6823	1715	481
6824	1716	590
6825	1716	549
6826	1716	338
6827	1716	470
6828	1717	518
6829	1717	469
6830	1717	460
6831	1717	530
6832	1718	557
6833	1718	314
6834	1718	549
6835	1718	387
6836	1719	587
6837	1719	448
6838	1719	342
6839	1719	558
6840	1720	478
6841	1720	310
6842	1720	579
6843	1720	319
6844	1721	354
6845	1721	391
6846	1721	426
6847	1721	373
6848	1722	473
6849	1722	308
6850	1722	443
6851	1722	359
6852	1723	521
6853	1723	466
6854	1723	441
6855	1723	474
6856	1724	552
6857	1724	450
6858	1724	600
6859	1724	574
6860	1725	438
6861	1725	370
6862	1725	574
6863	1726	344
6864	1726	473
6865	1726	364
6866	1726	545
6867	1727	522
6868	1727	486
6869	1727	524
6870	1727	313
6871	1728	349
6872	1728	544
6873	1728	440
6874	1728	438
6875	1729	379
6876	1729	381
6877	1729	458
6878	1729	308
6879	1730	362
6880	1730	329
6881	1730	327
6882	1730	536
6883	1731	415
6884	1731	374
6885	1731	428
6886	1731	361
6887	1732	590
6888	1732	378
6889	1732	545
6890	1732	587
6891	1733	441
6892	1733	571
6893	1733	417
6894	1733	457
6895	1734	366
6896	1734	464
6897	1734	531
6898	1734	389
6899	1735	313
6900	1735	390
6901	1735	423
6902	1735	397
6903	1736	412
6904	1736	529
6905	1736	408
6906	1736	315
6907	1737	320
6908	1737	562
6909	1737	573
6910	1737	302
6911	1738	590
6912	1738	477
6913	1738	573
6914	1738	351
6915	1739	455
6916	1739	485
6917	1739	336
6918	1739	458
6919	1740	437
6920	1740	360
6921	1740	469
6922	1740	570
6923	1741	337
6924	1741	527
6925	1741	471
6926	1741	450
6927	1742	593
6928	1742	463
6929	1742	357
6930	1742	592
6931	1743	541
6932	1743	316
6933	1743	533
6934	1743	531
6935	1744	402
6936	1744	595
6937	1744	582
6938	1745	386
6939	1745	459
6940	1745	309
6941	1745	357
6942	1746	456
6943	1746	577
6944	1746	432
6945	1746	529
6946	1747	459
6947	1747	397
6948	1747	602
6949	1747	345
6950	1748	448
6951	1748	337
6952	1748	375
6953	1748	399
6954	1749	493
6955	1749	544
6956	1749	510
6957	1749	541
6958	1750	482
6959	1750	556
6960	1750	367
6961	1750	303
6962	1751	512
6963	1751	585
6964	1751	318
6965	1751	467
6966	1752	310
6967	1752	490
6968	1752	320
6969	1752	374
6970	1753	509
6971	1753	349
6972	1753	546
6973	1753	549
6974	1754	367
6975	1754	318
6976	1754	554
6977	1754	336
6978	1755	546
6979	1755	344
6980	1755	330
6981	1755	522
6982	1756	398
6983	1756	331
6984	1756	539
6985	1756	529
6986	1757	490
6987	1757	451
6988	1757	549
6989	1757	429
6990	1758	434
6991	1758	323
6992	1758	302
6993	1758	363
6994	1759	353
6995	1759	440
6996	1759	471
6997	1759	328
6998	1760	597
6999	1760	438
7000	1760	532
7001	1760	549
7002	1761	570
7003	1761	332
7004	1761	409
7005	1761	330
7006	1762	445
7007	1762	354
7008	1762	415
7009	1762	411
7010	1763	452
7011	1763	515
7012	1763	339
7013	1763	559
7014	1764	337
7015	1764	524
7016	1764	582
7017	1764	502
7018	1765	418
7019	1765	501
7020	1765	485
7021	1765	533
7022	1766	545
7023	1766	458
7024	1766	425
7025	1766	338
7026	1767	409
7027	1767	450
7028	1767	484
7029	1767	518
7030	1768	350
7031	1768	412
7032	1768	514
7033	1768	358
7034	1769	361
7035	1769	487
7036	1769	356
7037	1769	547
7038	1770	322
7039	1770	545
7040	1770	514
7041	1770	477
7042	1771	487
7043	1771	570
7044	1771	551
7045	1771	395
7046	1772	381
7047	1772	319
7048	1772	412
7049	1772	307
7050	1773	369
7051	1773	497
7052	1773	351
7053	1773	484
7054	1774	399
7055	1774	489
7056	1774	572
7057	1774	585
7058	1775	588
7059	1775	325
7060	1775	459
7061	1775	402
7062	1776	347
7063	1776	462
7064	1776	414
7065	1776	576
7066	1777	514
7067	1777	340
7068	1777	309
7069	1777	325
7070	1778	338
7071	1778	501
7072	1778	420
7073	1778	471
7074	1779	412
7075	1779	596
7076	1779	438
7077	1779	600
7078	1780	455
7079	1780	316
7080	1780	418
7081	1780	403
7082	1781	479
7083	1781	313
7084	1781	325
7085	1781	471
7086	1782	350
7087	1782	320
7088	1782	408
7089	1782	551
7090	1783	307
7091	1783	429
7092	1783	357
7093	1783	364
7094	1784	408
7095	1784	397
7096	1784	524
7097	1784	307
7098	1785	574
7099	1785	358
7100	1785	426
7101	1785	567
7102	1786	351
7103	1786	535
7104	1786	463
7105	1786	476
7106	1787	430
7107	1787	356
7108	1787	476
7109	1787	514
7110	1788	309
7111	1788	569
7112	1788	422
7113	1788	386
7114	1789	590
7115	1789	539
7116	1789	423
7117	1789	342
7118	1790	369
7119	1790	500
7120	1790	395
7121	1790	591
7122	1791	589
7123	1791	363
7124	1791	593
7125	1791	421
7126	1792	457
7127	1792	414
7128	1792	350
7129	1792	381
7130	1793	379
7131	1793	449
7132	1793	433
7133	1793	496
7134	1794	363
7135	1794	556
7136	1794	325
7137	1794	505
7138	1795	433
7139	1795	491
7140	1795	385
7141	1795	530
7142	1796	396
7143	1796	416
7144	1796	502
7145	1796	432
7146	1797	429
7147	1797	586
7148	1797	490
7149	1797	540
7150	1798	559
7151	1798	505
7152	1798	599
7153	1798	315
7154	1799	490
7155	1799	470
7156	1799	338
7157	1799	419
7158	1800	355
7159	1800	465
7160	1800	575
7161	1800	430
7162	1801	386
7163	1801	514
7164	1801	375
7165	1801	359
7166	1802	310
7167	1802	418
7168	1802	473
7169	1802	331
7170	1803	566
7171	1803	343
7172	1803	590
7173	1803	328
7174	1804	495
7175	1804	358
7176	1804	442
7177	1804	534
7178	1805	488
7179	1805	538
7180	1805	376
7181	1805	527
7182	1806	566
7183	1806	334
7184	1806	424
7185	1806	316
7186	1807	389
7187	1807	586
7188	1807	305
7189	1807	550
7190	1808	324
7191	1808	469
7192	1808	595
7193	1808	420
7194	1809	423
7195	1809	550
7196	1809	340
7197	1809	519
7198	1810	421
7199	1810	348
7200	1810	520
7201	1810	569
7202	1811	337
7203	1811	309
7204	1811	505
7205	1811	585
7206	1812	528
7207	1812	359
7208	1812	374
7209	1812	377
7210	1813	357
7211	1813	542
7212	1813	492
7213	1813	307
7214	1814	439
7215	1814	464
7216	1814	396
7217	1814	337
7218	1815	378
7219	1815	403
7220	1815	398
7221	1815	365
7222	1816	432
7223	1816	427
7224	1816	404
7225	1816	512
7226	1817	539
7227	1817	599
7228	1817	470
7229	1817	382
7230	1818	470
7231	1818	538
7232	1818	468
7233	1818	393
7234	1819	464
7235	1819	304
7236	1819	379
7237	1819	585
7238	1820	364
7239	1820	483
7240	1820	595
7241	1820	581
7242	1821	423
7243	1821	500
7244	1821	376
7245	1821	543
7246	1822	376
7247	1822	402
7248	1822	408
7249	1822	443
7250	1823	564
7251	1823	598
7252	1823	463
7253	1823	516
7254	1824	502
7255	1824	401
7256	1824	370
7257	1824	382
7258	1825	393
7259	1825	429
7260	1825	443
7261	1825	511
7262	1826	326
7263	1826	436
7264	1826	361
7265	1826	450
7266	1827	533
7267	1827	415
7268	1827	429
7269	1827	527
7270	1828	368
7271	1828	410
7272	1828	302
7273	1828	475
7274	1829	487
7275	1829	496
7276	1829	569
7277	1829	584
7278	1830	377
7279	1830	447
7280	1830	486
7281	1830	450
7282	1831	536
7283	1831	320
7284	1831	316
7285	1831	494
7286	1832	365
7287	1832	374
7288	1832	321
7289	1832	591
7290	1833	470
7291	1833	515
7292	1833	369
7293	1833	584
7294	1834	596
7295	1834	482
7296	1834	377
7297	1834	324
7298	1835	592
7299	1835	513
7300	1835	397
7301	1835	396
7302	1836	441
7303	1836	303
7304	1836	384
7305	1836	438
7306	1837	420
7307	1837	327
7308	1837	355
7309	1837	443
7310	1838	523
7311	1838	573
7312	1838	495
7313	1838	342
7314	1839	346
7315	1839	343
7316	1839	325
7317	1839	320
7318	1840	416
7319	1840	598
7320	1840	445
7321	1840	487
7322	1841	392
7323	1841	311
7324	1841	382
7325	1841	308
7326	1842	421
7327	1842	359
7328	1842	459
7329	1842	475
7330	1843	458
7331	1843	384
7332	1843	538
7333	1843	535
7334	1844	505
7335	1844	409
7336	1844	597
7337	1844	507
7338	1845	602
7339	1845	510
7340	1845	366
7341	1845	559
7342	1846	429
7343	1846	368
7344	1846	475
7345	1846	506
7346	1847	535
7347	1847	498
7348	1847	492
7349	1847	338
7350	1848	586
7351	1848	430
7352	1848	350
7353	1848	556
7354	1849	414
7355	1849	369
7356	1849	531
7357	1849	549
7358	1850	382
7359	1850	391
7360	1850	414
7361	1850	543
7362	1851	415
7363	1851	562
7364	1851	402
7365	1851	398
7366	1852	374
7367	1852	395
7368	1852	346
7369	1852	441
7370	1853	512
7371	1853	537
7372	1853	501
7373	1853	563
7374	1854	390
7375	1854	487
7376	1854	441
7377	1854	498
7378	1855	551
7379	1855	420
7380	1855	351
7381	1855	599
7382	1856	541
7383	1856	550
7384	1856	505
7385	1856	476
7386	1857	602
7387	1857	502
7388	1857	346
7389	1857	317
7390	1858	530
7391	1858	578
7392	1858	536
7393	1858	434
7394	1859	502
7395	1859	330
7396	1859	504
7397	1859	594
7398	1860	581
7399	1860	338
7400	1860	392
7401	1860	455
7402	1861	483
7403	1861	503
7404	1861	518
7405	1861	570
7406	1862	544
7407	1862	396
7408	1862	552
7409	1862	366
7410	1863	511
7411	1863	594
7412	1863	592
7413	1863	338
7414	1864	471
7415	1864	489
7416	1864	562
7417	1864	574
7418	1865	309
7419	1865	572
7420	1865	516
7421	1865	550
7422	1866	374
7423	1866	410
7424	1866	322
7425	1866	470
7426	1867	542
7427	1867	536
7428	1867	489
7429	1867	324
7430	1868	395
7431	1868	553
7432	1868	586
7433	1868	552
7434	1869	318
7435	1869	537
7436	1869	390
7437	1869	303
7438	1870	311
7439	1870	555
7440	1870	573
7441	1870	362
7442	1871	395
7443	1871	432
7444	1871	385
7445	1871	414
7446	1872	509
7447	1872	421
7448	1872	352
7449	1872	456
7450	1873	566
7451	1873	469
7452	1873	323
7453	1873	455
7454	1874	357
7455	1874	443
7456	1874	309
7457	1874	541
7458	1875	500
7459	1875	527
7460	1875	347
7461	1875	311
7462	1876	349
7463	1876	546
7464	1876	425
7465	1876	352
7466	1877	522
7467	1877	572
7468	1877	439
7469	1877	422
7470	1878	401
7471	1878	397
7472	1878	486
7473	1878	495
7474	1879	602
7475	1879	446
7476	1879	347
7477	1879	345
7478	1880	589
7479	1880	582
7480	1880	372
7481	1880	318
7482	1881	319
7483	1881	470
7484	1881	483
7485	1881	475
7486	1882	369
7487	1882	317
7488	1882	570
7489	1882	455
7490	1883	506
7491	1883	417
7492	1883	533
7493	1883	438
7494	1884	462
7495	1884	401
7496	1884	537
7497	1884	472
7498	1885	342
7499	1885	374
7500	1885	309
7501	1885	568
7502	1886	404
7503	1886	531
7504	1886	315
7505	1886	536
7506	1887	332
7507	1887	483
7508	1887	368
7509	1887	600
7510	1888	455
7511	1888	390
7512	1888	302
7513	1888	578
7514	1889	464
7515	1889	505
7516	1889	337
7517	1889	599
7518	1890	418
7519	1890	523
7520	1890	559
7521	1890	339
7522	1891	558
7523	1891	594
7524	1891	430
7525	1891	577
7526	1892	476
7527	1892	558
7528	1892	376
7529	1892	514
7530	1893	421
7531	1893	554
7532	1893	339
7533	1893	559
7534	1894	514
7535	1894	397
7536	1894	373
7537	1894	499
7538	1895	353
7539	1895	366
7540	1895	480
7541	1895	521
7542	1896	324
7543	1896	372
7544	1896	388
7545	1896	445
7546	1897	351
7547	1897	345
7548	1897	370
7549	1897	499
7550	1898	547
7551	1898	434
7552	1898	330
7553	1898	500
7554	1899	483
7555	1899	369
7556	1899	550
7557	1899	588
7558	1900	585
7559	1900	316
7560	1900	497
7561	1900	385
7562	1901	323
7563	1901	437
7564	1901	416
7565	1901	436
7566	1902	483
7567	1902	352
7568	1902	316
7569	1902	354
7570	1903	377
7571	1903	419
7572	1903	412
7573	1903	502
7574	1904	514
7575	1904	577
7576	1904	534
7577	1904	523
7578	1905	456
7579	1905	479
7580	1905	319
7581	1905	478
7582	1906	404
7583	1906	490
7584	1906	599
7585	1906	546
7586	1907	375
7587	1907	494
7588	1907	355
7589	1907	411
7590	1908	469
7591	1908	542
7592	1908	389
7593	1909	533
7594	1909	573
7595	1909	583
7596	1910	409
7597	1910	566
7598	1910	322
7599	1910	583
7600	1911	308
7601	1911	398
7602	1911	426
7603	1911	550
7604	1912	305
7605	1912	599
7606	1912	539
7607	1912	594
7608	1913	308
7609	1913	442
7610	1913	591
7611	1913	511
7612	1914	559
7613	1914	527
7614	1914	417
7615	1914	505
7616	1915	432
7617	1915	599
7618	1915	308
7619	1915	502
7620	1916	348
7621	1916	473
7622	1916	565
7623	1916	400
7624	1917	384
7625	1917	415
7626	1917	447
7627	1917	394
7628	1918	550
7629	1918	533
7630	1918	448
7631	1918	455
7632	1919	372
7633	1919	319
7634	1919	546
7635	1919	342
7636	1920	386
7637	1920	568
7638	1920	440
7639	1920	567
7640	1921	388
7641	1921	503
7642	1921	430
7643	1921	341
7644	1922	490
7645	1922	555
7646	1922	471
7647	1922	386
7648	1923	340
7649	1923	563
7650	1923	595
7651	1923	548
7652	1924	426
7653	1924	579
7654	1924	575
7655	1924	396
7656	1925	393
7657	1925	583
7658	1925	344
7659	1925	428
7660	1926	303
7661	1926	306
7662	1926	483
7663	1926	557
7664	1927	435
7665	1927	408
7666	1927	466
7667	1927	599
7668	1928	509
7669	1928	547
7670	1928	493
7671	1929	455
7672	1929	488
7673	1929	556
7674	1929	317
7675	1930	580
7676	1930	504
7677	1930	574
7678	1930	432
7679	1931	428
7680	1931	370
7681	1931	381
7682	1931	596
7683	1932	549
7684	1932	394
7685	1932	363
7686	1932	439
7687	1933	598
7688	1933	549
7689	1933	534
7690	1933	536
7691	1934	338
7692	1934	500
7693	1934	594
7694	1934	567
7695	1935	426
7696	1935	498
7697	1935	373
7698	1935	518
7699	1936	457
7700	1936	403
7701	1936	410
7702	1936	467
7703	1937	548
7704	1937	599
7705	1937	597
7706	1937	564
7707	1938	490
7708	1938	552
7709	1938	601
7710	1938	536
7711	1939	536
7712	1939	543
7713	1939	422
7714	1939	324
7715	1940	358
7716	1940	324
7717	1940	387
7718	1940	303
7719	1941	485
7720	1941	533
7721	1941	583
7722	1941	453
7723	1942	337
7724	1942	580
7725	1942	398
7726	1942	450
7727	1943	527
7728	1943	570
7729	1943	521
7730	1943	409
7731	1944	439
7732	1944	335
7733	1944	522
7734	1944	324
7735	1945	577
7736	1945	536
7737	1945	594
7738	1945	458
7739	1946	304
7740	1946	367
7741	1946	340
7742	1946	464
7743	1947	428
7744	1947	601
7745	1947	556
7746	1947	334
7747	1948	343
7748	1948	464
7749	1948	333
7750	1948	350
7751	1949	389
7752	1949	359
7753	1949	403
7754	1949	600
7755	1950	429
7756	1950	444
7757	1950	374
7758	1950	428
7759	1951	428
7760	1951	526
7761	1951	551
7762	1951	397
7763	1952	386
7764	1952	385
7765	1952	357
7766	1952	560
7767	1953	523
7768	1953	326
7769	1953	586
7770	1953	498
7771	1954	410
7772	1954	357
7773	1954	430
7774	1954	482
7775	1955	326
7776	1955	469
7777	1955	367
7778	1955	525
7779	1956	580
7780	1956	493
7781	1956	457
7782	1956	303
7783	1957	513
7784	1957	325
7785	1957	402
7786	1957	473
7787	1958	404
7788	1958	592
7789	1958	561
7790	1958	321
7791	1959	568
7792	1959	515
7793	1959	425
7794	1959	454
7795	1960	363
7796	1960	395
7797	1960	428
7798	1960	446
7799	1961	331
7800	1961	351
7801	1961	461
7802	1961	310
7803	1962	307
7804	1962	327
7805	1962	600
7806	1962	411
7807	1963	500
7808	1963	466
7809	1963	316
7810	1963	326
7811	1964	596
7812	1964	473
7813	1964	475
7814	1964	394
7815	1965	594
7816	1965	307
7817	1965	409
7818	1965	383
7819	1966	422
7820	1966	537
7821	1966	316
7822	1966	530
7823	1967	557
7824	1967	390
7825	1967	364
7826	1967	438
7827	1968	322
7828	1968	365
7829	1968	512
7830	1968	373
7831	1969	417
7832	1969	595
7833	1969	376
7834	1969	531
7835	1970	349
7836	1970	416
7837	1970	361
7838	1970	459
7839	1971	474
7840	1971	439
7841	1971	423
7842	1971	499
7843	1972	448
7844	1972	539
7845	1972	584
7846	1972	464
7847	1973	373
7848	1973	529
7849	1973	379
7850	1973	354
7851	1974	471
7852	1974	435
7853	1974	530
7854	1974	475
7855	1975	362
7856	1975	584
7857	1975	349
7858	1975	420
7859	1976	420
7860	1976	500
7861	1976	417
7862	1976	376
7863	1977	583
7864	1977	339
7865	1977	532
7866	1978	475
7867	1978	574
7868	1978	578
7869	1978	411
7870	1979	319
7871	1979	370
7872	1979	547
7873	1979	576
7874	1980	418
7875	1980	498
7876	1980	349
7877	1980	466
7878	1981	520
7879	1981	367
7880	1981	548
7881	1981	471
7882	1982	482
7883	1982	452
7884	1982	329
7885	1982	481
7886	1983	598
7887	1983	390
7888	1983	511
7889	1983	353
7890	1984	491
7891	1984	310
7892	1984	567
7893	1984	458
7894	1985	545
7895	1985	560
7896	1985	346
7897	1985	333
7898	1986	416
7899	1986	507
7900	1986	501
7901	1986	518
7902	1987	496
7903	1987	334
7904	1987	490
7905	1987	470
7906	1988	348
7907	1988	341
7908	1988	328
7909	1988	307
7910	1989	321
7911	1989	394
7912	1989	432
7913	1989	342
7914	1990	531
7915	1990	535
7916	1990	407
7917	1990	591
7918	1991	324
7919	1991	354
7920	1991	487
7921	1991	576
7922	1992	388
7923	1992	346
7924	1992	391
7925	1992	549
7926	1993	485
7927	1993	319
7928	1993	338
7929	1993	568
7930	1994	466
7931	1994	379
7932	1994	433
7933	1994	468
7934	1995	578
7935	1995	528
7936	1995	548
7937	1995	575
7938	1996	426
7939	1996	480
7940	1996	555
7941	1996	561
7942	1997	511
7943	1997	398
7944	1997	390
7945	1997	446
7946	1998	419
7947	1998	520
7948	1998	440
7949	1998	547
7950	1999	467
7951	1999	583
7952	1999	496
7953	1999	456
7954	2000	332
7955	2000	458
7956	2000	493
7957	2000	505
7958	2001	402
7959	2001	461
7960	2001	421
7961	2001	324
7962	2002	535
7963	2002	521
7964	2002	554
7965	2002	302
\.


--
-- Data for Name: migration_versions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migration_versions (version, executed_at) FROM stdin;
20200522070007	2020-05-23 07:19:53
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_id_seq', 602, true);


--
-- Name: company_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.company_category_id_seq', 7965, true);


--
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.company_id_seq', 2002, true);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: company_category company_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_category
    ADD CONSTRAINT company_category_pkey PRIMARY KEY (id);


--
-- Name: company company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);


--
-- Name: migration_versions migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migration_versions
    ADD CONSTRAINT migration_versions_pkey PRIMARY KEY (version);


--
-- Name: company_title; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX company_title ON public.company USING btree (title);


--
-- Name: category category_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.category(id);


--
-- Name: company_category company_category_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_category
    ADD CONSTRAINT company_category_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.category(id);


--
-- Name: company_category company_category_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_category
    ADD CONSTRAINT company_category_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- PostgreSQL database dump complete
--

