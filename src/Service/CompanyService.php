<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\Company;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;

class CompanyService
{
    /** @var EntityManagerInterface  */
    protected $em;

    /** @var CompanyRepository */
    protected $repository;

    /**
     * CompanyService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Company::class);
    }

    /**
     * @param $id
     * @return Company|null
     */
    public function getById($id): ?Company
    {
        return $this->repository->find($id);
    }

    /**
     * @param $id
     * @return array
     */
    public function getByCategory($id): array
    {
        return $this->repository
            ->createQueryBuilder('com')
            ->innerJoin('com.categories', 'cat')
            ->andWhere('cat.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function findByName($query): array
    {
        return $this->repository
            ->createQueryBuilder('com')
            ->andWhere('com.title LIKE :query')
            ->setParameter('query', "%$query%")
            ->getQuery()
            ->getResult();
    }
}